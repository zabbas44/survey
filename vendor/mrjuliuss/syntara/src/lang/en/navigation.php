<?php 

return array(
    'index' => 'Dashboard',
    'admin-users' => 'Admin Users',
    'users' => 'Users',
    'survey-users' => 'Survey Users',
    'survey-roles' => 'Survey Roles',
    'survey-permissions' => 'Survey Permission',
    'groups' => 'Groups',
    'permissions' => 'Permissions',
    'logout' => 'Logout'
);