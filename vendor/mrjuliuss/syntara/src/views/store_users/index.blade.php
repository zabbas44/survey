@extends(Config::get('syntara::views.master'))


{{-- Web site Title --}}
@section('title')
{{{ $title }}} :: @parent
@stop

{{-- Content --}}
@section('content')
<div class="container" id="main-container">
    <div class="page-header">
        <h3>
            {{{ $title }}}

            
        </h3>
    </div>
    <ol class="breadcrumb">
        <li> <a class="" target="_parent" href="{{{ URL::to('survey/') }}}">Home</a></li>
        <li> <a class="" target="_parent" href="{{{ URL::to('survey/users') }}}">User Management</a></li>

    </ol>
    <table id="data-tables" class="table table-bordered  table-striped table-hover">
        <thead>
            <tr>
                <th class="col-md-1">{{{ Lang::get('wtadmin/users/table.first_name') }}}</th>
                <th class="col-md-1">{{{ Lang::get('wtadmin/users/table.last_name') }}}</th>
                <th class="col-md-2">{{{ Lang::get('wtadmin/users/table.email') }}}</th>
                <th class="col-md-2">{{{ Lang::get('wtadmin/users/table.company_name') }}}</th>
                <th class="col-md-1">{{{ Lang::get('wtadmin/users/table.activated') }}}</th>
                <th class="col-md-2">{{{ Lang::get('wtadmin/users/table.created_at') }}}</th>
                <th class="col-md-2">{{{ Lang::get('table.actions') }}}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{{$user->first_name}}}</td>
                <td>{{{$user->last_name}}}</td>
                <td>{{{$user->email}}}</td>
                <td>{{{$user->company_name}}}</td>
                <td>@if($user->confirmed)
                            Yes
                        @else
                            No
                        @endif
                </td>
                <td>{{{$user->created_at}}}</td>
                <td></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@stop

