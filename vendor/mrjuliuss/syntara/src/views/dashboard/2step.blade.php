@extends(Config::get('syntara::views.master'))

@section('content')
<script type="text/javascript" src="{{ asset('packages/mrjuliuss/syntara/assets/js/dashboard/login.js') }}"></script>
<div class="container" id="main-container">
    <div class="row" style="margin-top: 20px;">
        <div class="col-lg-2 col-lg-offset-5">
            <form id="2step-form" method="post" class="form-horizontal">
                <div class="form-group account-2stepvalidation">
                     <label class="control-label">2 Step Verification</label>
                     <input maxlength="4" type="text" class="col-lg-12 form-control" placeholder="Pin Code" name="pin_code" id="pin_code">
                </div>
                
                <button class="btn btn-block btn-large btn-primary" style="margin-top: 15px;">{{ trans('syntara::all.signin') }}</button>
            </form>
        </div>
    </div>
</div>
@stop