<?php 

namespace MrJuliuss\Syntara\Controllers;

use Illuminate\Support\Facades\View;
use Illuminate\Routing\Controller;
use Config;

class BaseController extends Controller 
{
    /**
    * Setup the layout used by the controller.
    *
    * @return void
    */
    protected function setupLayout()
    {
        $this->layout = View::make(Config::get('syntara::views.master'));
        $this->layout->title = 'Syntara - Dashboard';
        $this->layout->breadcrumb = array();
    }
    
    /*
     * creating generic path so we can use for all over the application
     */
    
    public function make_path($filename = '',$db_path = false){        
        
        $save_path = '/uploads/categories/';
        
        if($db_path){
            return $save_path;
        }
        
        return rtrim($_SERVER['DOCUMENT_ROOT'],'/') . $save_path . $filename;
    }
    public function make_path_dynamic($filename = '',$db_path = false, $folder_name=''){        
        
        if(!empty($folder_name))
            $save_path = '/uploads/' . Sentry::getUser()->id . '/' . Auth::user()->id . '/'.$folder_name.'/';
        else {
            $save_path = '/uploads/' . Sentry::getUser()->id . '/' . Auth::user()->id . '/image/';
        }    
        
        if($db_path){
            return $save_path;
        }
        
        return rtrim($_SERVER['DOCUMENT_ROOT'],'/') . $save_path . $filename;
    }
    public function make_path_theme($filename = '',$db_path = false){        
        
        $save_path = '/theme_images/';
        
        if($db_path){
            return $save_path;
        }
        
        return rtrim($_SERVER['DOCUMENT_ROOT'],'/') . $save_path . $filename;
    }
    
}