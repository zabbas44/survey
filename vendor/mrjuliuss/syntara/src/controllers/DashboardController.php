<?php

namespace MrJuliuss\Syntara\Controllers;

use MrJuliuss\Syntara\Controllers\BaseController;
use MrJuliuss\Syntara\Services\Validators\User as UserValidator;
use View;
use Input;
use Response;
use Request;
use Sentry;
use Config;
use Redirect;
use URL;
use PermissionProvider;
use DB;
use Mail;
class DashboardController extends BaseController {

    /**
     * Index loggued page
     */
    public function getIndex() {
        $this->layout = View::make(Config::get('syntara::views.dashboard-index'));
        $this->layout->title = trans('syntara::all.titles.index');
        $this->layout->breadcrumb = Config::get('syntara::breadcrumbs.dashboard');
    }

    /**
     * Login page
     */
    public function getLogin() {
        $this->layout = View::make(Config::get('syntara::views.login'));
        $this->layout->title = trans('syntara::all.titles.login');
        $this->layout->breadcrumb = Config::get('syntara::breadcrumbs.login');
    }

    /**
     * Login page
     */
    public function get2StepValidation() {
        $this->layout = View::make(Config::get('syntara::views.2step_validation'));
        $this->layout->title = trans('syntara::all.titles.login');
        $this->layout->breadcrumb = Config::get('syntara::breadcrumbs.login');
    }

    /**
     * Login post authentication
     */
    public function postLogin() {
        try {

            $validator = new UserValidator(Input::all(), 'login');

            if (!$validator->passes()) {
                return Response::json(array('logged' => false, 'errorMessages' => $validator->getErrors()));
            }

            $credentials = array(
                'email' => Input::get('email'),
                'password' => Input::get('pass'),
            );
            \Session::forget('credentials');
            \Session::forget('email');
            \Session::forget('password');
            \Session::push('email', Input::get('email'));
            \Session::push('password', Input::get('pass'));

            // authenticate user
            $response = Sentry::authenticate($credentials, Input::get('remember'));
        } catch (\Cartalyst\Sentry\Throttling\UserBannedException $e) {
            return Response::json(array('logged' => false, 'errorMessage' => trans('syntara::all.messages.banned'), 'errorType' => 'danger'));
        } catch (\RuntimeException $e) {
            return Response::json(array('logged' => false, 'errorMessage' => trans('syntara::all.messages.login-failed'), 'errorType' => 'danger'));
        }

        $step_validation = Sentry::getStepValidation();
        if ($step_validation == 1) {
            return Response::json(array('step' => true));
        } else {
            return Response::json(array('logged' => true));
        }
    }

    /**
     * Login post authentication
     */
    public function post2StepValidation() {
        try {

            $validator = new UserValidator(Input::all(), 'stepValidation');

            if (!$validator->passes()) {
                return Response::json(array('logged' => false, 'errorMessages' => $validator->getErrors()));
            }

            $credentials = \Session::get('credential');
            $email = \Session::get('email');
            $password = \Session::get('password');
            $pin_code = Input::get('pin_code');
            $credentials = array(
                'email' => (isset($email[0]) ? $email[0] : ''),
                'password' => (isset($password[0]) ? $password[0] : ''),
                'pin_code' => $pin_code,
            );

            // authenticate user
            $user_pin_code = Sentry::authenticate($credentials, Input::get('remember'), true);
        } catch (\RuntimeException $e) {
            return Response::json(array('logged' => false, 'errorMessage' => 'Pin Code is invalid', 'errorType' => 'danger'));
        }

        return Response::json(array('logged' => true));
    }

    /**
     * Logout user
     */
    public function getLogout() {
        Sentry::logout();

        return Redirect::route('indexDashboard');
    }

    /**
     * Access denied page
     */
    public function getAccessDenied() {
        $this->layout = View::make(Config::get('syntara::views.error'), array('message' => trans('syntara::all.messages.denied')));
        $this->layout->title = trans('syntara::all.titles.error');
        $this->layout->breadcrumb = Config::get('syntara::breadcrumbs.dashboard');
    }


}
