<?php
/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

/** ------------------------------------------
 *  Route model binding
 *  ------------------------------------------
 */
Route::model('user', 'User');
Route::model('role', 'Role');
Route::model('permission', 'Permission');

/** ------------------------------------------
 *  Route constraint patterns
 *  ------------------------------------------
 */
Route::pattern('user', '[0-9]+');
Route::pattern('role', '[0-9]+');
Route::pattern('permission', '[0-9]+');
Route::pattern('token', '[0-9a-z]+');

/** ------------------------------------------
 *  Admin Routes
 *  ------------------------------------------
 */
Route::group(array('prefix' => 'survey', 'before' => 'auth'), function() {
      # User Management
    Route::get('users/{user}/show', 'AdminUsersController@getShow');
    Route::get('users/{user}/edit', 'AdminUsersController@getEdit');
    Route::post('users/{user}/edit', 'AdminUsersController@postEdit');
    Route::get('users/{user}/delete', 'AdminUsersController@getDelete');
    Route::post('users/{user}/delete', 'AdminUsersController@postDelete');
    Route::post('users/{user}/delete', 'AdminUsersController@postDelete');
    Route::get('users/import', 'AdminUsersController@getImportUser');
    Route::post('users/import', 'AdminUsersController@importUser');
    Route::controller('users', 'AdminUsersController');

    Route::get('lists/{user}/show', 'AdminListsController@getShow');
    Route::get('lists/{user}/edit', 'AdminListsController@getEdit');
    Route::post('lists/{user}/edit', 'AdminListsController@postEdit');
    Route::get('lists/{lists}/delete', 'AdminListsController@getDelete');
    Route::post('lists/{lists}/delete', 'AdminListsController@postDelete');
    Route::post('lists/{lists}/delete', 'AdminListsController@postDelete');
    Route::get('lists/import', 'AdminListsController@getImportUser');
    Route::post('lists/import', 'AdminListsController@importUser');
    Route::controller('lists', 'AdminListsController');


    # User Role Management
    Route::get('roles/{role}/show', 'AdminRolesController@getShow');
    Route::get('roles/{role}/edit', 'AdminRolesController@getEdit');
    Route::post('roles/{role}/edit', 'AdminRolesController@postEdit');
    Route::get('roles/{role}/delete', 'AdminRolesController@getDelete');
    Route::post('roles/{role}/delete', 'AdminRolesController@postDelete');
    Route::controller('roles', 'AdminRolesController');

    # Permission Management System
    Route::get('permissions/{permission}/show', 'AdminPermissionsController@getShow');
    Route::get('permissions/{permission}/edit', 'AdminPermissionsController@getEdit');
    Route::post('permissions/{permission}/edit', 'AdminPermissionsController@postEdit');
    Route::get('permissions/{permission}/delete', 'AdminPermissionsController@getDelete');
    Route::post('permissions/{permission}/delete', 'AdminPermissionsController@postDelete');
    Route::controller('permissions', 'AdminPermissionsController');

    # Survey Management System
    Route::get('{surveyId}/show', 'AdminDashboardController@getShow');
    Route::get('{surveyId}/edit', 'AdminDashboardController@getEdit');
    Route::post('{surveyId}/edit', 'AdminDashboardController@postEdit');
    Route::get('{surveyId}/delete', 'AdminDashboardController@getDelete');
    Route::post('{surveyId}/delete', 'AdminDashboardController@postDelete');
    Route::post('{surveyId}/add', 'AdminDashboardController@postAdd');
    Route::get('get', 'AdminDashboardController@getRecords');
    Route::get('getfull', 'AdminDashboardController@getFullRecords');


    Route::get('getr', 'AdminDashboardController@getResponse');
    Route::get('getc', 'AdminDashboardController@getComments');

    Route::get('getpackage/{packageid}', 'AdminDashboardController@getPackage');

    Route::get('getsubscription', 'AdminDashboardController@getSubscription');

    Route::get('package', array('uses' => 'AdminDashboardController@package'));

    Route::get('payment', array('uses' => 'AdminDashboardController@getPayment'));

    Route::post('makepayment', array('uses' => 'AdminDashboardController@getMakepayment'));

    Route::get('paymentcomplete', array('uses' => 'AdminDashboardController@PaymentSuccess'));

    Route::get('detail/{surveyId}', array('uses' => 'AdminDashboardController@getSurveyDetail'));

    Route::get('getresponse/{surveyId}', array('uses' => 'AdminDashboardController@getResponseDetail'));

    Route::get('surveydetails/', 'AdminDashboardController@surveyDetails');

    Route::get('newquestion/', 'AdminDashboardController@newSurveyhtml');
    Route::get('email/', 'AdminDashboardController@emailConfigurations');

    Route::get('email/add/', 'EmailDashboardController@emailConfigurationsadd');

    Route::get('email/clientconfigurations/', 'EmailDashboardController@clientconfigurations');

    Route::get('email/{id}/edit', 'EmailDashboardController@edit');

    Route::get('email/{id}/delete', 'EmailDashboardController@remove');

    Route::post('email/{id}/delete', 'EmailDashboardController@clientconfigurationsremove');

    Route::post('email/create/', 'EmailDashboardController@create');

    Route::get('email/create/constantcontact', 'EmailDashboardController@constantcontact');

    Route::get('email/create/aweber', 'EmailDashboardController@createaweber');

    Route::post('email/update/', 'EmailDashboardController@update');

    Route::get('email/emailclientconfigurations/', 'EmailDashboardController@configurations');

    Route::get('mailchimp/getlist', 'MailchimpController@get');

    Route::get('responseservice/getlist', 'EmailResponseController@get');

    Route::controller('/', 'AdminDashboardController');


});


Route::get('response/add/{survey}/{question}/{answer}/{email}', 'ResponseController@getLogEntry');
Route::post('response/postcomments', 'ResponseController@postComments');

/** ------------------------------------------
 *  Frontend Routes
 *  ------------------------------------------
 */
// User reset routes
Route::get('forgot', 'UserController@getForgot');
Route::get('user/reset/{token}', 'UserController@getReset');
// User password reset
Route::post('user/reset/{token}', 'UserController@postReset');
//:: User Account Routes ::
Route::post('user/{user}/edit', 'UserController@postEdit');

//:: User Account Routes ::
Route::get('user/login', 'UserController@getLogin');
Route::post('user/login', 'UserController@postLogin');

//:: User Account Routes ::
Route::get('user/success/{Id}', 'UserController@getSuccessPage');
Route::get('user/error', 'UserController@getErrorPage');

# User RESTful Routes (Login, Logout, Register, etc)
Route::controller('user', 'UserController');

//:: Application Routes ::
# Filter for detect language
Route::when('contact-us', 'detectLang');

# Contact Us Static Page
Route::get('contact-us', function() {
    // Return about us page
    return View::make('site/contact-us');
});


Route::get('info', array('uses' => 'surveyDashboardController@surveyInfo'));
Route::get('your-story', array('uses' => 'surveyDashboardController@yourStory'));
Route::get('package', array('uses' => 'surveyDashboardController@package'));
Route::get('dashboard', array('uses' => 'surveyDashboardController@index'));
Route::get('/', array('uses' => 'UserController@getLogin'));


