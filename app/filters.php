<?php

/*
  |--------------------------------------------------------------------------
  | Application & Route Filters
  |--------------------------------------------------------------------------
  |
  | Below you will find the "before" and "after" events for the application
  | which may be used to do any work before or after a request into your
  | application. Here you may also register your custom route filters.
  |
 */

App::before(function($request) {
    
//    $request_uri = Request::server('REQUEST_URI');
//            if ($request_uri == 'dashboard' || $request_uri == '/') {
//                if (Auth::check()) {
//                    return Redirect::to('/survey');
//                } else{
//                    return Redirect::to('');
//                }
//            }

});


App::after(function($request, $response) {
    //
});

/*
  |--------------------------------------------------------------------------
  | Authentication Filters
  |--------------------------------------------------------------------------
  |
  | The following filters are used to verify that the user of the current
  | session is logged into this application. The "basic" filter easily
  | integrates HTTP Basic authentication for quick, simple checking.
  |
 */

Route::filter('auth', function() {
    if (Auth::guest()) { // If the user is not logged in
        return Redirect::guest('user/login');
    }
});

Route::filter('auth.basic', function() {
    return Auth::basic();
});

/*
  |--------------------------------------------------------------------------
  | Guest Filter
  |--------------------------------------------------------------------------
  |
  | The "guest" filter is the counterpart of the authentication filters as
  | it simply checks that the current user is not logged in. A redirect
  | response will be issued if they are, which you may freely change.
  |
 */

Route::filter('guest', function() {
    if (Auth::check())
        return Redirect::to('user/login/');
});

/*
  |--------------------------------------------------------------------------
  | Role Permissions
  |--------------------------------------------------------------------------
  |
  | Access filters based on roles.
  |
 */

// Check for role on all wtadmin routes
//Entrust::routeNeedsRole( 'wtadmin*', array('wtadmin'), Redirect::to('/') );
// Check for permissions on wtadmin actions
//Entrust::routeNeedsPermission('wtadmin/blogs*', 'manage_blogs', Redirect::to('/wtadmin'));
//Entrust::routeNeedsPermission('wtadmin/reports*', 'manage_reports', Redirect::to('/wtadmin'));
//Entrust::routeNeedsPermission('wtadmin/users*', 'manage_users', Redirect::to('/wtadmin'));
//Entrust::routeNeedsPermission('wtadmin/roles*', 'manage_roles', Redirect::to('/wtadmin'));
//Entrust::routeNeedsPermission('wtadmin/permissions*', 'manage_permission', Redirect::to('/wtadmin'));
//Entrust::routeNeedsPermission('wtadmin/grades*', 'manage_groups', Redirect::to('/wtadmin'));
//Entrust::routeNeedsPermission('wtadmin/categories*', 'manage_categories', Redirect::to('/wtadmin'));
//Entrust::routeNeedsPermission('wtadmin/pages*', 'manage_content', Redirect::to('/wtadmin'));
//Entrust::routeNeedsPermission('wtadmin/resources*', 'manage_resources', Redirect::to('/wtadmin'));

/*
  |--------------------------------------------------------------------------
  | CSRF Protection Filter
  |--------------------------------------------------------------------------
  |
  | The CSRF filter is responsible for protecting your application against
  | cross-site request forgery attacks. If this special token in a user
  | session does not match the one given in this request, we'll bail.
  |
 */

Route::filter('csrf', function() {
    if (Session::getToken() != Input::get('csrf_token') && Session::getToken() != Input::get('_token')) {
        throw new Illuminate\Session\TokenMismatchException;
    }
});

/*
  |--------------------------------------------------------------------------
  | Language
  |--------------------------------------------------------------------------
  |
  | Detect the browser language.
  |
 */

Route::filter('detectLang', function($route, $request, $lang = 'auto') {

    if ($lang != "auto" && in_array($lang, Config::get('app.available_language'))) {
        Config::set('app.locale', $lang);
    } else {
        $browser_lang = !empty($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? strtok(strip_tags($_SERVER['HTTP_ACCEPT_LANGUAGE']), ',') : '';
        $browser_lang = substr($browser_lang, 0, 2);
        $userLang = (in_array($browser_lang, Config::get('app.available_language'))) ? $browser_lang : Config::get('app.locale');
        Config::set('app.locale', $userLang);
        App::setLocale($userLang);
    }
});
