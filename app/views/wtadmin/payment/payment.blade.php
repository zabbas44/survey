@extends('wtadmin.layouts.default')

{{-- Content --}}
@section('content')


    <br/>
    <br/>
    <br/>

    <ol class="breadcrumb no-bg">
        <li><a class="" target="_parent" href="{{{ URL::to('survey/') }}}">Home</a></li>
        <li><a class="" target="_parent" href="{{{ URL::to('survey/payment') }}}">Payment</a></li>

    </ol>

    <div class="col-lg-12">

        <div class="alert alert-info">

            Your payment information will be not be stored for security purposes.

        </div>

        <br/>

    </div>

    <div class="col-lg-12" style="float:left;">


        <form class="form-horizontal" method="post" action="{{ URL::to('survey/makepayment') }}" autocomplete="off" enctype="multipart/form-data">

            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />


            <div class="panel panel-info">

                <!-- Default panel contents -->
                <div class="panel-heading">Pay be Paypal</div>
                <div class="panel-body">
                    <p>

                    <div class="col-lg-6">

                        <table id="package" class="table  table-striped table-hover">
                            <thead>

                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <div class="col-lg-4">

                                                    <input type="hidden" value="{{ $package  }}" name="package" />
                                                    <input type="hidden" value="paypal" name="type" />
                                                    <input type="submit" class="form-control btn-info" value="Make Payment" />

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </td>
                            </tr>

                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>

                    </p>
                </div>

            </div>


        </form>

        <form class="form-horizontal" method="post" action="{{ URL::to('survey/makepayment') }}" autocomplete="off" enctype="multipart/form-data">


        <div class="panel panel-info">

            <!-- Default panel contents -->
            <div class="panel-heading">Pay by Credit Card</div>
            <div class="panel-body">
                <p>

                <div class="col-lg-6">

                    <table id="package" class="table  table-striped table-hover">
                        <thead>
                        <tr>
                            <td style="padding-top: 17px;"><b>Name on card: </b></td>
                            <td><input type="text" class="form-control" name="cc_name" value="Zulqurnain abbas"
                                       placeholder="Example : John Smoth"/></td>
                        </tr>
                        <tr>
                            <td style="padding-top: 17px;"><b>Card number: </b></td>
                            <td><input type="text" class="form-control" name="cc_number" value="4012888888881881"
                                       placeholder="4012888888881881"/></td>
                        </tr>
                        <tr>
                            <td style="padding-top: 17px;"><b>Expiry date: </b></td>
                            <td>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-4">

                                                <select name="year" class="form-control">

                                                    @for($i=2015;$i<=2025;$i++)

                                                        <option value="{{ $i  }}">{{ $i }}</option>

                                                    @endfor
                                                </select>


                                            </div>




                                            <div class="col-lg-4">

                                                <select name="month" class="form-control">

                                                     @for($i=1;$i<=12;$i++)

                                                        <option value="{{ $i  }}">{{ $i }}</option>

                                                     @endfor


                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </td>
                        </tr>

                        <tr>
                            <td style="padding-top: 17px;"><b>Csv code: </b></td>
                            <td>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-4">

                                                <input type="text" class="form-control col-lg-12" name="csvcode" value="538"
                                                       placeholder="Ex : 123"/>

                                        </div>
                                    </div>
                                    </div>
                                </div>

                            </td>
                        </tr>

                        <tr>
                            <td style="padding-top: 17px;"></td>
                            <td>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-12">

                                                <input type="hidden" value="{{ $package  }}" name="package" />
                                                <input type="hidden" value="cc" name="type" />

                                                <input type="submit" class="form-control btn-info" value="Make Payment" />

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </td>
                        </tr>

                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                </div>

                </p>
            </div>

        </div>

            </form>

    </div>


@stop
