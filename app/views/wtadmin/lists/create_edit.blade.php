@extends('wtadmin.layouts.modal')

{{-- Content --}}
@section('content')
    <ol class="breadcrumb">
        <li><a class="" target="_parent" href="{{{ URL::to('wtadmin/') }}}">Home</a></li>
        <li><a class="" target="_parent" href="{{{ URL::to('wtadmin/users') }}}">List Management</a></li>
        <li class="last"> @if (!isset($user))
                Create New
            @else
                Update
            @endif  List
        </li>
    </ol>

    <div class="col-lg-12">
        <div class="box-tab justified">
            <ul class="nav nav-tabs">
                <li class=""><a data-toggle="tab" href="#home4" aria-expanded="false">Bulk upload CSV</a>
                </li>
                <li class="active"><a data-toggle="tab" href="#manual" aria-expanded="false">Manual Email Add</a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="home4" class="tab-pane">

                    {{-- Create User Form --}}

                    <form class="form-horizontal" method="post"
                          action="@if (isset($user)){{ URL::to('wtadmin/list/create') }}@endif" autocomplete="off"
                          enctype="multipart/form-data">
                        <!-- CSRF Token -->
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                        <!-- ./ csrf token -->

                        <!-- username -->
                        <div class="form-group {{{ $errors->has('username') ? 'error' : '' }}}">
                            <label class="col-md-2 control-label" for="username">List Name</label>

                            <div class="col-md-10">
                                <input class="form-control" type="text" name="username" id="username"
                                       value="{{{ Input::old('username', isset($user) ? $user->username : null) }}}"/>
                                {{ $errors->first('username', '<span class="help-inline">:message</span>') }}
                            </div>
                        </div>
                        <!-- ./ username -->

                        <!-- Email -->
                        <div class="form-group {{{ $errors->has('email') ? 'error' : '' }}}">
                            <label class="col-md-2 control-label" for="email">Upload File (csv file)</label>

                            <div class="col-md-10">
                                <input class="form-control" type="file" name="file" id="email"
                                       value="{{{ Input::old('email', isset($user) ? $user->email : null) }}}"/>
                                {{ $errors->first('email', '<span class="help-inline">:message</span>') }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="email">Sample (csv file)</label>

                            <div class="col-md-10">
                                <a href="/files/sample.csv"> Download file </a>
                            </div>
                        </div>
                        <!-- ./ email -->

                <!-- Form Actions -->
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <element class="btn btn-danger btn-cancel close_popup">Cancel</element>
                        <button type="reset" class="btn btn-default">Reset</button>
                        <button type="submit" class="btn btn-success">OK</button>
                        <input type="hidden" value="1" name="option" />
                    </div>
                </div>

                    </form>


            <!-- ./ tabs content -->

            <!-- ./ form actions -->

        </div>
        <div id="manual" class="tab-pane active">


            {{-- Create User Form --}}

            <form class="form-horizontal" method="post"
                  action="@if (isset($user)){{ URL::to('wtadmin/list/create') }}@endif" autocomplete="off"
                  enctype="multipart/form-data">
                <!-- CSRF Token -->
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                <!-- ./ csrf token -->

                <!-- username -->
                <div class="form-group {{{ $errors->has('username') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="username">Select list</label>

                    <div class="col-md-10">


                        <select name="list_id">

                        @foreach($lists as $perlist)

                            <option class="form-control" value="<?php echo $perlist['list_id'] ?>"><?php

                                echo $perlist['lists_name'];

                                ?></option>

                        @endforeach

                        </select>

                    </div>

                </div>
                <!-- ./ username -->

                <!-- Email -->
                <div class="form-group {{{ $errors->has('input') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="email">Email Address</label>

                    <div class="col-md-10">
                        <input class="form-control" placeholder="Email Address" type="input" name="email" id="input"
                               value="{{{ Input::old('email', isset($user) ? $user->email : null) }}}"/>
                        {{ $errors->first('email', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>

                <!-- ./ email -->

                <!-- Form Actions -->
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <element class="btn btn-danger btn-cancel close_popup">Cancel</element>
                        <button type="reset" class="btn btn-default">Reset</button>
                        <button type="submit" class="btn btn-success">OK</button>
                        <input type="hidden" value="2" name="option" />
                    </div>
                </div>

            </form>



        </div>
    </div>

@stop
