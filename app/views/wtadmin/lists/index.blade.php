@extends('wtadmin.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{{ $title }}} :: @parent
@stop

{{-- Content --}}
@section('content')

    <br/>
    <br/>
    <br/>

    <div>
            <h3>
                <div class="pull-right">
                    <a href="{{{ URL::to('survey/lists/create') }}}" class="btn btn-small btn-info iframe"><span
                                class="glyphicon glyphicon-plus-sign"></span> Create New List</a>&nbsp;&nbsp;

                </div>
            </h3>
        </div>
        <ol class="breadcrumb no-bg">
            <li><a class="" target="_parent" href="{{{ URL::to('survey/') }}}">Home</a></li>
            <li><a class="" target="_parent" href="{{{ URL::to('survey/lists') }}}">Lists Management</a></li>

        </ol>
        <div class="col-lg-12">
            <div class="panel panel-info">
                <!-- Default panel contents -->
                <div class="panel-heading">Lists Management</div>
                <div class="panel-body">
                    <p>
                    <table id="users" class="table table-bordered  table-striped table-hover">
                        <thead>
                        <tr>
                            <th class="col-md-2">{{{ Lang::get('wtadmin/lists/table.list_count') }}}</th>
                            <th class="col-md-2">{{{ Lang::get('wtadmin/lists/table.list_name') }}}</th>{{--
                        <th class="col-md-2">{{{ Lang::get('wtadmin/lists/table.list_email_count') }}}</th>--}}
                            <th class="col-md-2">{{{ Lang::get('wtadmin/lists/table.created_at') }}}</th>
                            <th class="col-md-2">{{{ Lang::get('table.actions') }}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

{{-- Scripts --}}
@section('scripts')
    <script type="text/javascript">
        var oTable;
        $(document).ready(function () {
            oTable = $('#users').dataTable({
                "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page"
                },
                "bProcessing": true,
                "sAjaxSource": "{{ URL::to('survey/lists/data') }}",
                "fnDrawCallback": function (oSettings) {
                    $(".iframe").colorbox({iframe: true, width: "80%", height: "80%"});
                }
            });
        });
    </script>
@stop