@extends('wtadmin.layouts.new_theme_default')

{{-- Content --}}
@section('content')
<!-- Tabs -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a class="" target="_parent" href="{{{ URL::to('wtadmin/') }}}"> Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a class="" target="_parent" href="{{{ URL::to('wtadmin/comments/') }}}"> Comments Management</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">{{{ $title }}}</a>
        </li>
    </ul>

</div>

<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>Update Comments 
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div>
                        <h2 class="modal-title">Update Comments</h2>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="btn-group pull-right">

                        <button class="btn default close_popup"><span class="glyphicon glyphicon-circle-arrow-left"></span> Back</button>

                    </div>
                </div>
            </div>
        </div>
        <!--                <div class="portlet-body form">-->
        <br/>
        <div class="portlet-body form">
            <!-- ./ tabs -->
            {{-- Edit Blog Comment Form --}}
            <form class="form-horizontal" method="post" action="" autocomplete="off">
                <!-- CSRF Token -->
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                <!-- ./ csrf token -->

                <!-- Tabs Content -->
                <div class="tab-content">
                    <!-- General tab -->
                    <div class="tab-pane active" id="tab-general">

                        <!-- Content -->
                        <div class="form-group {{{ $errors->has('content') ? 'error' : '' }}}">
                            <div class="col-md-12">
                                <!--                                            <label class="control-label" for="content">Content</label>-->
                                <textarea class="form-control full-width wysihtml5" name="content" value="content" rows="10">{{{ Input::old('content', $comment->content) }}}</textarea>
                                {{ $errors->first('content', '<span class="help-inline">:message</span>') }}
                            </div>
                        </div>
                        <!-- ./ content -->
                    </div>
                    <!-- ./ general tab -->
                </div>
                <!-- ./ tabs content -->

                <!-- Form Actions -->
                <div class="form-group">
                    <div class="col-md-12">

                        <element class="btn default btn-cancel close_popup">Close</element>
                        <button type="reset" class="btn default">Reset</button>
                        <button type="submit" class="btn green">Update</button>
                    </div>
                </div>
                <!-- ./ form actions -->
            </form>
        </div>
    </div>

</div>
@stop
