@extends('wtadmin.layouts.default')

@section('content')

        <br/><br/><br/>

        <ol class="breadcrumb no-bg">
            <li><a class="" target="_parent" href="{{{ URL::to('survey/') }}}">Home</a></li>
            <li><a class="last" target="_parent"
                   href="{{{ URL::to('survey/detail') }}}/{{ $survey_details[0]['s_id']  }}">{{ $survey_details[0]['s_name']  }}</a>
            </li>
        </ol>

<div class="col-lg-12">
        <div class="panel panel-success">

            <?php foreach($survey_details as $persurvey){  ?>

                <!-- Default panel contents -->
                <div class="panel-heading">
                    <b> {{ ucfirst($persurvey['s_question']) }}</b> </div>

                <div class="panel-body">

                    <div class="col-lg-12 row">

                    <div class="col-lg-8">

                    <div class="panel panel-default row panel-bordered">

                        <div class="panel-heading">Survey Responses (count):</div>
                        <div class="panel-body">

                            <div class="col-lg-12">

                                <?php

                                    $result = 0;
                                    $result1 = 0;
                                    $result2 = 0;
                                    $result3 = 0;
                                    $message = false;

                                ?>

                                @foreach($response_model as $perresponse)

                                    @if($persurvey['s_question_order'] == $perresponse['s_question'])

                                    @if($perresponse['r_response'] == 1)
                                        <?php $result++; $message = true;  ?>
                                    @endif
                                    @if($perresponse['r_response'] == 2)
                                        <?php $result1++;$message = true;  ?>
                                    @endif
                                    @if($perresponse['r_response'] == 3)
                                        <?php $result2++; $message = true; ?>
                                    @endif
                                    @if($perresponse['r_response'] == 4)
                                        <?php $result3++;$message = true;  ?>
                                    @endif

                                    @endif

                                @endforeach


                                <?php if(!empty($persurvey['s_1_response'])){ ?>

                                <div class="col-lg-12">
                                        <span class="label label-primary">{{ $persurvey['s_1_response']  }}
                                            - [{{ $result }}]</span><br/>

                                </div>

                                <?php  } ?>

                                <?php if(!empty($persurvey['s_2_response'])){ ?>

                                <div class="col-lg-12">
                                        <span class="label label-success">{{ $persurvey['s_2_response']  }}
                                            - [{{ $result1 }}]</span><br/>

                                </div>

                                <?php } ?>

                                <?php if(!empty($persurvey['s_3_response'])){ ?>

                                <div class="col-lg-12">
                                        <span class="label label-warning">{{ $persurvey['s_3_response']  }}
                                            - [{{ $result2 }}]</span><br/>

                                </div>

                                <?php } ?>

                                <?php if(!empty($persurvey['s_4_response'])){ ?>

                                <div class="col-lg-12">
                                        <span class="label label-danger">{{ $persurvey['s_4_response']  }}
                                            - [{{ $result3 }}]</span><br/>

                                </div>

                                <?php  } ?>


                                @if($message == false)



                                    <div class="col-lg-12" style="margin-top: 20px;">

                                        <div class="alert alert-danger">

                                            No response recieved yet!

                                        </div>

                                    </div>

                                @endif


                            </div>


                        </div>

                    </div>

                    </div>

                        <div class="col-lg-4">

                            <div class="panel panel-default panel-bordered">

                                <div class="panel-heading">Survey Options :</div>
                                <div class="panel-body">

                                    <div class="col-lg-12">
                                        <span class="label label-primary">{{ $persurvey['s_1_response']  }}</span><br/>

                                    </div>
                                    <div class="col-lg-12">
                                        <span class="label label-success">{{ $persurvey['s_2_response']  }}</span><br/>

                                    </div>
                                    <div class="col-lg-12">
                                        <span class="label label-warning">{{ $persurvey['s_3_response']  }}</span><br/>

                                    </div>
                                    <div class="col-lg-12">
                                        <span class="label label-danger">{{ $persurvey['s_4_response']  }}</span><br/>

                                    </div>
                                </div>

                            </div>
                        </div>
                 </div>

                </div>

                <?php } ?>

                        <div class="panel panel-success panel-bordered">



                            <div class="panel-heading"><b> Responses : </b></div>
                            <div class="panel-body">

                                <div class="col-lg-12">


                                    <table id="responses" class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-3">{{{ Lang::get('wtadmin/survey/table.d_created_at') }}}</th>
                                            <th class="col-md-3">Question No</th>
                                            <th class="col-md-3">Response Text</th>
                                            <th class="col-md-3">Email</th>
                                            <th class="col-md-3">Created at </th>

                                        </tr
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>


                                    {{-- Scripts --}}
                                    @section('scripts')
                                        <script type="text/javascript">
                                            var oTable;
                                            $(document).ready(function () {
                                                oTable = $('#responses').dataTable({
                                                    "sDom": "<'row'<'col-md-12'l><'col-md-6'f>r>t<'row'<'col-md-12'i><'col-md-12'p>>",
                                                    "sPaginationType": "bootstrap",
                                                    "oLanguage": {
                                                        "sLengthMenu": ""
                                                    },
                                                    bFilter: false, bInfo: false,
                                                    "bProcessing": true,
                                                    "bServerSide": true,
                                                    "sAjaxSource": "{{ URL::to('survey/getresponse/') }}/{{$survey_details[0]['s_id']}}",
                                                    "fnDrawCallback": function (oSettings) {
                                                        $(".iframe").colorbox({
                                                            iframe: true,
                                                            width: "80%",
                                                            height: "80%"
                                                        });
                                                    }
                                                });

                                            });

                                        </script>
                                    @endsection

                                </div>

                                {{----}}

                            </div>


                        </div>


                    </div>


                        <?php if(isset($survey_details[0]['l_page_text']) && !empty($survey_details[0]['l_page_text'])){ ?>


                        <div class="panel panel-success panel-bordered">

                            <div class="panel-heading ">Landing page text :</div>
                            <div class="panel-body">

                                <div class="col-lg-12">
                                    <span class="">{{ $survey_details[0]['l_page_text']  }}</span><br/>

                                </div>

                            </div>

                        </div>

                        <?php } ?>

                    </div>

                </div>
            </div>
        </div>

    @stop