@extends('wtadmin.layouts.default')

{{-- Content --}}
@section('content')

    <br/>
    <br/>
    <br/>
    <ol class="breadcrumb no-bg">
        <li><a class="" target="_parent" href="{{{ URL::to('survey/') }}}">Home</a></li>
        <li><a class="" target="_parent" href="{{{ URL::to('survey/') }}}">Dashboard</a></li>
    </ol>

    <div class="page" style="padding-bottom: 50px;">
        <div class="pull-right">
            <a href="{{{ URL::to('survey/create') }}}" class="btn btn-small btn-info"><span
                        class="glyphicon glyphicon-plus-sign"></span> Create Survey</a>&nbsp;&nbsp;
        </div>
    </div>

            <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">Displaying All Surveys</div>
                                <div class="panel-body">

                                    <table id="roles" class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-2">id</th>
                                            <th class="col-md-2">{{{ Lang::get('wtadmin/survey/table.d_survey_name') }}}</th>
                                            <th class="col-md-2">{{{ Lang::get('wtadmin/survey/table.d_created_at') }}}</th>
                                            <th class="col-md-1">detail</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>

            {{-- Scripts --}}
            @section('scripts')
                <script type="text/javascript">
                    var oTable;
                    $(document).ready(function () {
                        oTable = $('#roles').dataTable({
                            "sDom": "<'row'<'col-md-12'l><'col-md-6'f>r>t<'row'<'col-md-12'i><'col-md-12'p>>",
                            "sPaginationType": "bootstrap",
                            "oLanguage": {
                                "sLengthMenu": ""
                            },
                            bFilter: false, bInfo: false,
                            "bProcessing": true,
                            "bServerSide": true,
                            "sAjaxSource": "{{ URL::to('survey/getfull') }}",
                            "fnDrawCallback": function (oSettings) {
                                $(".iframe").colorbox({iframe: true, width: "80%", height: "80%"});
                            }
                        });
                    });

                </script>
            @stop

        </div>
    </div>

@stop
