<div class="panel" id="question_{{ $id }}_container">

    <div class="panel-heading border">
        <b> Question No {{ $id }} </b>

        @if($id != 1)
            <button id="question_{{ $id }}" onclick="removeQuestion({{ $id }});" class="btn btn-danger btn-sm btn-icon mr5" type="button" style="float: right;">
                <i onclick="removeQuestion({{ $id }});" class="fa fa-exclamation-triangle"></i>
                <span onclick="removeQuestion({{ $id }});">Remove</span>
            </button>
        @endif

    </div>

    <div class="panel-body">

        <!-- Question -->
        <div class="form-group ">
            <label for="question" class="col-md-2 control-label">Survey Question</label>
            <div class="col-lg-6 question">
                <input type="text" placeholder="Your Question" id="question" name="question[]" class="form-control" >
            </div>
        </div>

        <hr/>
        <!-- ./ Question -->

        <!-- Response -->
        <div class="form-group ">
            <label for="first_response" class="col-md-2 control-label">First Response</label>
            <div class="col-md-6 first_response">
                <input type="text" value="" c placeholder="Your first Option" id="first_response" name="first_response[]" class="form-control">

            </div>
            <div class="col-md-4">
                <input type="checkbox" id="opt_1_check" name="opt_1_check[]" class="">
                <label for="opt_1_check" class="">Enable Comments</label>
            </div>
        </div>
        <!-- ./ Response -->
        <!-- Response -->
        <div class="form-group ">
            <label for="opt_2" class="col-md-2 control-label">Second Response</label>
            <div class="col-md-6">
                <input type="text" placeholder="Your Second Option" id="opt_2" name="opt_2[]" class="form-control">

            </div>
            <div class="col-md-4">
                <input type="checkbox" id="opt_2_check" name="opt_2_check" class="">
                <label for="opt_2_check" class="">Enable Comments</label>
            </div>
        </div>
        <!-- ./ Response -->
        <!-- Response -->
        <div class="form-group ">
            <label for="opt_3" class="col-md-2 control-label">Third Response</label>
            <div class="col-md-6">
                <input type="text" placeholder="Your Third Option" id="opt_3" name="opt_3[]" class="form-control">

            </div>
            <div class="col-md-4">
                <input type="checkbox" id="opt_3_check" name="opt_3_check[]" class="">
                <label for="opt_3_check" class="">Enable Comments</label>
            </div>
        </div>
        <!-- ./ Response -->
        <!-- Response -->
        <div class="form-group ">
            <label for="opt_4" class="col-md-2 control-label">Fourth Response</label>
            <div class="col-md-6">
                <input type="text" placeholder="Your Fourth Option" id="opt_4" name="opt_4[]" class="form-control">

            </div>
            <div class="col-md-4">
                <input type="checkbox" id="opt_4_check" name="opt_4_check[]" class="">
                <label for="opt_4_check" class="">Enable Comments</label>
            </div>
        </div>

    </div>


</div>