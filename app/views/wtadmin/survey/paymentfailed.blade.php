@extends('wtadmin.layouts.default')

{{-- Content --}}
@section('content')

    <br/>
    <br/>
    <br/>

    <ol class="breadcrumb no-bg">
        <li> <a class="" target="_parent" href="{{{ URL::to('survey/') }}}">Home</a></li>
        <li> <a class="" target="_parent" href="{{{ URL::to('survey/payment') }}}/{{ $package }}">Payment</a></li>

    </ol>

<div class="col-lg-12" style="float:left;">


    <div class="panel panel-info">
        <!-- Default panel contents -->
        <div class="panel-heading">Payment information </div>
        <div class="panel-body">
            <p>

                <div class="alert alert-danger">
                     <b> Transaction Declined : </b> {{ $error }}

                     <a class="" target="_parent" href="{{{ URL::to('survey/payment') }}}/{{ $package }}">Go Back</a>

                </div>

            </p>
        </div>

    </div>

</div>

@stop
