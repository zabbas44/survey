@extends('wtadmin.layouts.default')

{{-- Content --}}
@section('content')

    <br/>
    <br/>
    <br/>

<ol class="breadcrumb no-bg">
    <li> <a class="" target="_parent" href="{{{ URL::to('survey/') }}}">Home</a></li>
    <li class="last"> {{{ $title }}}</li>
</ol>


    <div class="col-lg-12">

        @if(Session::get('success'))

        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>

        @endif

{{-- Create Category Form --}}
<form class="form-horizontal" enctype="multipart/form-data" id="form-survey"  method="post" action="{{{ URL::to('survey/add') }}}" autocomplete="off">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
    <!-- ./ csrf token -->

    <!-- Tabs Content -->
    <div class="tab-content">
        <!-- Tab General -->
        <div class="tab-pane active" id="tab-general">

            <div class="panel">
                <div class="panel-heading border">
                    <b> Survey Options </b>
                </div>
                <div class="panel-body">

                    <div class="form-group {{{ $errors->has('name') ? 'error' : '' }}}">
                        <label class="col-md-2 control-label" for="name">Name</label>
                        <div class="col-lg-6 name_error">
                            <input class="form-control" type="text" name="name" id="name" value="{{{ Input::old('name') }}}" />
                            {{ $errors->first('name', '<span class="alert alert-danger col-lg-12">:message</span>') }}
                        </div>
                    </div>

                </div>
            </div>

            <div id="base_panel">



            </div>

            </div>

            <input type="button" class="alert alert-success" id="add_another" value="Add Another Question +" />

            <hr />

            <div class="col-md-12 form-group {{{ $errors->has('list') ? 'error' : '' }}}">
                <label class="col-md-2 control-label" for="list">List</label>
                <div class="col-md-6">
                    <select class="form-control" name="list" id="list">
                    @foreach ($lists as $perlist)
                        <option value="{{ $perlist->list_id }}">{{ $perlist->lists_name }}</option>
                    @endforeach
                    
                    </select>
                    {{ $errors->first('list', '<span class="help-inline">:message</span>') }}
                </div>
            </div>
            <hr />
        <div class="col-md-12 form-group {{{ $errors->has('list') ? 'error' : '' }}}">
            <label class="col-md-2 control-label" for="list">Email Service</label>
            <div class="col-md-6">
                <select class="form-control" name="service" id="list">
                    @foreach ($serviceList as $key=>$perservice)
                        <option value="{{ $key }}">{{ $perservice }}</option>
                    @endforeach

                </select>
                {{ $errors->first('list', '<span class="help-inline">:message</span>') }}
            </div>
        </div>
            <hr />
            <div class="col-md-12 form-group {{{ $errors->has('landing_page') ? 'error' : '' }}}">
                <label class="col-md-2 control-label" for="landing_page">Landing Page</label>
                <div class="col-md-6">
                    <textarea class="form-control" name="landing_page"></textarea>
                    {{ $errors->first('landing_page', '<span class="help-inline">:message</span>') }}
                </div>
            </div>
            <hr/>
            <div class="col-md-12 form-group {{{ $errors->has('notify_me') ? 'error' : '' }}}">
                <div class="col-md-2"></div>
                <div class="col-md-4">
                    <input type="checkbox" name="notify_me" />
                    <label class="" for="notify_me">NOTIFY ME OF EACH NEW RESPONSE</label>
                {{ $errors->first('notify_me', '<span class="help-inline">:message</span>') }}
                </div>
            </div>

            <?php if(isset($package_details[0]['pl_email_counts'])){ ?>

            <div class="col-md-12 form-group">
                <div class="col-md-2"></div>
                    <div role="alert" class="alert alert-info">
                        <strong>Package : </strong> {{ $package_details[0]['pl_name'] }}<br/>
                        <strong>Package Emails's : </strong> {{ $package_details[0]['pl_email_counts'] }} <br/>
                        <strong>Available Email's : </strong> {{ $left_emails }}

                    </div>
            </div>

            <?php } ?>

        </div>
        <!-- ./ tab general -->


    <!-- Form Actions -->
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            @if(!empty($serviceList))
            <element class="btn btn-danger btn-cancel close_popup">Cancel</element>
            <button type="reset" class="btn btn-default">Reset</button>
            <button type="submit" id="create_survey" class="btn btn-success">Create Survey</button>
            @else
            <div class="alert alert-danger">
                Email service not found! please add to create survey
            </div>
            @endif
        </div>
    </div>

    </div>
    <!-- ./ tabs content -->



    <!-- ./ form actions -->
</form>

    </div>

    </div>

@stop

{{-- Scripts --}}
@section('scripts')
    <script type="text/javascript">

        $(document).on("click","#create_survey",function(){

            $(".alert-danger").remove();

            var pass = true;
            var name = $("#name").val();

            if(name = ""){
                $(".name_error").append('<span class="alert alert-danger col-lg-12">The field is required.</span>');
            }

            $( "#base_panel > .panel" ).each( function( index, element ){

                var test = $( this ).find("#question").val();
                var fr   = $( this ).find("#first_response").val();



                if(test == ""){
                    $( this ).find(".question").append('<span class="alert alert-danger col-lg-12">The field is required.</span>');
                    pass = false;
                    console.log("ac");
                }

                if(fr == ""){
                    $( this ).find(".first_response").append('<span class="alert alert-danger col-lg-12">The field is required.</span>');
                    pass = false;
                    console.log("a");
                }

            });

            if(pass == false){
                    return false;
            }

            $("#form-survey").submit();

        });

        $(document).ready(function() {

            generate_question();

            $('#add_another').click(function(){

                generate_question();

            });

        });

        function generate_question(){

            var count = $("#base_panel > .panel").length;
            count++;

            $.ajax({
                url: 'newquestion?count='+count ,
                success: function(data) {

                    if(data == 'false'){

                        alert("Unexpected Error Ocurred Please reload!");
                        return false;

                    }

                    $("#base_panel").append(data);
                },
                type: 'GET'
            });

        }

        function removeQuestion(id){

            var string = "question_"+id+"_container";
            $('#'+string).remove();

        }

    </script>
@stop

