@extends('wtadmin.layouts.default')

{{-- Content --}}
@section('content')

    <br/>
    <br/>
    <br/>

    <ol class="breadcrumb no-bg">
        <li><a class="" target="_parent" href="{{{ URL::to('survey/') }}}">Home</a></li>
        <li><a class="" target="_parent" href="{{{ URL::to('survey/package') }}}">Package Management</a></li>

    </ol>

    <div class="col-lg-12">

        <div class="panel panel-info">
            <!-- Default panel contents -->
            <div class="panel-heading">Package information</div>
            <div class="panel-body">

                <table id="package" class="table table-bordered  table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="col-md-2">{{{ Lang::get('wtadmin/lists/table.package_id') }}}</th>
                        <th class="col-md-2">{{{ Lang::get('wtadmin/lists/table.package_name') }}}</th>
                        <th class="col-md-2">{{{ Lang::get('wtadmin/lists/table.package_count_total') }}}</th>
                        <th class="col-md-2">{{{ Lang::get('wtadmin/lists/table.package_price') }}}</th>
                        <th class="col-md-2">{{{ Lang::get('wtadmin/lists/table.created_at') }}}</th>
                        <th class="col-md-2">{{{ Lang::get('wtadmin/lists/table.package_action') }}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

        </div>

        <div class="panel panel-info">
            <!-- Default panel contents -->
            <div class="panel-heading">Billing/Subscriptions information</div>
            <div class="panel-body">
                <table id="subs" class="table table-bordered  table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="col-md-2">{{{ Lang::get('wtadmin/lists/table.sub_id') }}}</th>
                        <th class="col-md-2">{{{ Lang::get('wtadmin/lists/table.sub_name') }}}</th>
                        <th class="col-md-2">{{{ Lang::get('wtadmin/lists/table.sub_count_left') }}}</th>
                        <th class="col-md-2">{{{ Lang::get('wtadmin/lists/table.sub_count_total') }}}</th>
                        <th class="col-md-2">{{{ Lang::get('wtadmin/lists/table.created_at') }}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
            @section('scripts')
    <script type="text/javascript">
        var oTable;
        $(document).ready(function () {
            oTable = $('#package').dataTable({
                "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page"
                },
                "bProcessing": true,
                "sAjaxSource": "{{ URL::to('survey/getpackage/') }}/{{$package}}",
                "fnDrawCallback": function (oSettings) {
                    $(".iframe").colorbox({iframe: true, width: "80%", height: "80%"});
                }
            });
            oTable = $('#subs').dataTable({
                "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page"
                },
                "bProcessing": true,
                "sAjaxSource": "{{ URL::to('survey/getsubscription') }}",
                "fnDrawCallback": function (oSettings) {
                    $(".iframe").colorbox({iframe: true, width: "80%", height: "80%"});
                }
            });

        });
    </script>
            @stop
    </div>
@stop