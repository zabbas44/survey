@extends('wtadmin.layouts.modal')

{{-- Content --}}
@section('content')


    <div class="col-lg-12">

        <ol class="breadcrumb no-bg">
            <li><a class="" target="_parent" href="{{{ URL::to('survey/') }}}">Home</a></li>
            <li class="last"> {{{ $title }}}</li>
        </ol>

        <div class="col-lg-12">

            {{-- Create Category Form --}}
            <form class="form-horizontal" enctype="multipart/form-data" id="form-survey" method="post"
                  action="{{{ URL::to('survey/email/create') }}}" autocomplete="off">
                <!-- CSRF Token -->
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                <!-- ./ csrf token -->

                <!-- Tabs Content -->
                <div class="tab-content">
                    <!-- Tab General -->
                    <div class="tab-pane active" id="tab-general">


                        <div class="col-md-12 form-group {{{ $errors->has('list') ? 'error' : '' }}}">
                            <label class="col-md-2 control-label" for="list">Email Service</label>

                            <div class="col-md-10">
                                <select class="form-control" name="service" id="email_client">
                                    <option value="option">Please select email client</option>
                                    @foreach ($serviceList as $key=>$perservice)
                                        <option value="{{ $key }}" @if($option == $key) selected @endif >{{ $perservice }}</option>
                                    @endforeach

                                </select>

                                {{ $errors->first('service', '<span class="help-inline">:message</span>') }}

                            </div>

                            <br/><br/>

                            <div class="content" id="content">

                            </div>

                        </div>

                        <!-- Form Actions -->
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <element class="btn btn-danger btn-cancel close_popup">Cancel</element>
                                <button type="reset" class="btn btn-default">Reset</button>
                            </div>
                        </div>

                    </div>
            </form>

            <input type="hidden" value="{{ $option }}" id="optionerror" />

            @stop

            {{-- Scripts --}}
            @section('scripts')
                <script type="text/javascript">


                    $(document).on("click","#getresponse",function(){

                        var list_value = $("#apikey").val();

                        if(list_value == ""){

                            alert("please enter api-key");
                            return false;

                        }

                        $.ajax({
                            url: '{{ URL::to('survey') }}/responseservice/getlist?option=' + list_value,
                            success: function (data) {
                                $(".data_populate").html(data);
                            },
                            type: 'GET'
                        });

                    });

                    $(document).on("click","#fetch_lists",function(){

                        var list_value = $("#apikey").val();

                        if(list_value == ""){

                            alert("please enter api-key");
                            return false;

                        }

                            $.ajax({
                                url: '{{ URL::to('survey') }}/mailchimp/getlist?option=' + list_value,
                                success: function (data) {
                                    $(".data_populate").html(data);
                                },
                                type: 'GET'
                            });

                    });

                    $(document).ready(function(){

                        var value = $("#optionerror").val();

                        if(value != ""){

                            $.ajax({
                                url: '{{ URL::to('survey/') }}/emailclientconfigurations?option=' + value,
                                success: function (data) {
                                    $("#content").html(data);
                                },
                                type: 'GET'
                            });

                        }

                    });

                    $(document).on("change", "#email_client", function () {

                        var value = $('#email_client :selected').val();

                        $.ajax({
                            url: 'emailclientconfigurations?option=' + value,
                            success: function (data) {
                                $("#content").html(data);
                            },
                            type: 'GET'
                        });

                    });

                </script>
@stop

