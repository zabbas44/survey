<div class="col-lg-12">
    <div class="box-tab justified">

            <div id="home4" class="tab-pane active">

                <div class="panel panel-default">
                    <div class="panel-heading">Configure Client {{ $clientname }}</div>
                    <div class="panel-body">

                        <label class="col-md-2 control-label" for="list">Api Key</label>

                        <div class="col-md-10">

                            <input type="text" name="apikey" id="apikey" value="{{ $u_e_c_key }}" placeholder="Your Account Api key" class="form-control" />

                            <input type="button" id="getresponse" value="Validate Api" class="form-controll btn btn-success col-lg-3" style="margin-top: 5px;margin-bottom: 5px;" />

                                <span class="data_populate">

                                </span>

                            <br/>
                            <br/>

                            <div class="alert alert-info">

                                <b> You can find you api key in </b>
                                <br/>

                                My Account > Account details > GetResponse API

                            </div>

                        </div>

                    </div>
                </div>

        </div>
    </div>
