@extends('wtadmin.layouts.default')

{{-- Content --}}
@section('content')


    <br/>
    <br/>
    <br/>
    <ol class="breadcrumb no-bg">
        <li><a class="" target="_parent" href="{{{ URL::to('survey/') }}}">Home</a></li>
        <li><a class="" target="_parent" href="{{{ URL::to('survey/email') }}}">Emails Configurations Dashboard</a></li>
    </ol>

    <div class="page" style="padding-bottom: 50px;">
        <div class="pull-right">
            <a href="{{{ URL::to('survey/email/add') }}}" class="btn btn-small btn-info iframe cboxElement"><span
                        class="glyphicon glyphicon-plus-sign"></span> Add New Email Client</a>&nbsp;&nbsp;
        </div>
    </div>

            <div class="col-lg-12">
                <div class="box-tab justified">

                    <div class="tab-content">
                        <div id="home4" class="tab-pane active">

                            <div class="panel panel-default">
                                <div class="panel-heading">Displaying All Emails</div>
                                <div class="panel-body">

                                    <table id="roles" class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-3">id</th>
                                            <th class="col-md-5">Email Client Name</th>
                                            <th class="col-md-3">{{{ Lang::get('wtadmin/survey/table.d_created_at') }}}</th>
                                            <th class="col-md-2">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            {{-- Scripts --}}
            @section('scripts')
                <script type="text/javascript">
                    var oTable;
                    $(document).ready(function () {
                        oTable = $('#roles').dataTable({
                            "sDom": "<'row'<'col-md-12'l><'col-md-6'f>r>t<'row'<'col-md-12'i><'col-md-12'p>>",
                            "sPaginationType": "bootstrap",
                            "oLanguage": {
                                "sLengthMenu": ""
                            },
                            bFilter: false, bInfo: false,
                            "bProcessing": true,
                            "bServerSide": true,
                            "sAjaxSource": "{{ URL::to('survey/email/clientconfigurations/') }}",
                            "fnDrawCallback": function (oSettings) {
                                $(".iframe").colorbox({iframe: true, width: "80%", height: "80%"});
                            }
                        });

                        oTable = $('#responses').dataTable({
                            "sDom": "<'row'<'col-md-12'l><'col-md-6'f>r>t<'row'<'col-md-12'i><'col-md-12'p>>",
                            "sPaginationType": "bootstrap",
                            "oLanguage": {
                                "sLengthMenu": ""
                            },
                            bFilter: false, bInfo: false,
                            "bProcessing": true,
                            "bServerSide": true,
                            "sAjaxSource": "{{ URL::to('survey/getr') }}",
                            "fnDrawCallback": function (oSettings) {
                                $(".iframe").colorbox({iframe: true, width: "80%", height: "80%"});
                            }
                        });
                        oTable = $('#comments_table').dataTable({
                            "sDom": "<'row'<'col-md-12'l><'col-md-6'f>r>t<'row'<'col-md-12'i><'col-md-12'p>>",
                            "sPaginationType": "bootstrap",
                            "oLanguage": {
                                "sLengthMenu": ""
                            },
                            bFilter: false, bInfo: false,
                            "bProcessing": true,
                            "bServerSide": true,
                            "sAjaxSource": "{{ URL::to('survey/getc') }}",
                            "fnDrawCallback": function (oSettings) {
                                $(".iframe").colorbox({iframe: true, width: "80%", height: "80%"});
                            }
                        });
                    });

                </script>
            @stop

        </div>
    </div>

@stop
