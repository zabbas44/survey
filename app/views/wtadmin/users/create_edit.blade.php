@extends('wtadmin.layouts.modal')

{{-- Content --}}
@section('content')
<ol class="breadcrumb">
    <li> <a class="" target="_parent" href="{{{ URL::to('wtadmin/') }}}">Home</a></li>
    <li> <a class="" target="_parent" href="{{{ URL::to('wtadmin/users') }}}">List Management</a></li>
    <li class="last"> @if (!isset($user))
        Create New
        @else
        Update
        @endif  List</li>
</ol>
<!-- Tabs -->
<ul class="nav nav-tabs">
    <li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
</ul>
<!-- ./ tabs -->

{{-- Create User Form --}}
<form class="form-horizontal" method="post" action="@if (isset($user)){{ URL::to('wtadmin/users/' . $user->id . '/edit') }}@endif" autocomplete="off" enctype="multipart/form-data">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
    <!-- ./ csrf token -->

    <!-- Tabs Content -->
    <div class="tab-content">
        <!-- General tab -->
        <div class="tab-pane active" id="tab-general">
            <!-- username -->
            <div class="form-group {{{ $errors->has('username') ? 'error' : '' }}}">
                <label class="col-md-2 control-label" for="username">List Name</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" name="username" id="username" value="{{{ Input::old('username', isset($user) ? $user->username : null) }}}" />
                    {{ $errors->first('username', '<span class="help-inline">:message</span>') }}
                </div>
            </div>
            <!-- ./ username -->

            <!-- Email -->
            <div class="form-group {{{ $errors->has('email') ? 'error' : '' }}}">
                <label class="col-md-2 control-label" for="email">Upload File (csv file)</label>
                <div class="col-md-10">
                    <input class="form-control" type="file" name="file" id="email" value="{{{ Input::old('email', isset($user) ? $user->email : null) }}}" />
                    {{ $errors->first('email', '<span class="help-inline">:message</span>') }}
                </div>
            </div>
            <!-- ./ email -->

        </div>
        <!-- ./ general tab -->
    </div>
    <!-- ./ tabs content -->

    <!-- Form Actions -->
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <element class="btn btn-danger btn-cancel close_popup">Cancel</element>
            <button type="reset" class="btn btn-default">Reset</button>
            <button type="submit" class="btn btn-success">OK</button>
        </div>
    </div>
    <!-- ./ form actions -->
</form>
@stop
