<!DOCTYPE html>

<html lang="en">

<head id="Starter-Site">

    <meta charset="UTF-8">

    <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>
        @section('title')
            Administration
        @show
    </title>

    <meta name="keywords" content="@yield('keywords')"/>
    <meta name="author" content="@yield('author')"/>
    <!-- Google will often use this as its description of your page/site. Make it good. -->
    <meta name="description" content="@yield('description')"/>

    <!-- Speaking of Google, don't forget to set your site up: http://google.com/webmasters -->
    <meta name="google-site-verification" content="">

    <!-- Dublin Core Metadata : http://dublincore.org/ -->
    <meta name="DC.title" content="Project Name">
    <meta name="DC.subject" content="@yield('description')">
    <meta name="DC.creator" content="Umair Majeed">

    <!--  Mobile Viewport Fix -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <!-- This is the traditional favicon.
     - size: 16x16 or 32x32
     - transparency is OK
     - see wikipedia for info on browser support: http://mky.be/favicon/ -->
    <link rel="shortcut icon" href="{{{ asset('assets/ico/favicon.png') }}}">

    <!-- iOS favicons. -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144"
          href="{{{ asset('assets/ico/apple-touch-icon-144-precomposed.png') }}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114"
          href="{{{ asset('assets/ico/apple-touch-icon-114-precomposed.png') }}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72"
          href="{{{ asset('assets/ico/apple-touch-icon-72-precomposed.png') }}}">
    <link rel="apple-touch-icon-precomposed" href="{{{ asset('assets/ico/apple-touch-icon-57-precomposed.png') }}}">

    @if (!empty($favicon))
        <link rel="icon" {{ !empty($faviconType) ? 'type="' . $faviconType . '"' : '' }} href="{{ $favicon }}"/>
    @endif

    <link rel="stylesheet" href="{{asset('assets/vendor/bootstrap/dist/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/styles/roboto.css')}}">
    <link rel="stylesheet" href="{{asset('assets/styles/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('assets/styles/panel.css')}}">
    <link rel="stylesheet" href="{{asset('assets/styles/feather.css')}}">
    <link rel="stylesheet" href="{{asset('assets/styles/animate.css')}}">
    <link rel="stylesheet" href="{{asset('assets/styles/urban.css')}}">
    <link rel="stylesheet" href="{{asset('assets/styles/urban.skins.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/colorbox.css')}}">


    @yield('styles')

    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

</head>

<body>
<!-- Container -->
    <div id="container-fluid" class="app layout-fixed-header">

        @include('wtadmin.layouts.menu')
        @yield('content')


    <footer class="content-footer">

        <nav class="footer-right">
            <ul class="nav">
                <li>
                    <a href="javascript:;">Feedback</a>
                </li>
                <li>
                    <a href="javascript:;" class="scroll-up">
                        <i class="fa fa-angle-up"></i>
                    </a>
                </li>
            </ul>
        </nav>

        <nav class="footer-left">
            <ul class="nav">
                <li>
                    <a href="javascript:;">Copyright <i class="fa fa-copyright"></i> <span>Urban</span> 2015. All rights
                        reserved</a>
                </li>
                <li>
                    <a href="javascript:;">Careers</a>
                </li>
                <li>
                    <a href="javascript:;">
                        Privacy Policy
                    </a>
                </li>
            </ul>
        </nav>

    </footer>

    </div>


</div>
<!-- /bottom footer -->

<script src="{{asset('assets/scripts/extentions/modernizr.js')}}"></script>
<script src="{{asset('assets/vendor/jquery/dist/jquery.js')}}"></script>
<script src="{{asset('assets/vendor/bootstrap/dist/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/vendor/jquery.easing/jquery.easing.js')}}"></script>
<script src="{{asset('assets/vendor/fastclick/lib/fastclick.js')}}"></script>
<script src="{{asset('assets/vendor/onScreen/jquery.onscreen.js')}}"></script>
<script src="{{asset('assets/vendor/jquery-countTo/jquery.countTo.js')}}"></script>
<script src="{{asset('assets/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js')}}"></script>
<script src="{{asset('assets/scripts/ui/accordion.js')}}"></script>
<script src="{{asset('assets/scripts/ui/animate.js')}}"></script>
<script src="{{asset('assets/scripts/ui/link-transition.js')}}"></script>
<script src="{{asset('assets/scripts/ui/panel-controls.js')}}"></script>
<script src="{{asset('assets/scripts/ui/preloader.js')}}"></script>
<script src="{{asset('assets/scripts/ui/toggle.js')}}"></script>
<script src="{{asset('assets/scripts/urban-constants.js')}}"></script>
<script src="{{asset('assets/scripts/extentions/lib.js')}}"></script>
<script src="{{asset('assets/js/wysihtml5/wysihtml5-0.3.0.js')}}"></script>
<script src="{{asset('assets/js/wysihtml5/bootstrap-wysihtml5.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/datatables-bootstrap.js')}}"></script>
<script src="{{asset('assets/js/jquery.colorbox.js')}}"></script>
<script src="{{asset('assets/js/prettify.js')}}"></script>
<script src="{{asset('assets/datepicker/js/bootstrap-datepicker.js')}}"></script>


<!-- ./ Footer -->{{--



<!-- page level scripts -->
<script src="vendor/d3/d3.min.js"></script>
<script src="vendor/rickshaw/rickshaw.min.js"></script>
<script src="vendor/flot/jquery.flot.js"></script>
<script src="vendor/flot/jquery.flot.resize.js"></script>
<script src="vendor/flot/jquery.flot.categories.js"></script>
<script src="vendor/flot/jquery.flot.pie.js"></script>
<!-- /page level scripts -->

<!-- initialize page scripts -->
<script src="scripts/pages/dashboard.js"></script>
<!-- /initialize page scripts -->



        <!-- Javascripts -->
        <script src="{{asset('assets/js/jquery.min.js')}}"></script>
        <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assets/js/wysihtml5/wysihtml5-0.3.0.js')}}"></script>
        <script src="{{asset('assets/js/wysihtml5/bootstrap-wysihtml5.js')}}"></script>
        <script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('assets/js/datatables-bootstrap.js')}}"></script>
        <script src="{{asset('assets/js/datatables.fnReloadAjax.js')}}"></script>
        <script src="{{asset('assets/js/jquery.colorbox.js')}}"></script>
        <script src="{{asset('assets/js/prettify.js')}}"></script>
        <script src="{{asset('assets/datepicker/js/bootstrap-datepicker.js')}}"></script>

--}}

<script type="text/javascript">
    $('.wysihtml5').wysihtml5();
    $(prettyPrint);
    jQuery('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
    });

    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('#from_date').datepicker({
        onRender: function (date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#to_date')[0].focus();
    }).data('datepicker');
    var checkout = $('#to_date').datepicker({
        onRender: function (date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        checkout.hide();
    }).data('datepicker');

    $(".iframe_file").colorbox({iframe: true, width: "90%", height: "100%"});
</script>

@yield('scripts')

</body>

</html>
