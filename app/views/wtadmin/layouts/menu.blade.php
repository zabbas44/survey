
            @if (Auth::check())
               @else
                <ul class="nav navbar-nav pull-right">
                    <li class="hidden-sm"><img class="ajax-loader ajax-loader-lg" src="{{ asset('packages/mrjuliuss/syntara/assets/img/ajax-load.gif') }}" style="float: right;"/></li>
                    <li {{ (Request::is('/') ? ' class=" "' : '') }}><a href="{{{ URL::to('/') }}}"><span class="text">Home</span></a></li>
                    <li {{ (Request::is('info') ? ' class=" "' : '') }}><a href="{{{ URL::to('info') }}}"><span class="text">Overview</span></a></li>
                    <li {{ (Request::is('your-story') ? ' class=" "' : '') }}><a href="{{{ URL::to('your-story') }}}"><span class="text">Your Story</span></a></li>
                    <li {{ (Request::is('package') ? ' class=" "' : '') }}><a href="{{{ URL::to('package') }}}"><span class="text">Pricing</span></a></li>
                    <li {{ (Request::is('user/login') ? ' class=" "' : '') }}><a href="{{{ URL::to('user/login') }}}"><span class="text">Login</span></a></li>
                    <li {{ (Request::is('user/register') ? ' class="active"' : '') }}><a href="{{{ URL::to('user/create') }}}">Get Started</a></li>
                </ul>
            @endif


    <!-- sidebar panel -->
    <div class="sidebar-panel offscreen-left">

        <div class="brand">

            <!-- logo -->
            <div class="brand-logo">
                <img src="images/logo.png" height="15" alt="">
            </div>
            <!-- /logo -->

            <!-- toggle small sidebar menu -->
            <a href="javascript:;" class="toggle-sidebar hidden-xs hamburger-icon v3" data-toggle="layout-small-menu">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </a>
            <!-- /toggle small sidebar menu -->

        </div>

        <!-- main navigation -->
        <nav role="navigation">

            <ul class="nav">

                <!-- dashboard -->
                <li {{ (Request::is('') ? ' class="open"' : '')}}>
                    <a  href="{{{ URL::to('survey') }}}">
                        <i class="fa fa-flask"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li {{ (Request::is('survey/surveydetails') ? ' class="open"' : '') }} >
                    <a href="{{{ URL::to('survey/surveydetails') }}}">
                        <i class="fa fa-toggle-on"></i>
                          <span>Surveys </span>
                    </a>
                </li>

                <li{{ (Request::is('survey/lists') ? ' class="open"' : '') }}>
                    <a href="{{{ URL::to('survey/lists') }}}">
                        <i class="fa fa-list"></i>
                        <span> Lists Management </span>
                    </a>
                </li>

                <li class="divider-vertical"></li>
                <li{{ (Request::is('survey/package') ? ' class="open"' : '') }}>
                    <a href="{{{ URL::to('survey/package') }}}">
                        <i class="fa fa-list-alt"></i>
                        <span> Packages </span>
                    </a>
                </li>

                <li class="divider-vertical"></li>
                <li{{ (Request::is('survey/email') ? ' class="open"' : '') }}>
                    <a href="{{{ URL::to('survey/email') }}}">
                        <i class="fa fa-list-alt"></i>
                        <span> Email Configurations </span>
                    </a>
                </li>


            </ul>
        </nav>
        <!-- /main navigation -->

    </div>

    <!-- content panel -->
    <div class="main-panel">

        <!-- top header -->
        <header class="header navbar">

            <div class="brand visible-xs">
                <!-- toggle offscreen menu -->
                <div class="toggle-offscreen">
                    <a href="#" class="hamburger-icon visible-xs" data-toggle="offscreen" data-move="ltr">
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>
                </div>
                <!-- /toggle offscreen menu -->

                <!-- logo -->
                <div class="brand-logo">
                    <img src="images/logo-dark.png" height="15" alt="">
                </div>
                <!-- /logo -->
            </div>

            <ul class="nav navbar-nav hidden-xs">
                <li>
                    <p class="navbar-text">
                        Dashboard
                    </p>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right hidden-xs">

                <li>

                    <a href="javascript:;" data-toggle="dropdown">
                        <img src="{{{ URL::to('assets/images/avatar.jpg') }}}" class="header-avatar img-circle ml10" alt="user" title="user">
                        <span class="pull-left">{{{ Auth::user()->username }}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{{ URL::to('user/settings') }}}"><span class="glyphicon glyphicon-cog"></span> Settings</a></li>
                        <li>
                            <a href="{{{ URL::to('survey/package') }}}"><span class="glyphicon glyphicon-off"></span> Upgrade</a>
                        </li>
                        <li><a href="{{{ URL::to('user/logout') }}}"><span class="glyphicon glyphicon-off"></span> Logout</a></li>
                        </ul>

                </li>


            </ul>
        </header>

