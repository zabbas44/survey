@extends('wtadmin.layouts.default')

{{-- Content --}}
@section('content')


    <br/>
    <br/>
    <br/>
    <ol class="breadcrumb no-bg">
        <li><a class="" target="_parent" href="{{{ URL::to('survey/') }}}">Home</a></li>
        <li><a class="" target="_parent" href="{{{ URL::to('survey/') }}}">Dashboard</a></li>
    </ol>

    <div class="page" style="padding-bottom: 50px;">
        <div class="pull-right">
            <a href="{{{ URL::to('survey/create') }}}" class="btn btn-small btn-info"><span
                        class="glyphicon glyphicon-plus-sign"></span> Create Survey</a>&nbsp;&nbsp;
        </div>
    </div>



            <div class="col-lg-12">
                <div class="box-tab justified">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#home4" aria-expanded="false">Latest Surveys</a>
                        </li>
                        <li class=""><a data-toggle="tab" href="#profile4" aria-expanded="false">Latest Responses</a>
                        </li>
                        <li class=""><a data-toggle="tab" href="#about4" aria-expanded="true">Latest Comments</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="home4" class="tab-pane active">

                            <div class="panel panel-default">
                                <div class="panel-heading">Displaying All Surveys</div>
                                <div class="panel-body">

                                    <table id="roles" class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-3">{{{ Lang::get('wtadmin/survey/table.d_created_at') }}}</th>
                                            <th class="col-md-7">{{{ Lang::get('wtadmin/survey/table.d_survey_name') }}}</th>
                                            <th class="col-md-2"> </th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>

                                </div>
                            </div>

                        </div>
                        <div id="profile4" class="tab-pane">

                            <div class="panel panel-default">
                                <div class="panel-heading">Displaying All Responses</div>
                                <div class="panel-body">

                                    <table id="responses" class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1">{{{ Lang::get('wtadmin/survey/table.d_created_at') }}}</th>
                                            <th class="col-md-4">{{{ Lang::get('wtadmin/survey/table.d_survey_name') }}}</th>
                                            <th class="col-md-4">Question</th>
                                            <th class="col-md-2">{{{ Lang::get('wtadmin/survey/table.d_survey_anwer') }}}</th>
                                            <th class="col-md-2">Details</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>

                                </div>

                            </div>

                        </div>

                        <div id="about4" class="tab-pane">

                            <div class="panel panel-default">
                                <div class="panel-heading">Displaying All Comments</div>
                                <div class="panel-body">

                                    <table id="comments_table" class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-3">{{{ Lang::get('wtadmin/survey/table.d_created_at') }}}</th>
                                            <th class="col-md-3">{{{ Lang::get('wtadmin/survey/table.d_s_name') }}}</th>
                                            <th class="col-md-3">{{{ Lang::get('wtadmin/survey/table.d_s_comment') }}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>



            {{-- Scripts --}}
            @section('scripts')
                <script type="text/javascript">
                    var oTable;
                    $(document).ready(function () {
                        oTable = $('#roles').dataTable({
                            "sDom": "<'row'<'col-md-12'l><'col-md-6'f>r>t<'row'<'col-md-12'i><'col-md-12'p>>",
                            "sPaginationType": "bootstrap",
                            "oLanguage": {
                                "sLengthMenu": ""
                            },
                            bFilter: false, bInfo: false,
                            "bProcessing": true,
                            "bServerSide": true,
                            "sAjaxSource": "{{ URL::to('survey/get') }}",
                            "fnDrawCallback": function (oSettings) {
                                $(".iframe").colorbox({iframe: true, width: "80%", height: "80%"});
                            }
                        });

                        oTable = $('#responses').dataTable({
                            "sDom": "<'row'<'col-md-12'l><'col-md-6'f>r>t<'row'<'col-md-12'i><'col-md-12'p>>",
                            "sPaginationType": "bootstrap",
                            "oLanguage": {
                                "sLengthMenu": ""
                            },
                            bFilter: false, bInfo: false,
                            "bProcessing": true,
                            "bServerSide": true,
                            "sAjaxSource": "{{ URL::to('survey/getr') }}",
                            "fnDrawCallback": function (oSettings) {
                                $(".iframe").colorbox({iframe: true, width: "80%", height: "80%"});
                            }
                        });
                        oTable = $('#comments_table').dataTable({
                            "sDom": "<'row'<'col-md-12'l><'col-md-6'f>r>t<'row'<'col-md-12'i><'col-md-12'p>>",
                            "sPaginationType": "bootstrap",
                            "oLanguage": {
                                "sLengthMenu": ""
                            },
                            bFilter: false, bInfo: false,
                            "bProcessing": true,
                            "bServerSide": true,
                            "sAjaxSource": "{{ URL::to('survey/getc') }}",
                            "fnDrawCallback": function (oSettings) {
                                $(".iframe").colorbox({iframe: true, width: "80%", height: "80%"});
                            }
                        });
                    });

                </script>
            @stop

        </div>
    </div>

@stop
