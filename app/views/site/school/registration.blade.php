@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('school/school.register') }}} ::
@parent
@stop

{{-- Content --}}
@section('content')
<div id="register_user">
    <a class="btn btn-info btn-new" href="{{ URL::route('indexDashboard') }}">Go Back</a>
    <button data-dismiss="alert" class="close" type="button"></button>
    <div class="panel panel-success margin-top-20px"> 
    <!-- Default panel contents -->
    <div class="panel-heading">Success! Registration Completed</div>
    <div class="panel-body">
      <table cellpadding="10" class="table">
        <tr>
          <td width="200"><strong>survey Name</strong></td>
          <td>:</td>
          <td align="left"><?php echo $school->name; ?></td>
        </tr>
        <tr>
          <td><strong>Email Address</strong></td>
          <td>:</td>
          <td align="left"><?php echo $school->email; ?></td>
        </tr>
        
        <tr>
          <td><strong>Admin Username</strong></td>
          <td>:</td>
          <td align="left"><?php echo $school->username; ?></td>
        </tr>
      </table>
    </div>
  </div>
</div>
@stop