@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('store/store.register') }}} ::
@parent
@stop

{{-- Content --}}
@section('content')
<div>
<div class="panel panel-success margin-top-20px">
<!-- Default panel contents -->
<div class="panel-heading">Signup</div>
<div class="panel-body">
  <form method="POST" action="{{{ (Confide::checkAction('StoreAdminController@store')) ?URL::to('/signup'):  '' }}}" accept-charset="UTF-8">
    <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">
    <fieldset>
      <div class="form-group {{{ $errors->has('name') ? 'error' : '' }}}">
        <label for="name">{{{ Lang::get('store/store.name') }}}</label>
        <input class="form-control" placeholder="{{{ Lang::get('store/store.name') }}}" type="text" name="name" id="name" value="{{{ Input::old('name') }}}" />
        {{ $errors->first('name', '<span class="help-inline">:message</span>') }} </div>
      <div class="form-group {{{ $errors->has('sub_domain') ? 'error' : '' }}}">
        <label for="sub_domain">{{{ Lang::get('store/store.sub_domain') }}}</label>
        <input class="form-control" placeholder="{{{ Lang::get('store/store.sub_domain') }}}" type="text" name="sub_domain" id="sub_domain" value="{{{ Input::old('sub_domain') }}}">
        {{ $errors->first('sub_domain', '<span class="help-inline">:message</span>') }} </div>
      <div class="form-group {{{ $errors->has('email') ? 'error' : '' }}}">
        <label for="email">{{{ Lang::get('confide::confide.e_mail') }}} <small>{{ Lang::get('confide::confide.signup.confirmation_required') }}</small></label>
        <input class="form-control" placeholder="{{{ Lang::get('confide::confide.e_mail') }}}" type="text" name="email" id="email" value="{{{ Input::old('email') }}}">
        {{ $errors->first('email', '<span class="help-inline">:message</span>') }} </div>
      <div class="form-group {{{ $errors->has('user_name') ? 'error' : '' }}}">
        <label for="user_name">{{{ Lang::get('store/store.user_name') }}}</label>
        <input class="form-control" placeholder="{{{ Lang::get('store/store.user_name') }}}" type="text" name="user_name" id="user_name" value="{{{ Input::old('user_name') }}}">
        {{ $errors->first('user_name', '<span class="help-inline">:message</span>') }} </div>
      <div class="form-group {{{ $errors->has('password') ? 'error' : '' }}}">
        <label for="password">{{{ Lang::get('confide::confide.password') }}}</label>
        <input class="form-control" placeholder="{{{ Lang::get('confide::confide.password') }}}" type="password" name="password" id="password">
        {{ $errors->first('password', '<span class="help-inline">:message</span>') }} </div>
      <div class="form-group {{{ $errors->has('password_confirmation') ? 'error' : '' }}}">
        <label for="password_confirmation">{{{ Lang::get('confide::confide.password_confirmation') }}}</label>
        <input class="form-control" placeholder="{{{ Lang::get('confide::confide.password_confirmation') }}}" type="password" name="password_confirmation" id="password_confirmation">
        {{ $errors->first('password_confirmation', '<span class="help-inline">:message</span>') }} </div>
      @if ( Session::get('error') )
      <div class="alert alert-error alert-danger"> @if ( is_array(Session::get('error')) )
        {{ head(Session::get('error')) }}
        @endif </div>
      @endif
      
      @if ( Session::get('notice') )
      <div class="alert">{{ Session::get('notice') }}</div>
      @endif
      <div class="form-actions form-group">
        <button type="submit" class="btn btn-primary">{{{ Lang::get('store/store.submit') }}}</button>
      </div>
    </fieldset>
  </form>
</div>
@stop 