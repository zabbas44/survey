@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('store/store.register') }}} ::
@parent
@stop

{{-- Content --}}
@section('content')
<div>
<div class="panel panel-success margin-top-20px">
<!-- Default panel contents -->
<div class="panel-heading">{{{$school->name}}} Update</div>
<div class="panel-body">
  <form method="POST" action="" accept-charset="UTF-8">
    <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">
    <fieldset>
      <div class="form-group {{{ $errors->has('store_name') ? 'error' : '' }}}">
        <label for="store_name">{{{ Lang::get('store/store.store_name') }}}</label>
        <input class="form-control" placeholder="{{{ Lang::get('store/store.store_name') }}}" type="text" name="store_name" id="store_name" value="{{{ Input::old('store_name') }}}" />
        {{ $errors->first('store_name', '<span class="help-inline">:message</span>') }} 
      </div>
      
      <div class="form-group {{{ $errors->has('store_address') ? 'error' : '' }}}">
        <label for="store_name">{{{ Lang::get('store/store.store_address') }}}</label>
        <input class="form-control" placeholder="{{{ Lang::get('store/store.store_address') }}}" type="text" name="store_address" id="store_address" value="{{{ Input::old('store_address') }}}" />
        {{ $errors->first('store_address', '<span class="help-inline">:message</span>') }} 
      </div>
      <div class="form-group {{{ $errors->has('store_phone') ? 'error' : '' }}}">
        <label for="store_name">{{{ Lang::get('store/store.store_phone') }}}</label>
        <input class="form-control" placeholder="{{{ Lang::get('store/store.store_phone') }}}" type="text" name="store_phone" id="store_phone" value="{{{ Input::old('store_phone') }}}" />
        {{ $errors->first('store_phone', '<span class="help-inline">:message</span>') }} 
      </div>
      
        @if ( Session::get('error') )
      <div class="alert alert-error alert-danger"> @if ( is_array(Session::get('error')) )
        {{ head(Session::get('error')) }}
        @endif </div>
      @endif
      
      @if ( Session::get('notice') )
      <div class="alert">{{ Session::get('notice') }}</div>
      @endif
      <div class="form-actions form-group">
        <button type="submit" class="btn btn-primary">{{{ Lang::get('store/store.store_submit') }}}</button>
      </div>
    </fieldset>
  </form>
</div>
@stop 