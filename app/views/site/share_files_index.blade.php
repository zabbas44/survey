@extends('site.layouts.cate_default')

{{-- Web site Title --}}
@section('title')
{{{ $title }}} :: @parent
@stop

{{-- Content --}}
@section('content')

<div class="page-header">
    <h3>{{{ $con_title }}}
    <div class="pull-right ">
        
    </div>
    </h3>
</div>

<ol class="breadcrumb">
    @if($flag)
            <li> <a class="" target="_parent" href="{{{ URL::to('wtadmin/') }}}">Home</a></li>
            <li> <a class="" target="_parent" href="{{{ URL::to('wtadmin/users') }}}">Users Management</a></li>
            <li class="last"> <a class="" target="_parent" href="{{{ URL::to('wtadmin/user/dashboard/'.$user_id) }}}">Admin :  {{{$name}}} Dashboard</a></li>
            <li class="last"> {{{ $title }}}</li>
        @else
            <li> <a class="" target="_parent" href="{{{ URL::to('dashboard') }}}">Dashboard</a></li>
            <li class="last"> {{{ $title }}}</li>
        @endif
    
</ol>
<table id="pages" class="table table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th class="col-md-1">Shared User</th>
            <th class="col-md-1">Title</th>
            <th class="col-md-1">Description</th>
            <th class="col-md-1">File Name</th>
            <th class="col-md-1">File Type</th>
            <th class="col-md-1">File Size</th>
            <th class="col-md-1">Creation Date</th>
            <th class="col-md-1">Action</th>
        </tr>
    </thead>
    <tbody>

        @foreach($pages as $k => $val)
        <?php
        
        $size = @sizeFormat(@filesize(ltrim($val['path'], '/'))); 
               $size = (int)$size;
               if($size>0){
                   $type = '';
                if ($val['is_video'] == 1)
                    $type = 'Video';
                if ($val['is_audio'] == 1)
                    $type = 'Audio';
                if ($val['is_image'] == 1)
                    $type = 'Image';
                if ($val['is_doc'] == 1)
                    $type = 'Text/Document';
                
        ?>
        <tr>
            <td class="col-md-1">{{{ucwords($val['username'])}}}</td>
            <td class="col-md-1">{{{$val['title']}}}</td>
            <td class="col-md-1">{{{strip_tags(html_entity_decode($val['description']))}}}</td>
            <td class="col-md-1">{{{$val['file_name']}}}</td>
            <td class="col-md-1">{{{$type}}}</td>
            <td class="col-md-1"><?php  echo @sizeFormat(@filesize(ltrim($val['path'], '/'))); ?></td>
            
            <td class="col-md-1">{{{$val['created_at']}}}</td>
            <td class="col-md-2">
                <a href="{{{ URL::to('/file/view/'.$val['imv_id']) }}}" class="iframe_file btn btn-xs btn-default">View</a>
                <a href="{{{ URL::to('/share_file/'.$val['imv_id']) }}}" class="iframe_file btn btn-xs btn-success">Share</a>
                <a href="/download/<?= $val['imv_id'] ?>" class="btn btn-xs btn-info">Download</a>
            </td>
        </tr>
            <?php }?>
        @endforeach
    </tbody>
</table>
@stop

{{-- Scripts --}}
@section('scripts')
<script type="text/javascript">
  var oTable;
  $(document).ready(function() {
        $('#pages').dataTable();
        $(".iframe").colorbox({iframe: true, width: "80%", height: "95%"});
    });
    function ref_page(){
        window.location = '/wtadmin';
    }
</script>
@stop