<!-- Navbar -->
<div class="navbar main-bar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li {{ (Request::is('/') ? ' class="active"' : '') }}><a href="{{ URL::to('/') }}" ><strong></strong></a> </li>
            </ul>
            <ul class=" nav navbar-nav navbar-{{ (Config::get('syntara::config.direction') === 'rtl') ? 'left' : 'right' }} ">

                @if (Auth::check())

                <li><a href="{{{ URL::to('/survey') }}}">Surveys</a></li>
                <li><a href="{{{ URL::to('user') }}}">Reset Password</a></li>
                <li><a href="{{{ URL::to('/') }}}">{{{ Auth::user()->username }}}</a></li>
                <li><a href="{{{ URL::to('user/logout') }}}">Logout</a></li>
                @else
                {{--<li class="hidden-sm"><img class="ajax-loader ajax-loader-lg" src="{{ asset('packages/mrjuliuss/syntara/assets/img/ajax-load.gif') }}" style="float: right;"/></li>
                <li {{ (Request::is('/') ? ' class=" "' : '') }}><a href="{{{ URL::to('/') }}}"><span class="text">Home</span></a></li>
                <li {{ (Request::is('info') ? ' class=" "' : '') }}><a href="{{{ URL::to('info') }}}"><span class="text">Overview</span></a></li>
                <li {{ (Request::is('your-story') ? ' class=" "' : '') }}><a href="{{{ URL::to('your-story') }}}"><span class="text">Your Story</span></a></li>
                <li {{ (Request::is('package') ? ' class=" "' : '') }}><a href="{{{ URL::to('package') }}}"><span class="text">Pricing</span></a></li>
                <li {{ (Request::is('user/login') ? ' class=" "' : '') }}><a href="{{{ URL::to('user/login') }}}"><span class="text">Login</span></a></li>
                <li {{ (Request::is('user/register') ? ' class="active"' : '') }}><a href="{{{ URL::to('user/create') }}}">Get Started</a></li> 
                --}}@endif
            </ul>
            <!-- ./ nav-collapse --> 
        </div>
    </div>
</div>
<!-- ./ navbar --> 
