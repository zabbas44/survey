<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8" />
        <title>@section('title')
            Survey Management System
            @show
        </title>
        <meta name="keywords" content="your, awesome, keywords, here" />
        <meta name="author" content="parth patel" />
        <meta name="description" content="Survey management system." />

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- CSS
                        ================================================== -->
<!--        <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap-theme.min.css')}}">-->
        <!--<link rel="stylesheet" href="{{asset('bootstrap/css/custom.css')}}">-->


        <link rel="stylesheet" href="{{ asset('packages/mrjuliuss/syntara/assets/layout/bootstrap.min.css') }}" />

        <link rel="stylesheet" href="{{ asset('packages/mrjuliuss/syntara/assets/css/toggle-switch.css') }}" />

        <link rel="stylesheet" href="{{ asset('packages/mrjuliuss/syntara/assets/css/base.css') }}" media="all">

        <link rel="stylesheet" href="{{asset('assets/vendor/perfect-scrollbar/css/perfect-scrollbar.css')}}">
        <link rel="stylesheet" href="{{asset('assets/styles/roboto.css')}}">
        <link rel="stylesheet" href="{{asset('assets/styles/font-awesome.css')}}">
        <link rel="stylesheet" href="{{asset('assets/styles/panel.css')}}">
        <link rel="stylesheet" href="{{asset('assets/styles/feather.css')}}">
        <link rel="stylesheet" href="{{asset('assets/styles/animate.css')}}">
        <link rel="stylesheet" href="{{asset('assets/styles/urban.css')}}">
        <link rel="stylesheet" href="{{asset('assets/styles/urban.skins.css')}}">

        @if (!empty($favicon))
        <link rel="icon" {{ !empty($faviconType) ? 'type="' . $faviconType . '"' : '' }} href="{{ $favicon }}" />
        @endif

        
        <style>
            @section('styles') @show
        </style>

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
                        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
                        <![endif]-->

        <!-- Favicons
                        ================================================== -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{{ asset('assets/ico/apple-touch-icon-144-precomposed.png') }}}">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{{ asset('assets/ico/apple-touch-icon-114-precomposed.png') }}}">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{{ asset('assets/ico/apple-touch-icon-72-precomposed.png') }}}">
        <link rel="apple-touch-icon-precomposed" href="{{{ asset('assets/ico/apple-touch-icon-57-precomposed.png') }}}">
        <link rel="shortcut icon" href="{{{ asset('assets/ico/favicon.png') }}}">
    </head>

    <body>
       
        <!-- To make sticky footer need to wrap in a div -->
        <div id="wrap">
            <!-- Navbar --> 
            @include('site.layouts.menu') 
            <!-- ./ navbar --> 

            <!-- Container -->
            <div class="content">
                <!-- Notifications --> 
                @include('notifications') 
                <!-- ./ notifications --> 
                {{ isset($breadcrumb) ? Breadcrumbs::create($breadcrumb) : ''; }}
                <!-- Content --> 
                @yield('content') 
                <!-- ./ content -->
                <!-- the following div is needed to make a sticky footer -->
                <div id="push"></div>
            </div>
        </div>
        <!-- ./wrap -->


        <div id="footer" style="margin-top:50px">
            <div class="container" style="">
                <div class="lg-col-12 row">

                    <div style="margin:auto;width:380px;">
                        <center>
                        Copyright &copy; Survey Management system
                        </center>
                    </div>
                </div>
            </div>
        </div>

        <!-- Javascripts================================================== --> 
        <script src="{{ asset('packages/mrjuliuss/syntara/assets/layout/jquery.min.js') }}"></script>
        <script src="{{ asset('packages/mrjuliuss/syntara/assets/layout/bootstrap.min.js') }}"></script>
        <script src="{{ asset('packages/mrjuliuss/syntara/assets/js/dashboard/base.js') }}"></script>
        
        <link rel="stylesheet" href="{{asset('assets/css/datatables-bootstrap.css')}}">
        <script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('assets/js/datatables-bootstrap.js')}}"></script>
        <script src="{{asset('assets/js/datatables.fnReloadAjax.js')}}"></script>
        
        @yield('scripts')
    </body>
</html>
