@extends('site.layouts.default')

{{-- Content --}}
@section('content')
<div class="col-lg-12" style="float:left;">
    <br />
    <ol class="breadcrumb">
        <li class="last"> Billing</li>

    </ol>
</div>
<div class="col-lg-12" style="float:left;">


    <div class="panel panel-info">
        <!-- Default panel contents -->
        <div class="panel-heading">Package information </div>
        <div class="panel-body">
            <p>...</p>
        </div>

        <!-- Table -->
        <table class="table">
            ...
        </table>
    </div>

    <div class="panel panel-info">
        <!-- Default panel contents -->
        <div class="panel-heading">Billing/Subscriptions information</div>
        <div class="panel-body">
            <p>...</p>
        </div>

        <!-- Table -->
        <table class="table">
            ...
        </table>
    </div>

</div>
@stop
