@extends('site.layouts.view_modal')

{{-- Content --}}
@section('content')

<!-- Tabs -->

<?php
$timestamp = time();
$hash = md5('unique_salt' . $timestamp);
$oldhash = $postImgVdo->hash;
$csrf_token = csrf_token();
$sch_id = '';
?>
<input type="hidden" name="timestamp" id="timestamp" value="<?php echo $timestamp; ?>" />
<input type="hidden" name="hash" id="hash" value="<?php echo $hash; ?>" />
<input type="hidden" name="oldhash" id="oldhash" value="<?php echo $oldhash; ?>" />
<input type="hidden" name="path" id="path" value="<?php echo $sch_id . '/' . Auth::user()->id; ?>" />
<input type="hidden" name="_token" id="_token" value="{{{ $csrf_token }}}" />
<ol class="breadcrumb">
    <li> <a class="" target="_parent" href="{{{ URL::to('wtadmin/') }}}">Home</a></li>
    <li class="last">Share File</li>
    <li class="last"> {{{ $title }}}</li>
</ol>
<ul class="nav nav-tabs">
    <li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
</ul>
<!-- ./ tabs -->

{{-- Edit Content Form --}}

<!-- Tabs Content -->
<div class="tab-content">
    <!-- Tab General -->
    <div class="block col-xs-12 tab-pane active" id="tab-general">
        <div class="form-group">
            <div class="col-md-12">

                <button type="button" id="std_id" class="btn btn-large btn-info" >Users</button>

            </div>
        </div>

        <div class="clear clear_fix"></div>
        <div class="form-group">

            <div class="col-md-12">
                <br />
                <span id="error_on_delete" class="info error"></span>
            </div>
        </div>
    </div>

    <div class="block col-xs-12 tab-pane active">
        <!-- Tab Student Wise-->
        <div class="tab-pane1" id="tab-student">
            <form id="txt_frm" class="form-horizontal" method="post" action="" autocomplete="off" enctype="multipart/form-data">
                <!-- CSRF Token -->
                <input type="hidden" name="_token" value="{{{ $csrf_token }}}" />
                <input type="hidden" name="is_std_wise" id="is_std_wise" value="1" />
                <!-- ./ csrf token -->

                <!-- Name -->
                <div class="form-group {{{ $errors->has('title') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="title">Title</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="title" id="title" value="{{{ Input::old('title',$data['title']) }}}" />
                        {{ $errors->first('title', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <!-- ./ name -->
               <!-- description -->
                <div class="form-group {{{ $errors->has('description') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="description">Description</label>
                    <div class="col-md-10">
                        <textarea class="form-control full-width wysihtml5" name="description" value="description" rows="5">{{{ Input::old('description',$data['description']) }}}</textarea>
                        {{ $errors->first('description', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <!-- ./ description -->

                <!-- description -->
                <div class="form-group {{{ $errors->has('users') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="users">Users</label>
                    <div class="col-md-10 {{{ $errors->has('users') ? 'error' : '' }}}">
                        {{ $errors->first('users', '<span class="help-inline">:message</span>') }}
                        <select name="users[]" class="searchable" multiple="multiple" >
                            <?php
                            $members = explode(",", $data['selectedUsers']);
                            foreach ($data['users'] as $key => $val) {
                                if (in_array($val['id'], $members))
                                    echo '<option selected="" value="' . $val['id'] . '">' . ucfirst($val['username']) . '</option>';
                                else
                                    echo '<option value="' . $val['id'] . '">' . ucfirst($val['username']) . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <!-- ./ description -->



                <!-- Form Actions -->
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <button class="btn btn-danger close_popup">Cancel</button>
                        <button type="reset" class="btn btn-default">Reset</button>
                        <button type="submit" class="btn btn-success">Share Content</button>
                    </div>
                </div>
                <!-- ./ form actions -->
            </form>
        </div>
        <!-- Tab Student Wise-->
    </div>
</div>
<!-- ./ tabs content -->
@stop