@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('user/user.forgot_password') }}} ::
@parent
@stop

{{-- Content --}}
@section('content')
<div class="col-lg-12" style="float:left;">
    <br />
    <ol class="breadcrumb">
        <li class=""> Home</li>
        <li class="last"> Reset Password</li>
    </ol>
</div>
<div class="col-lg-12" style="float:left;">
<div class="page-header">
	<h1>Forgot Password</h1>
</div>
{{ Confide::makeResetPasswordForm($token)->render() }}
</div>

@stop
