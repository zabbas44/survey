@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('user/user.forgot_password') }}} ::
@parent
@stop

@section('content')

    <div class="app layout-fixed-header usersession bg-white" style="padding-top: 100px;padding-bottom: 20px;">
        <div class="full-height">
            <div class="center-wrapper">
                <div class="center-content">
                    <div class="row no-margin">
                        <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                                <p class="text-center mb25">Lost your password? Please enter your email address. You will receive a link to create a new password.</p>
                                <div class="form-inputs">

                                    {{ Confide::makeForgotPasswordForm() }}

                                </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@stop
