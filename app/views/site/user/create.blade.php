@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('user/user.register') }}} ::
@parent
@stop

{{-- Content --}}
@section('content')


<div class="app layout-fixed-header bg-white usersession" style="padding-top: 100px;">
    <div class="full-height">
        <div class="center-wrapper">
            <div class="center-content">
                <div class="row no-margin">
                    <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                        <form method="POST" action="{{{ (Confide::checkAction('UserController@store')) ?: URL::to('user')  }}}" accept-charset="UTF-8" id="signupform" class="form-layout" role="form">
                            <input type="hidden" name="_token" value="{{{ Session::getToken() }}}" />

                            <p class="text-center mb30">Create your account.</p>

                            @if ( Session::get('error') )
                                <div id="signupalert" style="display:none" class="alert alert-danger">
                                    <p>Error:</p>
                                        <span>@if ( is_array(Session::get('error')) )
                                                {{ head(Session::get('error')) }}
                                            @endif
                                        </span>
                                </div>
                            @endif
                            @if ( Session::get('notice') )
                                <div class="alert">{{ Session::get('notice') }}</div>
                            @endif


                            <div class="form-inputs">
                                <input type="text" class="form-control" name="first_name" placeholder="First Name" value="{{{Input::old('first_name')}}}" />
                                {{ $errors->first('first_name', '<span class="help-inline">:message</span>') }}

                                <input type="text" placeholder="Email address" name="email" class="form-control input-lg">
                                {{ $errors->first('email', '<span class="help-inline">:message</span>') }}


                                <div class="form-group  {{{ $errors->has('last_name') ? 'error' : '' }}}">
                                        <input type="text" class="form-control" name="last_name" placeholder="Last Name" value="{{{Input::old('last_name')}}}" />
                                        {{ $errors->first('last_name', '<span class="help-inline">:message</span>') }}
                                </div>
                                <div class="form-group  {{{ $errors->has('password') ? 'error' : '' }}}">
                                        <input class="form-control" type="password" name="password" id="password" placeholder="Password" value="{{{Input::old('password')}}}" />
                                        {{ $errors->first('password', '<span class="help-inline">:message</span>') }}
                                </div>

                                <div class="form-group  {{{ $errors->has('password_confirmation') ? 'error' : '' }}}">
                                        <input class="form-control" placeholder="{{{ Lang::get('confide::confide.password_confirmation') }}}" type="password" name="password_confirmation" id="password_confirmation" />
                                        {{ $errors->first('password_confirmation', '<span class="help-inline">:message</span>') }}
                                </div>

                                <div class="form-group  {{{ $errors->has('company_name') ? 'error ' : '' }}}">
                                        <input type="text" class="form-control" name="company_name" placeholder="Company Name" value="{{{Input::old('company_name')}}}" />
                                        {{ $errors->first('company_name', '<span class="help-inline">:message</span>') }}
                                </div>


                            </div>

                            <button type="submit" class="btn btn-success btn-block btn-lg mb15">Create Account</button>

                            <p class="text-left">By clicking signin up, you agree to our <a href="javascript:;">Terms of services &amp; Policies</a>.</p>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
@stop
