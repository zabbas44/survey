@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('user/user.profile') }}} ::
@parent
@stop

{{-- Content --}}
@section('content')
<div class="col-lg-12" style="float:left;">
    <br />
    <ol class="breadcrumb no-bg">
        <li class=""> Home</li>
        <li class="last"> Reset Password</li>
    </ol>
</div>
<div class="col-lg-12" style="float:left;">
    <div class="page-header">

        <div class="col-lg-12">
            <div class="panel panel-info">
                <!-- Default panel contents -->
                <div class="panel-heading">User profile information</div>
                <div class="panel-body">

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Username</th>
                            <th>Signed Up</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{{$user->id}}}</td>
                            <td>{{{$user->username}}}</td>
                            <td>{{{$user->joined()}}}</td>
                        </tr>
                        </tbody>
                    </table>


                </div>
                </div>

</div>
@stop
