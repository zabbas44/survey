@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('user/user.login') }}} ::
@parent
@stop

{{-- Content --}}
@section('content')

    <div class="row">
    <div class="app layout-fixed-header bg-white usersession" style="padding-top: 100px;">
        <div class="full-height">
            <div class="center-wrapper">
                <div class="center-content">
                    <div class="row no-margin">
                        <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                            <form class="form-layout" method="POST" action="{{ URL::to('user/login') }}" accept-charset="UTF-8">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <p class="text-center mb30">Welcome to Survey. Please sign in to your account</p>
                                <div class="form-inputs">
                                    <input type="email" name="email" placeholder="Email Address" class="form-control input-lg">
                                    @if($errors->has('email'))
                                        <div style="margin-bottom:10px;color: red;" class="{{{ $errors->has('email') ? 'error' : '' }}}">
                                            {{ $errors->first('email', '<span class="help-inline">:message</span>') }}
                                        </div>
                                    @endif

                                    <input type="password" name="password" placeholder="Password" class="form-control input-lg">

                                    @if($errors->has('password'))
                                        <div style="margin-bottom:10px;color: red;" class="{{{ $errors->has('password') ? 'error' : '' }}}">
                                            {{ $errors->first('password', '<span class="help-inline">:message</span>') }}
                                        </div>
                                    @endif


                                </div>
                                <button type="submit" class="btn btn-success btn-block btn-lg mb15">
                                    <span>Sign in</span>
                                </button>
                                <p>
                                    <a href="{{ URL::to('user/create') }}">Create an account</a> ·
                                    <a href="forgot">Forgot your password?</a>
                                </p>

                                @if ( Session::get('error') )
                                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                                @endif

                                @if ( Session::get('notice') )
                                    <div class="alert alert-success">{{ Session::get('notice') }}</div>
                                @endif

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
@stop
