@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('user/user.login') }}} ::
@parent
@stop

{{-- Content --}}
@section('content')
<div class="page-header">
	<h1>Login into your account</h1>
</div>

<div class="well">
    <h4 class="vertical-label hide">Search Your School</h4>

    <form  action="" id="signup-form" role="form" method="post">
        <div class="form-group">
            <div class="input-group">
                <input type="hidden" value="utility_3" name="account[plan]">

                <input type="text" title="Subdomain may only contain lowercase letters [a-z] and no spaces or special characters." pattern="^[a-z]+[a-z0-9\-]*?[a-z0-9]$" required="true" placeholder="Subdomain" name="subdomain" class="form-control" id="n-subdomain">
                <span class="input-group-addon">.datasnap.co.uk</span>
            </div>
        </div>
        <!--        <div class="form-group">
                    <input type="email" required="true" placeholder="Email Address or Username" name="admin[email]" class="form-control">
            <input type="text" required="true" placeholder="Email Address or Username" name="email" class="form-control">
        </div>
                <div class="form-group">
                   <input type="text" title="Username may only contain lowercase letters [a-z] and no spaces or special characters" pattern="^[a-z]+[a-z0-9]*$" required="true" placeholder="Username" name="admin[username]" class="form-control">
                </div>

        <div class="form-group">
            <input type="password" required="true" placeholder="Password" name="password" class="form-control">
                </div>-->

        <input type="hidden" value="1" name="tos_agree">

        <p style="margin-bottom:0; font-size:0.9em">
            <button class="btn btn-success pull-right" style="margin:0 0 0 10px" id="signup-form-submit" type="submit">Search</button>
        </p>

        <div class="clearfix"></div>

        <div class="signup-form-info"></div>

    </form>

</div>

@stop
