@extends('site.layouts.cate_default')

@if (Auth::check())
{{-- Web site Title --}}
@section('title')
{{{ $title }}} :: @parent
@stop

{{-- Content --}}
@section('content')

<div class="page-header">
    
</div>

<div class="col-lg-12" style="float:left;">
    <br />
    <ol class="breadcrumb">
        @if($flag)
            <li> <a class="" target="_parent" href="{{{ URL::to('wtadmin/') }}}">Home</a></li>
            <li> <a class="" target="_parent" href="{{{ URL::to('wtadmin/users') }}}">Users Management</a></li>
            <li class="last"> <a class="" target="_parent" href="{{{ URL::to('wtadmin/user/dashboard/'.$user_id) }}}">Admin :  {{{$name}}} Dashboard</a></li>
            <li class="last"> {{{ $title }}}</li>
        @else
            <li> <a class="" target="_parent" href="{{{ URL::to('dashboard') }}}">Dashboard</a></li>
            <li class="last"> {{{ $title }}}</li>
        @endif
    </ol>
</div>
<div class="col-lg-12" style="float:left;">
    <table id="pages" class="table table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th class="col-md-1">Title</th>
            <th class="col-md-1">Category</th>
            <th class="col-md-1">File Type</th>
            <th class="col-md-1">File Size</th>
            <th class="col-md-1">Creation Date</th>
            <th class="col-md-1">Comments</th>
            <th class="col-md-1">Status</th>
            <th class="col-md-1">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($pages as $k => $val)
         <?php  $size = @sizeFormat(@filesize(ltrim($val['path'], '/'))); 
               $size = (int)$size;
               if($size>0){
        ?>
        <tr>
            <td class="col-md-1">{{{$val['title']}}}</td>
            <td class="col-md-1">{{{$val['cat_name']}}}</td>
            <td class="col-md-1">{{{$val['type']}}}</td>
            <td class="col-md-1"><?php echo @sizeFormat(@filesize(ltrim($val['path'], '/'))); ?></td>
            <td class="col-md-1">{{{$val['created_at']}}}</td>
            <td class="col-md-1">{{{$val['comments']}}}</td>
            <td class="col-md-1">
                @if($val['status']==1)
                <button class="btn btn-success" name="status" >Approved</button>
                @elseif($val['status']==2)
                <button class="btn btn-warning" name="status" >Being Reviewed</button>
                @elseif($val['status']==3)
                <button class="btn btn-danger" name="status" >Awaiting Review</button>
                @endif
            </td>
            <td class="col-md-2">
                <a href="{{{ URL::to('/file/view/'.$val['imv_id']) }}}" class="iframe_file btn btn-xs btn-default">View</a>
                <a href="{{{ URL::to('/share_file/'.$val['imv_id']) }}}" class="iframe_file btn btn-xs btn-success">Share</a>
                <a href="/download/<?= $val['imv_id'] ?>" class="btn btn-xs btn-info">Download</a>
            </td>
        </tr>
        <?php }?>
        @endforeach

    </tbody>
</table>
</div>
@stop

{{-- Scripts --}}
@section('scripts')
<script type="text/javascript">
  var oTable;
  $(document).ready(function() {
        $('#pages').dataTable();
        $(".iframe").colorbox({iframe: true, width: "80%", height: "80%"});
    });
    
</script>
@stop

@endif