<?php

class BaseController extends Controller {

    /**
     * Initializer.
     *
     * @access   public
     * @return \BaseController
     */
    public function __construct() {
        $this->beforeFilter('csrf', array('on' => 'post'));
    }


    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout() {
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }

    /*
     * creating generic path so we can use for all over the application
     */
    
    public function make_path_api($filename = '',$db_path = false){        
        
        $save_path = '/uploads/users/';
        $usertargetFolder = $_SERVER['DOCUMENT_ROOT'] . $save_path; // Relative to the root
        if (!is_dir($usertargetFolder)) {
            @mkdir($schooltargetFolder, 0777);
            @chmod($schooltargetFolder, 0777);
        }
        if($db_path){
            return $save_path;
        }
        
        return rtrim($_SERVER['DOCUMENT_ROOT'],'/') . $save_path . $filename;
    }
    public function make_path($filename = '',$db_path = false){        
        
        $save_path = '/uploads/' . Auth::user()->s_id . '/' . Auth::user()->id . '/image/';
        
        if($db_path){
            return $save_path;
        }
        
        return rtrim($_SERVER['DOCUMENT_ROOT'],'/') . $save_path . $filename;
    }
    public function make_path_dynamic($filename = '',$db_path = false, $folder_name=''){        
        
        if(!empty($folder_name))
            $save_path = '/uploads/' . Auth::user()->s_id . '/' . Auth::user()->id . '/'.$folder_name.'/';
        else {
            $save_path = '/uploads/' . Auth::user()->s_id . '/' . Auth::user()->id . '/image/';
        }    
        
        if($db_path){
            return $save_path;
        }
        
        return rtrim($_SERVER['DOCUMENT_ROOT'],'/') . $save_path . $filename;
    }
    public function make_path_theme($filename = '',$db_path = false){        
        
        $save_path = '/theme_images/';
        
        if($db_path){
            return $save_path;
        }
        
        return rtrim($_SERVER['DOCUMENT_ROOT'],'/') . $save_path . $filename;
    }
    
    
}
