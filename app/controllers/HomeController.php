<?php

class HomeController extends BaseController {
    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      |
      | You may wish to use controllers instead of, or in addition to, Closure
      | based routes. That's great! Here is an example controller method to
      | get you started. To route to this controller, just add the route:
      |
      |	Route::get('/', 'HomeController@showWelcome');
      |
     */

    public function showWelcome() {
        return View::make('hello');
    }

    public function imageRotator() {
        echo 'Pakistan';
        // File and rotation
        $rotateFilename = $_SERVER['DOCUMENT_ROOT'].'uploads/8/16/image/Thuisbezorgd 1000.png'; // PATH
        $rotateFilename12 = '/uploads/8/16/image/Thuisbezorgd 1000.png'; // PATH
        $degrees = 90;
        $fileType = strtolower(substr('Thuisbezorgd 1000.png', strrpos('Thuisbezorgd 1000.png', '.') + 1));

        if ($fileType == 'png' || $fileType == 'PNG') {
            header('Content-type: image/png');
            $source = imagecreatefrompng($rotateFilename);
            $bgColor = imagecolorallocatealpha($source, 255, 255, 255, 127);
            // Rotate
            $rotate = imagerotate($source, $degrees, $bgColor);
            imagesavealpha($rotate, true);
            imagepng($rotate, $rotateFilename);
        }

        if ($fileType == 'jpg' || $fileType == 'jpeg') {
            header('Content-type: image/jpeg');
            $source = imagecreatefromjpeg($rotateFilename);
            // Rotate
            $rotate = imagerotate($source, $degrees, 0);
            imagejpeg($rotate, $rotateFilename);
        }

// Free the memory
        imagedestroy($source);
        imagedestroy($rotate);
        echo '<img src="'.$rotateFilename12.'">';
    }

}
