<?php

class AdminListsController extends AdminController {

    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Role Model
     * @var Role
     */
    protected $role;

    /**
     * Permission Model
     * @var Permission
     */
    protected $permission;
    protected $list;
    /**
     * Inject the models.
     * @param User $user
     * @param Role $role
     * @param Permission $permission
     */
    public function __construct(User $user, Role $role, Permission $permission , Listsusers $list) {
        
        parent::__construct();
        $this->user = $user;
        $this->role = $role;
        $this->permission = $permission;
        $this->list = $list;
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex() {
        // Title
        $title = Lang::get('wtadmin/users/title.user_management');

        // Grab all the users
        $users = $this->user;

        // Show the page
        return View::make('wtadmin/lists/index', compact('users', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate() {
        // All roles
//        $roles = $this->role->all();

        $lists = new Listsusersmodel();

        $lists = $lists->select()->where('user_id', Auth::user()->id)->get()->toArray();

        $roles = $this->role->select()->where('s_id', Auth::user()->s_id)->get();
        $roles[] = $this->role->find(3);

        // Get all the available permissions
//     $permissions = $this->permission->all();
        $permissions = $this->permission->select()->where('s_id', Auth::user()->s_id)->get();

        // Selected groups
        $selectedRoles = Input::old('roles', array());

        // Selected permissions
        $selectedPermissions = Input::old('permissions', array());

        // Title
        $title = Lang::get('Create New List');

        // Mode
        $mode = 'create';

        // Show the page
        return View::make('wtadmin/lists/create_edit', compact('lists','roles', 'permissions', 'selectedRoles', 'selectedPermissions', 'title', 'mode'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate() {


        $id = Input::get("option");

        /*
         * means manual addition
         */

        if($id == 2){

            $list_id = Input::get("list_id");
            $email_user   = Input::get("email");

            if(empty($email_user) || empty($list_id)){
                return Redirect::to('survey/lists/create')->with('error', Lang::get('wtadmin/lists/listsusers.generic_error'));
            }

             $userMoldel = new Usersemailsmodel();

             $lists = $userMoldel->select()->where('list_id', $list_id)->where('email_address',$email_user)->get()->toArray();

            if (!filter_var($email_user, FILTER_VALIDATE_EMAIL)) {

                return Redirect::to('survey/lists/create')->with('error', Lang::get('wtadmin/lists/listsusers.invalid_email'));

            }

             if(empty($lists)){

                 $userMoldel->list_id       = $list_id;
                 $userMoldel->email_address = $email_user;
                 $userMoldel->save();

                 return Redirect::to('survey/lists/create')->with('success', Lang::get('wtadmin/lists/listsusers.success_email_add'));

             }else{

                 return Redirect::to('survey/lists/create')->with('error', Lang::get('wtadmin/lists/listsusers.duplicate_email'));

             }


        }else {

            $lists = new Listsusersmodel();

            $lists->user_id = Confide::user()->id;
            $lists->lists_name = Input::get('username');

            if (empty($lists->lists_name)) {

                return Redirect::to('survey/lists/create')->with('error', Lang::get('wtadmin/lists/listsusers.list_name'));

            }

            if (Input::hasFile('file')) {
                $extension = Input::file('file')->getClientOriginalExtension();

                if (strtolower($extension) == 'csv') {

                    $name = Input::file('file')->getClientOriginalName();

                    Input::file('file')->move(PATH_FILE_LIST, $name);

                    $lists->save();

                    $data = $this->filereader($lists->id, $name);

                    if (is_array($data)) {

                        $data_final = sprintf(Lang::get('wtadmin/lists/listsusers.success_import'), $data['success'], $data['bad']);

                        return Redirect::to('survey/lists/create')->with('success', $data_final);

                    }

                    return Redirect::to('survey/lists/create')->with('success', Lang::get('wtadmin/lists/listsusers.success_import_normal'));


                } else {
                    return Redirect::to('survey/lists/create')->with('error', Lang::get('wtadmin/lists/listsusers.list_file_type'));
                }


            } else {
                return Redirect::to('survey/lists/create')->with('error', Lang::get('wtadmin/lists/listsusers.list_file'));
            }

        }
      
    }
    
    public function filereader($id,$name){

        $full_path = PATH_FILE_LIST.$name;
        $file      = fopen($full_path,"r");
        $counter   = 0;
        $bad       = 0;

        /*
         * validate if file is not opened or something else with permissions
         */

        if(!fopen($full_path,"r")){

            return Redirect::to('survey/lists/create')->with('error', sprintf(Lang::get('wtadmin/lists/listsusers.file_open_error'),$counter,$bad) );

        }

        /*
         * check and validation of file and loop through each records
         */

        while(! feof($file))
          {
            
            $email =  new Usersemailsmodel();
            $data  = fgetcsv($file);
            
            if(isset($data[0])){

                /*
                 * the one records good with validation
                 */

                if (filter_var($data[0], FILTER_VALIDATE_EMAIL)) {
    
                        $email->list_id       = $id;
                        $email->email_address = $data[0];
                        $email->save();
                        $counter ++;
                        
                    }
                }else{
                /*
                 * the one with bad validation
                 */
                        $bad++;
                }
          }

        fclose($file);


        $result = array(
            'success' => $counter,
            'bad'  => $bad
        );

        return $result;

    }

    /**
     * Display the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getShow($user) {
        // redirect to the frontend
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getEdit($user) {
        if ($user->id) {
//            $roles = $this->role->all();
//            $permissions = $this->permission->all();
//
//            $grades = $this->grade->all();

            $roles = $this->role->select()->where('s_id', Auth::user()->s_id)->get();
            $roles[] = $this->role->find(3);

            // Get all the available permissions
            //     $permissions = $this->permission->all();
            $permissions = $this->permission->select()->where('s_id', Auth::user()->s_id)->get();

            // Title
            $title = Lang::get('wtadmin/users/title.user_update');
            // mode
            $mode = 'edit';

            return View::make('wtadmin/users/create_edit', compact('user', 'roles', 'permissions', 'title', 'mode'));
        } else {
            return Redirect::to('survey/users')->with('error', Lang::get('wtadmin/users/messages.does_not_exist'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $user
     * @return Response
     */
    public function postEdit($user) {
        // Validate the inputs
        $validator = Validator::make(Input::all(), $user->getUpdateRules());


        if ($validator->passes()) {
            $oldUser = clone $user;
            $user->username = Input::get('username');
            $user->email = Input::get('email');
            $user->confirmed = Input::get('confirm');

            $password = Input::get('password');
            $passwordConfirmation = Input::get('password_confirmation');

            if (!empty($password)) {
                if ($password === $passwordConfirmation) {
                    $user->password = $password;
                    // The password confirmation will be removed from model
                    // before saving. This field will be used in Ardent's
                    // auto validation.
                    $user->password_confirmation = $passwordConfirmation;
                } else {
                    // Redirect to the new user page
                    return Redirect::to('survey/users/' . $user->id . '/edit')->with('error', Lang::get('wtadmin/users/messages.password_does_not_match'));
                }
            } else {
                unset($user->password);
                unset($user->password_confirmation);
            }

            if ($user->confirmed == null) {
                $user->confirmed = $oldUser->confirmed;
            }

            $user->prepareRules($oldUser, $user);

            // Save if valid. Password field will be hashed before save
            $user->amend();

            // Save roles. Handles updating.
            $user->saveRoles(Input::get('roles'));

        } else {
            return Redirect::to('survey/users/' . $user->id . '/edit')->with('error', Lang::get('wtadmin/users/messages.edit.error'));
        }

        // Get validation errors (see Ardent package)
        $error = $user->errors()->all();

        if (empty($error)) {
            // Redirect to the new user page
            return Redirect::to('survey/users/' . $user->id . '/edit')->with('success', Lang::get('wtadmin/users/messages.edit.success'));
        } else {
            return Redirect::to('survey/users/' . $user->id . '/edit')->with('error', Lang::get('wtadmin/users/messages.edit.error'));
        }
    }

    /**
     * Remove user page.
     *
     * @param $user
     * @return Response
     */
    public function getDelete($lists) {
        
        $title = Lang::get('wtadmin/lists/title.user_delete');
        return View::make('wtadmin/lists/delete', compact('lists', 'title'));
    
    }

    /**
     * Remove the specified user from storage.
     *
     * @param $user
     * @return Response
     */
    
    public function postDelete($lists) {
       
        ListUsers::where('list_id', $lists)->delete();
        
        return Redirect::to('survey/lists')->with('success', Lang::get('wtadmin/users/messages.delete.success'));
        
    }

    /**
     * Show a list of all the users formatted for Datatables.
     *
     * @return Datatables JSON
     */
    
    public function getData() {
        
        $id = Auth::user()->id;

        $users = ListUsers::select(array('list_id','list_id', 'lists_name', 'created_at'))
                 ->where('user_id', $id);


        return Datatables::of($users)
                        
                        ->add_column('actions', '
                                    <a href="{{{ URL::to(\'survey/lists/\' . $list_id . \'/delete\' ) }}}" class="iframe btn btn-xs btn-danger">{{{ Lang::get(\'button.delete\') }}}</a>
            ')
                        ->remove_column('user_id')
                        ->make();
    }

}
