<?php

class PaypalPaymentController extends \BaseController {

    /**
     * object to authenticate the call.
     * @param object $_apiContext
     */
    private $_apiContext;

    /**
     * Set the ClientId and the ClientSecret.
     * @param
     * string $_ClientId
     * string $_ClientSecret
     */
    private $_ClientId = 'ATEu1BEakgfLTybclDiHPPmDeHssS82WQ6ZdHIu7I-Ahn1rsgCZIht6yBYwa3RwtA4Y543zmmsAxRMuK';
    private $_ClientSecret = 'EFwf-Q-bkpkf_dl4lACSp0DzuptyKd9krdcflk6hINMu4EgIq11hLa4lPMO4tEIOz0gyIYdKwqfMWTH9';

    /*
     *   These construct set the SDK configuration dynamiclly,
     *   If you want to pick your configuration from the sdk_config.ini file
     *   make sure to update you configuration there then grape the credentials using this code :
     *   $this->_cred= Paypalpayment::OAuthTokenCredential();
     */

    public function __construct() {

        // ### Api Context
        // Pass in a `ApiContext` object to authenticate
        // the call. You can also send a unique request id
        // (that ensures idempotency). The SDK generates
        // a request id if you do not pass one explicitly.

        $this->_apiContext = Paypalpayment::ApiContext($this->_ClientId, $this->_ClientSecret);

        // Uncomment this step if you want to use per request
        // dynamic configuration instead of using sdk_config.ini

        $this->_apiContext->setConfig(array(
            'mode' => 'sandbox',
            'service.EndPoint' => 'https://api.sandbox.paypal.com',
            'http.ConnectionTimeOut' => 30,
            'log.LogEnabled' => true,
            'log.FileName' => __DIR__ . '/../PayPal.log',
            'log.LogLevel' => 'FINE'
        ));
                
    }

    /**
     * Display a listing of the resource.
     * GET /paypalpayment
     *
     * @return Response
     */
    public function index() {
        echo "<pre>";

        $payments = Paypalpayment::getAll(array('count' => 1, 'start_index' => 0), $this->_apiContext);

        dd($payments);
    }

    /**
     * Show the form for creating a new resource.
     * GET /paypalpayment/create
     *
     * @return Response
     */
    public function create() {
        return View::make('payment.order');
    }

    /**
     * Store a newly created resource in storage.
     * POST /paypalpayment
     *
     * @return Response
     */


    public function paypal_payment($input, $packageinformation , $userid){

        $total_price = str_replace("$","",$packageinformation[0]['pl_charges']);

        $payer = Paypalpayment::payer();
        $payer->setPaymentMethod("paypal");

        $item1 = Paypalpayment::item();
        $item1->setName($packageinformation[0]['pl_name'])
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice($total_price);

        $itemList = Paypalpayment::itemList();
        $itemList->setItems(array($item1));


        $details = Paypalpayment::details();
        $details->setSubtotal($total_price);


        $amount = Paypalpayment::amount();
        $amount->setCurrency("USD")
            ->setTotal($total_price)
            ->setDetails($details);

        $baseUrl = Paypalpayment::getBaseUrl();
        $redirectUrls = Paypalpayment::redirectUrls();
        $redirectUrls->setReturnUrl(URL::to('survey/paymentcomplete?success=true&package='.$input['package']))
            ->setCancelUrl("$baseUrl/survey/paymentcomplete?success=false&package=".$input['package']);

        $transaction = Paypalpayment::transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Payment for surveys tool")
            ->setInvoiceNumber(uniqid("INVOICE-"));

        $payment = Paypalpayment::payment();
        $payment->setIntent("authorize")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($this->_apiContext);
        } catch (Exception $ex) {
            ResultPrinter::printError("Created Payment Authorization Using PayPal. Please visit the URL to Authorize.", "Payment", null, $request, $ex);
            exit(1);
        }

        return $payment->getApprovalLink();

    }

    public function store($input, $packageinformation , $userid) {
        // ### Address

        $name = $input['cc_name'];
        $exploded = explode(' ',$name);

        if(!is_array($exploded)){
            $exploded[0] = $name;
            $exploded[1] = $name;
        }

        $total_price = str_replace("$","",$packageinformation[0]['pl_charges']);

        $card = Paypalpayment::creditCard();
        $card->setType("visa")
            ->setNumber($input['cc_number'])
            ->setExpireMonth($input['month'])
            ->setExpireYear($input['year'])
            ->setCvv2($input['csvcode'])
            ->setFirstName($exploded[0])
            ->setLastName($exploded[1]);

        $fi = Paypalpayment::fundingInstrument();
        $fi->setCreditCard($card);

        $payer = Paypalpayment::payer();
        $payer->setPaymentMethod("credit_card")
            ->setFundingInstruments(array($fi));

        $item1 = Paypalpayment::item();
        $item1->setName($packageinformation[0]['pl_name'])
                ->setDescription($packageinformation[0]['pl_name'].' for surveys')
                ->setCurrency('USD')
                ->setQuantity(1)
                ->setPrice($total_price);

        $itemList = Paypalpayment::itemList();
        $itemList->setItems(array($item1));

        $details = Paypalpayment::details();
        $details->setSubtotal($total_price);

        //Payment Amount
        $amount = Paypalpayment::amount();
        $amount->setCurrency("USD")
                ->setTotal($total_price)
                ->setDetails($details);

        $transaction = Paypalpayment::transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Payment for surveys website")
            ->setInvoiceNumber(uniqid("INVOICE-"));

        $payment = Paypalpayment::payment();

        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setTransactions(array($transaction));

        try {

            $response = $payment->create($this->_apiContext);
            $payee_id = $payment->getId();

            if(isset($response->name) && ($response->name == "VALIDATION_ERROR")){

                return $response = array(
                    'error' => true,
                    'msg'   => $response->details[0]['issue']
                );

            }else{

                return $response = array(
                    'error' => false,
                    'msg'   => $payee_id
                );

            }


        } catch (\PPConnectionException $ex) {

            return false;

        }

    }

    /**
     * Display the specified resource.
     * GET /paypalpayment/{id}
     *
     * @param  int  $id
     * @return Response
     */
    /*
        Use this call to get details about payments that have not completed, 
        such as payments that are created and approved, or if a payment has failed.
        url:payment/PAY-3B7201824D767003LKHZSVOA
    */

    public function show($payment_id)
    {
       $payment = Paypalpayment::getById($payment_id,$this->_apiContext);

       dd($payment);
    }

    /**
     * Show the form for editing the specified resource.
     * GET /paypalpayment/{id}/edit
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     * PUT /paypalpayment/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /paypalpayment/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

}
