<?php

use Hugofirth\Mailchimp\Facades\MailchimpWrapper;

class MailchimpController extends AdminController
{
    /**
     * @param
     * @return \Illuminate\View\View
     */

    public function get()
    {

        $key = Input::get("option");

        /*$mandrill = new Mandrill($key);

        $message = array(
            'html' => "validate the api",
            'subject'    => "validating",
            'from_email' => Auth::user()->email,
            'from_name'  => Auth::user()->first_name.' '.Auth::user()->last_name,
            'to' => array(
                array(
                    'email' => "zabbas44@gmail.com",
                    'name' => 'Account testing',
                    'type' => 'to'
                )
            ),
            'headers' => array('Reply-To' => 'noreply@noreply.com'),
        );

        $async   = false;*/

        //$response = $mandrill->messages->send($message, $async);

        MailchimpWrapper::lists()->setKey($key);

        $object_result = MailchimpWrapper::lists()->getList();

        /*if($response == 'apierror'){

            $error = true;
            $lists = "Api not valid!";
            return View::make('wtadmin/emails/clients/mailchimp_data',compact("lists","error"));

        }else{

            $error = false;
            $lists = "Successfully validated Api!";
            return View::make('wtadmin/emails/clients/mailchimp_data',compact("lists","error"));

        }*/

        if(isset($object_result['data'])){

            $lists = $object_result['data'];

            if(is_array($lists) && count($lists) > 0){

                $error = false;
                return View::make('wtadmin/emails/clients/mailchimp_data',compact("lists","error"));

            }else{

                $lists =  "No List found, Please create one in Mailchimp!";
                $error = true;
                return View::make('wtadmin/emails/clients/mailchimp_data',compact("lists","error"));

            }

            $error = false;
            return View::make('wtadmin/emails/clients/mailchimp_data',compact("lists","error"));


        }else{

            $lists = MailchimpWrapper::lists()->getList();

            $error = true;

            return View::make('wtadmin/emails/clients/mailchimp_data',compact("lists","error"));

        }

/*        $response = MailchimpWrapper::lists()->subscribe( $lists[0]['id'], array('email'=> $emailId));
*/


    }

}
