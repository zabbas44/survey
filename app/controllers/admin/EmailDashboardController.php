<?php

use Hugofirth\Mailchimp\Facades\MailchimpWrapper;
use Ctct\Auth\CtctOAuth2;
use Ctct\Exceptions\OAuth2Exception;


class EmailDashboardController extends AdminController
{
    /**
     * @param
     * @return \Illuminate\View\View
     */

    public function configurations(){

        $option = Input::get('option');
        $id     = Input::get('id');
        $data   = array();

        /*
         * mailchimp code
         */

        if($option == 1){

            $condition = array(
                'id' => $id,
                'u_e_c_userid' => $id = Auth::user()->id
            );

            $responses = Usersemailsclients::select("*")
                ->where($condition)->get()->toArray();

            if(is_array($responses) && count($responses) > 0){
                $data = $responses[0];
            }

        }

        /*
         * getresponse code
         */

        if($option == 2){

            $condition = array(
                'id' => $id,
                'u_e_c_userid' => $id = Auth::user()->id
            );

            $responses = Usersemailsclients::select("*")
                ->where($condition)->get()->toArray();

            if(is_array($responses) && count($responses) > 0){
                $data = $responses[0];
            }

        }

        /*
         * getresponse code
         */

        if($option == 3){

            $condition = array(
                'id' => $id,
                'u_e_c_userid' => $id = Auth::user()->id
            );

            $responses = Usersemailsclients::select("*")
                ->where($condition)->get()->toArray();

            if(is_array($responses) && count($responses) > 0){
                $data = $responses[0];
            }

        }

        /*
         * getresponse code
         */

        if($option == 5){

            $condition = array(
                'id' => $id,
                'u_e_c_userid' => $id = Auth::user()->id
            );

            $responses = Usersemailsclients::select("*")
                ->where($condition)->get()->toArray();

            if(is_array($responses) && count($responses) > 0){
                $data = $responses[0];
            }

        }


        $id = Auth::user()->id;
        return $this->client_views($option,$data);

    }

    /**
     * @param
     * @return $this|\Illuminate\Http\RedirectResponse
     */

    public function create(){

        return $this->saveService();

    }

    /**
     * @param $id
     * @param
     * @return \Illuminate\View\View|string
     * @throws Exception
     */

    public function remove($id){

        $confirm = Input::get('confirm');

        if($confirm == "yes"){

            $obj = new Usersemailsclients;

            $obj->id = $id;

            $obj->delete();

            return "true";

        }

        $title = "Confirm remove!";

        return View::make('wtadmin/emails/delete',compact("id","title"));

    }

    /**
     * @param
     * @return $this|\Illuminate\Http\RedirectResponse
     */

    public function update(){

        return $this->updateService();

    }

    /**
     * @param $view_id
     * @param
     * @return \Illuminate\View\View
     */

    private function client_views($view_id , $data = array()){

        //for mailchimp view load

        if($view_id == 1){

            $clientname = "Mailchimp";

            if(isset($data['u_e_c_key'])){
                $u_e_c_key = $data['u_e_c_key'];
            }else{
                $u_e_c_key = "";
            }

            return View::make('wtadmin/emails/clients/mailchimp',compact("clientname","u_e_c_key"));
        }

        if($view_id == 3){

            $clientname = "getresponse";

            if(isset($data['u_e_c_key'])){
                $u_e_c_key = $data['u_e_c_key'];
            }else{
                $u_e_c_key = "";
            }

            return View::make('wtadmin/emails/clients/getresponse',compact("clientname","u_e_c_key"));

        }

        //aweber response api

        if($view_id == 2){

            $clientname = "aweber";

            if(isset($data['u_e_c_key'])){
                $u_e_c_key = $data['u_e_c_key'];
            }else{
                $u_e_c_key = "";
            }

            return View::make('wtadmin/emails/clients/aweber',compact("clientname","u_e_c_key"));

        }

        //Constant contact api response

        if($view_id == 5){

            $clientname = "Constant Contact";

            if(isset($data['u_e_c_key'])){
                $u_e_c_key = $data['u_e_c_key'];
            }else{
                $u_e_c_key = "";
            }

            return View::make('wtadmin/emails/clients/constantcontact',compact("clientname","u_e_c_key"));

        }

    }

    /**
     * @param
     */

    public function emailConfigurationsadd(){

        $serviceList = array(
            '1' => 'Mailchimp',
            '2' => 'Aweber',
            '3' => 'Getresponse',
            '4' => 'Sendreach',
            '5' => 'Constant contact',
            '6' => 'Icontact',
            '7' => 'Infusionsoft',
        );

        $title = "Configure New Email Client";

        $option = Input::get("option");

        $id = Auth::user()->id;

        $types_used = Usersemailsclients::select()->where('u_e_c_userid', $id)->get()->toArray();

        $keys = array_keys($serviceList);

        if(is_array($types_used) && !empty($types_used)){

            foreach($types_used as $key=>$pertype){

                if(in_array($pertype['u_e_c_type'],$keys)){
                    unset($serviceList[$pertype['u_e_c_type']]);
                }

            }
        }

        return View::make('wtadmin/emails/create', compact('serviceList','title','option'));

    }


    /**
     * @param
     * @return mixed
     */

    public function clientconfigurations(){

        $responses = Usersemailsclients::select(array('id', 'u_e_c_type', 'created_at'))
            ->orderBy('created_at', 'desc')
            ->where('u_e_c_userid', Auth::user()->id);

        return Datatables::of($responses)
            ->edit_column('created_at', function ($result_obj) {
                //in a callback, the Eloquent object is returned so carbon may be used
                return $result_obj->created_at->format('d/m/Y h:i A');
            })
            ->edit_column('u_e_c_type', function ($result_obj) {
                //in a callback, the Eloquent object is returned so carbon may be used

                $serviceList = array(
                    '1' => 'Mailchimp',
                    '2' => 'Aweber',
                    '3' => 'Getresponse',
                    '4' => 'Sendreach',
                    '5' => 'Constant contact',
                    '6' => 'Icontact',
                    '7' => 'Infusionsoft',
                );

                if(isset($serviceList[$result_obj->u_e_c_type])){
                    return $serviceList[$result_obj->u_e_c_type];
                }

            })
            ->add_column('actions', ' <div style="width:100px !important;"> <a href="{{{ URL::to(\'survey/email/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-info" ><i class="fa fa-edit"></i></a>
                                <a href="{{{ URL::to(\'survey/email/\' . $id . \'/delete\' ) }}}" class="iframe btn btn-xs btn-danger" data-toggle=""><i class="fa fa-trash"></i></a></div>
                        ')
            ->make();

    }

    /**
     * @param $id
     * @param
     * @return \Illuminate\View\View
     */

    public function edit($id){

        $option = "";

        $serviceList = array(
            '1' => 'Mailchimp',
            '2' => 'Aweber',
            '3' => 'Getresponse',
            '4' => 'Sendreach',
            '5' => 'Constant contact',
            '6' => 'Icontact',
            '7' => 'Infusionsoft',
        );

        $title = "Update Email Client";

        $condition = array(
            'id' => $id
        );

        $responses = Usersemailsclients::select("*")
            ->where($condition)->get()->toArray();

        if(is_array($responses) && count($responses) > 0){

            $type = $responses[0]['u_e_c_type'];
            $option = $type;

        }

        $user = Auth::user()->id;

        $types_used = Usersemailsclients::select()->where('u_e_c_userid', $user)->get()->toArray();

        return View::make('wtadmin/emails/edit', compact('serviceList','title','option','id'));


    }


    private function updateService(){

        $service = Input::get('optionerror');
        $record  = Input::get('recordid');

        //for mailchimp view load

        if($service  == 1){

            $rules = array(
                'apikey' => 'required',
                'selected_list' => 'required'
            );

            // Validate the inputs
            $validator = Validator::make(Input::all(), $rules);
            // Check if the form validates with success
            if ($validator->passes()) {

                // Get the inputs, with some exceptions
                $inputs = Input::except('csrf_token');
                $apikey = Input::get('apikey');
                $list   = Input::get('selected_list');

                $client = new Usersemailsclients;

                $id = Auth::user()->id;

                $condition = array(
                    'id' => $record
                );

                $client->update();

                DB::table('users_emails_clients')
                    ->where($condition)
                    ->update(
                        array(
                            'u_e_c_key' => $apikey,
                            'u_e_c_list_id' => $list
                    ));


                return Redirect::to('survey/email/'.$record.'/edit')->with('success', "Email Client Updated Successfully!");


            }else{
                // Form validation failed
                return Redirect::to('survey/email/'.$record.'edit?option='.$service)->withInput()->withErrors($validator);
            }

        }

    }

    public function constantcontact(){


        $callbackUrl = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'/constantcontact';

        define("APIKEY", "82gcafcqq3vbrmkwvs37jh5p");
        define("CONSUMER_SECRET", "RTYchuBcMRBcvybhZE4HERzX");
        define("REDIRECT_URI", $callbackUrl);

        // instantiate the CtctOAuth2 class
        $oauth = new CtctOAuth2(APIKEY, CONSUMER_SECRET, REDIRECT_URI);

        $code = Input::get('code');

        if (isset($code) && !empty($code)) {
            try {

                $accessToken = $oauth->getAccessToken($code);


            } catch (OAuth2Exception $ex) {
                echo '<span class="label label-important">OAuth2 Error!</span>';
                echo '<div class="container alert-error"><pre class="failure-pre">';
                echo 'Error: ' . htmlspecialchars( $ex->getMessage() );
                echo '</pre></div>';
                die();
            }

            echo '<span class="label label-success">Access Token Retrieved!</span>';
            echo '<div class="container alert-success"><pre class="success-pre">';
            print_r( htmlspecialchars( $accessToken ) );
            echo '</pre></div>';

        } else {

            $url  = $oauth->getAuthorizationUrl();

            echo $url;

            die;

            header("Location: $url");
            die;

        }


    }

    public function createaweber(){

        $service = 2;
        $consumerKey    = "AktiA8cFhhqq65T0GB3xSa6x";
        $consumerSecret = "dSsTLS3v1etTepVMLnLcmEZQVScffSOdmuYCsxu6";

        $aweber = new AWeberAPI($consumerKey, $consumerSecret);

        $accessToken = Cache::get('accessToken');
        $ouath_token = Input::get('oauth_token');

        if (empty($accessToken)) {
            if (empty($ouath_token)) {

                $callbackUrl = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'/aweber';
                list($requestToken, $requestTokenSecret) = $aweber->getRequestToken($callbackUrl);
                Cache::put('callbackUrl', $callbackUrl, 10);
                Cache::put('requestTokenSecret', $requestTokenSecret, 10);

                header("Location: {$aweber->getAuthorizeUrl()}");
                exit();
            }

            $requestTokenSecret = Cache::get('requestTokenSecret');

            $aweber->user->tokenSecret = $requestTokenSecret;
            $aweber->user->requestToken = $_GET['oauth_token'];
            $aweber->user->verifier = $_GET['oauth_verifier'];
            list($accessToken, $accessTokenSecret) = $aweber->getAccessToken();

            $client = new Usersemailsclients;

            $id = Auth::user()->id;

            $client->u_e_c_key    = $accessToken;
            $client->u_e_c_userid = $id;
            $client->u_e_c_type   = $service;
            $client->u_e_c_list_id = $accessTokenSecret;

            $client->save();

            if($client->id){
                return Redirect::to('survey/email/add')->with('success', "Email Client Added Successfully!");
            }else{
                return Redirect::to('survey/email/add?option='.$service)->with('error', "Something Happened worng! try again");
            }

        }

    }

    private function saveService(){

        $service = Input::get('service');

        //for mailchimp view load

        if($service  == 1){

            $rules = array(
                'apikey' => 'required',
                'selected_list' => 'required'
            );

            // Validate the inputs
            $validator = Validator::make(Input::all(), $rules);
            // Check if the form validates with success
            if ($validator->passes()) {
                // Get the inputs, with some exceptions
                $inputs = Input::except('csrf_token');
                $apikey = Input::get('apikey');
                $list   = Input::get('selected_list');


                $client = new Usersemailsclients;

                $id = Auth::user()->id;

                $client->u_e_c_key    = $apikey;
                $client->u_e_c_userid = $id;
                $client->u_e_c_type   = $service;
                $client->u_e_c_list_id = $list;

                $client->save();

                if($client->id){
                    return Redirect::to('survey/email/add')->with('success', "Email Client Added Successfully!");
                }else{
                    return Redirect::to('survey/email/add?option='.$service)->with('error', "Something Happened worng! try again");
                }

            }else{
                // Form validation failed
                return Redirect::to('survey/email/add?option='.$service)->withInput()->withErrors($validator);
            }

        }

        //for aweber api

        if($service == 2){

            $consumerKey    = "AktiA8cFhhqq65T0GB3xSa6x";
            $consumerSecret = "dSsTLS3v1etTepVMLnLcmEZQVScffSOdmuYCsxu6";

            $aweber = new AWeberAPI($consumerKey, $consumerSecret);

            $accessToken = Cache::get('accessToken');
            $ouath_token = Input::get('oauth_token');
            $callbackUrl = Cache::get('callbackUrl');

            if (empty($accessToken)) {

                if (empty($ouath_token)) {

                    $callbackUrl = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'/aweber';
                    list($requestToken, $requestTokenSecret) = $aweber->getRequestToken($callbackUrl);
                    Cache::put('callbackUrl', $callbackUrl, 10);
                    Cache::put('requestTokenSecret', $requestTokenSecret, 10);

                    header("Location: {$aweber->getAuthorizeUrl()}");
                    exit();
                }

                $aweber->user->tokenSecret  = Cache::get('requestTokenSecret');
                $aweber->user->requestToken = Input::get('oauth_token');
                $aweber->user->verifier     = Input::get('oauth_verifier');
                list($accessToken, $accessTokenSecret) = $aweber->getAccessToken();

                $client = new Usersemailsclients;

                $id = Auth::user()->id;

                $client->u_e_c_key    = $accessToken;
                $client->u_e_c_userid = $id;
                $client->u_e_c_type   = $service;
                $client->u_e_c_list_id = $accessTokenSecret;

                $client->save();

                if($client->id){
                    return Redirect::to('survey/email/add')->with('success', "Email Client Added Successfully!");
                }else{
                    return Redirect::to('survey/email/add?option='.$service)->with('error', "Something Happened worng! try again");
                }


            }

        }

        //for getresponse view load

        if($service  == 3){

            $rules = array(
                'apikey' => 'required',
            );

            // Validate the inputs
            $validator = Validator::make(Input::all(), $rules);
            // Check if the form validates with success
            if ($validator->passes()) {
                // Get the inputs, with some exceptions
                $inputs = Input::except('csrf_token');
                $apikey = Input::get('apikey');

                $client = new Usersemailsclients;

                $id = Auth::user()->id;

                $client->u_e_c_key    = $apikey;
                $client->u_e_c_userid = $id;
                $client->u_e_c_type   = $service;

                $client->save();

                if($client->id){
                    return Redirect::to('survey/email/add')->with('success', "Email Client Added Successfully!");
                }else{
                    return Redirect::to('survey/email/add?option='.$service)->with('error', "Something Happened worng! try again");
                }

            }else{
                // Form validation failed
                return Redirect::to('survey/email/add?option='.$service)->withInput()->withErrors($validator);
            }

        }


        //for constant contact view load

        if($service  == 5){

                $callbackUrl = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'/constantcontact';

                define("APIKEY", "82gcafcqq3vbrmkwvs37jh5p");
                define("CONSUMER_SECRET", "RTYchuBcMRBcvybhZE4HERzX");
                define("REDIRECT_URI", $callbackUrl);

                // instantiate the CtctOAuth2 class
                $oauth = new CtctOAuth2(APIKEY, CONSUMER_SECRET, REDIRECT_URI);

                $code = Input::get('code');

                if (isset($code) && !empty($code)) {
                    try {

                        $accessToken = $oauth->getAccessToken($code);

                    } catch (OAuth2Exception $ex) {
                        echo '<span class="label label-important">OAuth2 Error!</span>';
                        echo '<div class="container alert-error"><pre class="failure-pre">';
                        echo 'Error: ' . htmlspecialchars( $ex->getMessage() );
                        echo '</pre></div>';
                        die();
                    }

                    echo '<span class="label label-success">Access Token Retrieved!</span>';
                    echo '<div class="container alert-success"><pre class="success-pre">';
                    print_r( htmlspecialchars( $accessToken ) );
                    echo '</pre></div>';

                } else {

                    $url  = $oauth->getAuthorizationUrl();
                    header("Location: $url");
                    die;

                }

        }


    }


}
