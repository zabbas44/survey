<?php

use Hugofirth\Mailchimp\Facades\MailchimpWrapper;

class AdminDashboardController extends AdminController
{

    /**
     * Admin dashboard
     *
     */
    public function getIndex()
    {

        $title = "Survey Management System ";
        $con_title = "Dashboard";

        $id = Auth::user()->id;

        $terms = SurveysModel::select()->where('s_user_id', $id)->get();

        return View::make('wtadmin/dashboard', compact('con_title', 'title', 'terms'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {

        $id = Auth::user()->id;

        $title = Lang::get('wtadmin/survey/title.create_a_new_survey');
        $lists = DB::table('listsusers')->where("user_id", $id)->get();

        $package_details = array();

        $user_package = PackagesModel::select()
            ->orderby('created_at', 'desc')
            ->where('p_user_id', $id)->get()->toArray();

        if (isset($user_package[0]['p_package'])) {

            $package = $user_package[0]['p_package'];

        } else {
            $package = 1;
        }


        $package_details = PackagesListModel::select()->where('pl_id', $package)->get()->toArray();

        $total_left_emails = 0;

        if (isset($package_details[0]['p_package'])) {

            $p_emails = $package_details[0]['p_package'];

        } else {

            $p_emails = 500;

        }

        if (isset($user_package[0]['p_used_emails'])) {

            $p_used = $user_package[0]['p_used_emails'];

        } else {

            $p_used = 0;

        }

        if ($p_used <= $p_emails) {

            $left_emails = $p_emails - $p_used;

        } else {
            $left_emails = 0;
        }

        $condition = array(
            'u_e_c_userid' => Auth::user()->id
        );

        $responses = Usersemailsclients::select("*")
            ->where($condition)->get()->toArray();

        $serviceList_nedefault = array(
            '1' => 'Mailchimp',
            '2' => 'Aweber',
            '3' => 'Getresponse',
            '4' => 'Sendreach',
            '5' => 'Constant contact',
            '6' => 'Icontact',
            '7' => 'Infusionsoft',
        );

        $data_array  = array();
        $serviceList = array();

        if(is_array($responses) && $responses){
            foreach($responses as $key=>$perresponse){
                $serviceList[$perresponse['u_e_c_type']] = $serviceList_nedefault[$perresponse['u_e_c_type']];
            }
        }

        return View::make('wtadmin/survey/create', compact('title', 'lists', 'package_details', 'left_emails','serviceList'));

    }


    /**
     * @function package()
     * Survey Billing Package Page.
     * GET /
     * @return Response billing Package page view
     */


    public function package()
    {

        $title = Lang::get('wtadmin/survey/title.survey_update');

        $id = Auth::user()->id;

        $user_package = PackagesModel::select()
            ->orderBy("created_at", "desc")
            ->where('p_user_id', $id)->get()->toArray();

        if (isset($user_package[0]['p_package'])) {
            $package = $user_package[0]['p_package'];
        } else {
            $package = 1;
        }


        $package_details = PackagesListModel::select()
            ->where('pl_id', $package)->get()->toArray();
        return View::make('wtadmin/survey/package', compact('package', 'package_details', 'title'));

    }


    public function postAdd()
    {

        $user_id = Auth::user()->id;
        $name = Input::get("name");
        $question = Input::get("question");
        $first_response = Input::get("first_response");
        $opt_2 = Input::get("opt_2");
        $opt_3 = Input::get("opt_3");
        $opt_4 = Input::get("opt_4");
        $opt_1_check = Input::get("opt_1_check");
        $opt_2_check = Input::get("opt_2_check");
        $opt_3_check = Input::get("opt_3_check");
        $opt_4_check = Input::get("opt_4_check");
        $inputs = Input::except('csrf_token');
        $service = Input::get("service");

        /*
         * just for validation
         */

        if (is_array($question)) {

            foreach ($question as $key => $perQuestion) {

                $rules['name'] = 'required';
                $rules['question.' . $key] = 'required';
                $rules['first_response.' . $key] = 'required';

                $validator = Validator::make(Input::all(), $rules);

                if ($validator->passes()) {

                    /*
                     * do nothing here, if there is any eeror it will redirect to the back page with error
                     */

                } else {

                    return Redirect::to('survey/create')->withInput()->withErrors($validator);

                }

            }
        }

        /*
         * here means survey is good to send out
         * so save it
         */

        $survey = new SurveysModel();

        $survey->s_user_id = $user_id;

        if (isset($inputs['name'])) {
            $survey->s_name = $inputs['name'];
        }

        if (isset($inputs['landing_page'])) {
            $survey->l_page_text = $inputs['landing_page'];
        }

        if (isset($inputs['notify_me'])) {
            $survey->notify = $inputs['notify_me'];
        }

        if (isset($inputs['list'])) {
            $survey->s_list_id = $inputs['list'];
        }

        $survey->save();

        $response = $this->request_generator($survey, $inputs, $survey->id,$service);

        if ($response == 'expired' && $response != true) {
            return Redirect::to('survey/create')->with('error', Lang::get('wtadmin/survey/messages.packageexpired'));
        }

        if($response == false){
            return Redirect::to('survey/create')->with('error', Lang::get('wtadmin/survey/Something wrong happened, please try Again'));
        }

        $counter = 1;

        foreach ($question as $key => $perQuestion) {

            $question = new QuestionModel();

            $question->s_question = $perQuestion;

            $question->q_survey_id = $survey->id;

            $question->s_question_order = $counter;

            if (isset($first_response[$key])) {
                $question->s_1_response = $first_response[$key];
            }

            if (isset($opt_2[$key])) {
                $question->s_2_response = $opt_2[$key];
            }

            if (isset($opt_3[$key])) {
                $question->s_3_response = $opt_3[$key];
            }

            if (isset($opt_4[$key])) {
                $question->s_4_response = $opt_4[$key];
            }

            if (isset($opt_1_check[$key])) {
                $question->s_1_enable_comments = $opt_1_check[$key];
            }

            if (isset($opt_2_check[$key])) {
                $question->s_2_enable_comments = $opt_2_check[$key];
            }

            if (isset($opt_3_check[$key])) {
                $question->s_3_enable_comments = $opt_3_check[$key];
            }

            if (isset($opt_4_check[$key])) {
                $question->s_4_enable_comments = $opt_4_check[$key];
            }

            if (isset($inputs['list'])) {
                $question->s_list_id = $inputs['list'];
            }

            $question->save();
            $counter++;

        }

        return Redirect::to('survey/create')->with('success', Lang::get('wtadmin/survey/messages.create'));

    }

    function request_generator(SurveysModel $model, $input, $id , $service)
    {

        $user_id = Auth::user()->id;

        $subject = "Survey, " . $input['name'];

        // To send HTML mail, the Content-type header must be set

        /*$headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        */

        if (!empty($model->s_list_id)) {;

            $lists = Usersemailsmodel::select()->where('list_id', $model->s_list_id)->get()->toArray();

            $used = 0;

            $package_details = PackagesModel::select()
                ->orderby('created_at', 'desc')
                ->where('p_user_id', $user_id)->get()->toArray();
            /*
             * if package is expired
             */

            $total = 0;

            if (isset($package_details[0]['p_available_emails']) && isset($package_details[0]['p_used_emails'])) {

                $p_available_emails = $package_details[0]['p_available_emails'];
                $p_used_emails = $package_details[0]['p_used_emails'];
                $current_service = count($lists);

                $total = $current_service + $p_used_emails;

                if ($total > $p_available_emails) {
                    return "expired";
                }

            }

            if (is_array($lists) && count($lists) > 0) {
               $operation = $this->serviceMaker($service , $lists , $subject , $id ,  $input);
            }

            if (!isset($package_details[0]['p_id'])) {
                $package_details[0]['p_id'] = 1;
            }

            /*
             * if emails are send then minus please
             */

            if($operation){

                DB::table('packages')
                    ->where('p_id', $package_details[0]['p_id'])
                    ->where('p_user_id', $user_id)
                    ->orderby('created_at', 'desc')
                    ->update(array('p_used_emails' => $total));

                return true;

            }

            return false;

        }

        return true;

    }

    public function serviceMaker($serviceId , $lists , $subject , $id , $input){

        $condition = array(
            'u_e_c_type'   => $serviceId,
            'u_e_c_userid' => Auth::user()->id
        );

        $responses = Usersemailsclients::select("*")->where($condition)->get()->toArray();

        //core mailchimp api
        if($serviceId == 1) {

            /**
             * to use generic content over the mailchimp we have to set this random
             */

            $emailId = 'Random';

            MailchimpWrapper::lists()->setKey($responses[0]['u_e_c_key']);

            $email_body = View::make('emails/email_survey', compact('input', 'id', 'emailId'));

            foreach ($lists as $perlist) {

                $emailId  = $perlist['email_address'];

                /*
                 * mailchimp functionality
                 */

                $response = MailchimpWrapper::lists()->subscribe($responses[0]['u_e_c_list_id'], array('email' => $emailId), '', '', false);

            }

            /*
             * create the compain and send them emails
             */

            $com_created = MailchimpWrapper::Campaigns()->create('plaintext',array(
                "list_id"    => $responses[0]['u_e_c_list_id'],
                "subject"    => $subject,
                "from_email" => Auth::user()->email,
                "from_name"  => Auth::user()->first_name.' '.Auth::user()->last_name,
            ),array("html" => "$email_body" ));

            if(isset($com_created['id'])){

                $c_id = $com_created['id'];
                $final_response = MailchimpWrapper::Campaigns()->send($c_id);

                if(isset($final_response['complete']) && $final_response['complete'] == 1){
                    return true;
                }

            }else{
                return false;
            }

            return false;

        }

        //Aweber service

        if($serviceId == 2) {

            $consumerKey    = "AktiA8cFhhqq65T0GB3xSa6x";
            $consumerSecret = "dSsTLS3v1etTepVMLnLcmEZQVScffSOdmuYCsxu6";

            $emailId     = 'Random';
            $email_body = View::make('emails/email_survey', compact('input', 'id', 'emailId'));
            $aweber = new AWeberAPI($consumerKey, $consumerSecret);

            $aweber->adapter->debug = false;

            $account = $aweber->getAccount($responses[0]['u_e_c_key'], $responses[0]['u_e_c_list_id']);
            $lists_local = $account->lists->find(array('name' => ''));

            $url_valid = $lists_local->url.'/'.$lists_local->data['entries'][0]['id'];

            $list = $account->loadFromUrl($url_valid);

            foreach ($lists as $perlist) {

                $emailId  = $perlist['email_address'];

                # create a subscriber
                $params = array(
                    'email' => $emailId,
                    'last_followup_message_number_sent' => '1001'
                );

                try{

                    $subscribers = $list->subscribers;
                    $new_subscriber = $subscribers->create($params);

                }catch (Exception $e){


                }

            }

            $list = $lists_local[0];

            $data = array(
                    'notify_on_send' => True,
                    'is_archived' => false,
                    'body_html' => $email_body,
                    'subject' => $subject);

            $broadCast = "https://api.aweber.com/1.0".$url_valid."/broadcasts";

            $response = $aweber->new_campaign($broadCast,$data);

            $data_broadcast = array(
                'scheduled_for' => date('Y').'-'.date('d',strtotime(' +1 day')).'-02T20:00:00+00:00'
            );

            $scheduleBroadcast = "https://api.aweber.com/1.0$url_valid/broadcasts/$response/schedule";

            $compaing_sent = $aweber->schedule_campaign($scheduleBroadcast,$data_broadcast);

            if($compaing_sent){
                return true;
            }

            return false;

        }

        //Getresponse service

        if($serviceId == 3) {

            $emailId     = 'Random';
            $CAMPAIGN_ID = "";

            $email_body = View::make('emails/email_survey', compact('input', 'id', 'emailId'));

            /*
             * getResponse functionality
             */

            $obj     =  new GetResponse();
            $api_key = $responses[0]['u_e_c_key'];
            $api_url = "http://api2.getresponse.com";
            $client  = $obj->initiate($api_url);

            $campaigns = $obj->get_campaigns(
                $api_key
            );

            $mycompains = array_keys($campaigns);

            /*
             * if compain is found and valid
             */

            if(isset($mycompains[0]) && !empty($mycompains)){
                $CAMPAIGN_ID = $mycompains[0];
            }else{
                return false;
            }

            foreach ($lists as $perlist) {

                $emailId  = $perlist['email_address'];

                # add contact to the campaign
                $result = $obj->add_contact(
                    $api_key,
                    array (
                        'campaign'  => $CAMPAIGN_ID,
                        'email'     => $perlist['email_address'],
                    )
                );

            }

            /*
             * send the compaign
             */

            $from_field  = Auth::user()->email;
            $params      = array('campaign' => $CAMPAIGN_ID, 'subject' => $subject);

            if(is_string("$email_body")) $params['content']['html'] =  (string)$email_body;
            if(is_string("$from_field")) $params['from_field'] = (string)$from_field;

            echo '<pre>';

            print_r($params);

            die;

            $sending = $obj->send_newsletter(
                $api_key,
                $params);

            echo '<pre>';

            print_r($sending);

            die;

            echo 'operation completed';

            die;

            /*
             * create the compain and send them emails
             */

            if(isset($com_created['id'])){

                $c_id = $com_created['id'];
                $final_response = MailchimpWrapper::Campaigns()->send($c_id);

                if(isset($final_response['complete']) && $final_response['complete'] == 1){
                    return true;
                }

            }else{
                return false;
            }

            return false;

        }


        //1 for mailchimp mandrill
        /*if($serviceId == 1){

                    foreach ($lists as $perlist) {

                        $emailId = $perlist['email_address'];

                        $condition = array(
                            'u_e_c_type'   => 1,
                            'u_e_c_userid' => Auth::user()->id
                        );

                        $responses = Usersemailsclients::select("*")->where($condition)->get()->toArray();

                        $email_body = View::make('emails/email_survey', compact('input', 'id', 'emailId'));

                        $mandrill = new Mandrill($responses[0]['u_e_c_key']);

                        $message = array(
                                'html' => "$email_body",
                                'subject'    => $subject,
                                'from_email' => Auth::user()->email,
                                'from_name'  => Auth::user()->first_name.' '.Auth::user()->last_name,
                                'to' => array(
                                    array(
                                        'email' => $emailId,
                                        'name' => '',
                                        'type' => 'to'
                                    )
                                ),
                                'headers' => array('Reply-To' => 'noreply@noreply.com'),
                            );

                            $async   = false;

                            $mandrill->messages->send($message, $async);

                    }

        }*/



    }

    /**
     * @param
     * @return \Illuminate\View\View
     */

    public function emailConfigurations(){

        $title = "Survey Management System ";
        $con_title = "Dashboard";

        $id = Auth::user()->id;

        $terms = SurveysModel::select()->where('s_user_id', $id)->get();

        return View::make('wtadmin/emails/dashboard', compact('con_title', 'title', 'terms'));


    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */

    function surveyDetails()
    {
        $title = "Survey Management System ";
        $con_title = "Dashboard";

        $id = Auth::user()->id;

        $terms = SurveysModel::select()->where('s_user_id', $id)->get();

        return View::make('wtadmin/survey/surveys_dashboard', compact('con_title', 'title', 'terms'));


    }

    public function postCreate()
    {

        // Declare the rules for the form validation
        $rules = array(
            'name' => 'required',
            'valid_date' => 'required|date',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);
        // Check if the form validates with success
        if ($validator->passes()) {
            // Get the inputs, with some exceptions
            $inputs = Input::except('csrf_token');

            $Cate = Category::select()->where('survey.s_id', Auth::user()->s_id)->where('cat_name', $inputs['name'])->get()->toArray();
            if (isset($Cate[0]) && !empty($Cate[0])) {
                return Redirect::to('wtadmin/survey/create')->with('error', Lang::get('wtadmin/survey/messages.already'));
            }

            //get the required base path set in base controller

            if (Input::hasFile('upload_cat_image') && Input::file('upload_cat_image')->isValid()) {
                $destinationPath = $this->make_path();
                $name = Input::file('upload_cat_image')->getClientOriginalName();

                $replace_arr = array(' ', '%', '_', '+');

                $new_file_name = strtolower(str_replace($replace_arr, '-', rand(1, 1000) . '-cate-' . $name));
                Input::file('upload_cat_image')->move($destinationPath, $new_file_name);
                $path = $this->make_path('', true);
                $path_new = $path . $new_file_name;
                $complete_path = $this->make_path('', false) . $new_file_name;

                $res = $this->resize_image($destinationPath, $new_file_name);

                if ($res) {

                    $thumb_new_path = $path . $res;
                    $this->survey->cat_image = $thumb_new_path;
                } else {
                    $thumb_new_path = $path;
                    $this->survey->cat_image = $thumb_new_path;
                }
            }

            $this->survey->url_key = strtolower(str_replace(" ", "_", $inputs['name']));
            $this->survey->cat_name = ucwords($inputs['name']);
            $this->survey->cate_date = ($inputs['valid_date']);
            $this->survey->term_id = ($inputs['terms']);
            $this->survey->comments = ($inputs['comments']);
            $this->survey->s_id = Auth::user()->s_id;
            $this->survey->user_id = Auth::user()->id;
            $this->survey->is_active = $inputs['is_active'];
            $this->survey->save();

            // Was the survey created?
            if ($this->survey->id) {
                // Redirect to the new role page
                return Redirect::to('wtadmin/survey/' . $this->survey->id . '/edit')->with('success', Lang::get('wtadmin/survey/messages.create.success'));
            }

            // Redirect to the new survey page
            return Redirect::to('wtadmin/survey/create')->with('error', Lang::get('wtadmin/survey/messages.create.error'));

            // Redirect to the survey create page
            return Redirect::to('wtadmin/survey/create')->withInput()->with('error', Lang::get('wtadmin/survey/messages.' . $error));
        }

        // Form validation failed
        return Redirect::to('wtadmin/survey/create')->withInput()->withErrors($validator);
    }

    function resize_image($path, $image_path)
    {

        include __DIR__ . '/..//..//library/thumbnail.class.php';

        $imageful = new Thumbnail($path . $image_path, 350, 0, 100, 100);
        $name = 'thumb_' . $image_path;
        $new_name = $path . $name;
        $response = $imageful->save($new_name);
        return $name;

    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return Response
     */
    public function getShow($id)
    {
        // redirect to the frontend
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $survey
     * @return Response
     */
    public function getEdit($survey)
    {
        // Title
        $title = Lang::get('wtadmin/survey/title.survey_update');

        // Selected Terms
        $selectedTerm = Input::old('terms', array());
        $terms = Term::select()->where('term.s_id', Auth::user()->s_id)->get();
        // Show the page
        return View::make('wtadmin/survey/edit', compact('survey', 'terms', 'selectedTerm', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $Permission
     * @return Response
     */
    public function postEdit($survey)
    {
        // Declare the rules for the form validation
        $rules = array(
            'name' => 'required',
            'valid_date' => 'required|date',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes()) {

            $old_name = $survey->cat_name;
            $new_name = Input::get('name');
            if ($old_name != $new_name) {
                $Cate = Category::select()->where('survey.s_id', Auth::user()->s_id)->where('cat_name', Input::get('name'))->get()->toArray();
                if (isset($Cate[0]) && !empty($Cate[0])) {
                    return Redirect::to('wtadmin/survey/' . $survey->id . '/edit')->with('error', Lang::get('wtadmin/survey/messages.already'));
                }
            }

            if (Input::hasFile('upload_cat_image') && Input::file('upload_cat_image')->isValid()) {
                $destinationPath = $this->make_path();
                $name = Input::file('upload_cat_image')->getClientOriginalName();

                $replace_arr = array(' ', '%', '_', '+');

                $new_file_name = strtolower(str_replace($replace_arr, '-', rand(1, 1000) . '-cate-' . $name));
                Input::file('upload_cat_image')->move($destinationPath, $new_file_name);
                $path = $this->make_path('', true);
                $path_new = $path . $new_file_name;
                $complete_path = $this->make_path('', false) . $new_file_name;

                $res = $this->resize_image($destinationPath, $new_file_name);

                if ($res) {
                    $thumb_new_path = $path . $res;
                    $survey->cat_image = $thumb_new_path;
                } else {
                    $thumb_new_path = $path;
                    $survey->cat_image = $thumb_new_path;
                }
            }

            // Update the Permission data
            $survey->url_key = strtolower(str_replace(" ", "_", Input::get('name')));
            $survey->cat_name = ucwords(Input::get('name'));
            $survey->cate_date = (Input::get('valid_date'));
            $survey->term_id = (Input::get('terms'));
            $survey->comments = (Input::get('comments'));
            $survey->user_id = Auth::user()->id;
            $survey->is_active = Input::get('is_active');

            // Was the role updated?
            if ($survey->save()) {
                // Redirect to the Permission page
                return Redirect::to('wtadmin/survey/' . $survey->id . '/edit')->with('success', Lang::get('wtadmin/survey/messages.update.success'));
            } else {
                // Redirect to the role page
                return Redirect::to('wtadmin/survey/' . $survey->id . '/edit')->with('error', Lang::get('wtadmin/survey/messages.update.error'));
            }
        }

        // Form validation failed
        return Redirect::to('wtadmin/survey/' . $survey->id . '/edit')->withInput()->withErrors($validator);
    }

    /**
     * Remove user page.
     *
     * @param $role
     * @return Response
     */
    public function getDelete($survey)
    {
        // Title
        $title = Lang::get('wtadmin/survey/title.survey_delete');

        // Show the page
        return View::make('wtadmin/survey/delete', compact('survey', 'title'));
    }

    public function getPayment()
    {

        $package = Input::segment(3);

        return View::make('wtadmin/payment/payment', compact('package'));

    }

    public function getSurveyDetail()
    {

        $user_id = Auth::user()->id;

        $s_id = Input::segment(3);
        $survey_details = SurveysModel::select("surveys.*","questions.*")
            ->Leftjoin("questions","questions.q_survey_id",'=',"surveys.s_id")
            ->where('s_id', $s_id)
            ->get()->toArray();

        $response_model = ResponseModel::select()
            ->where('r_s_id', $s_id)
            ->get()->toArray();

        $comments_model = CommentsModel::select()
            ->where('c_s_id', $s_id)
            ->get()->toArray();

        $title = Lang::get('wtadmin/survey/title.survey_detail');

        if (isset($survey_details[0]) && is_array($survey_details)) {

            return View::make('wtadmin/survey/detail', compact('survey_details', 'title', 'response_model', 'comments_model'));

        } else {

            return Redirect::to('survey/');


        }

    }

    public function PaymentSuccess(){

        $payment_id = Input::get("paymentId");
        $msg        = Input::get("success");
        $token      = Input::get("token");
        $packageid  = Input::get("package");

        if ($msg == "true" && !empty($payment_id)) {

            $user_id = Auth::user()->id;

            $obj = new PaymentsModel();

            $package_details = PackagesListModel::select()->where('pl_id', $packageid)->get()->toArray();

            $obj->payment_no = $payment_id;
            $obj->payment_user_id = $user_id;
            $obj->payment_charges = $package_details[0]['pl_charges'];

            $obj->save();

            $current_package = PackagesModel::select()->where('p_user_id', Auth::user()->id)->get()->toArray();

            $payment = new PackagesModel;
            $payment->p_user_id = $user_id;
            $payment->p_available_emails = $current_package[0]['p_available_emails'] + $package_details[0]['pl_email_counts'];
            $payment->p_package = $package_details[0]['pl_id'];
            $payment->p_used_emails = $current_package[0]['p_used_emails'];

            $payment->save();

            return View::make('wtadmin/survey/paymentsuccess', compact('survey_details', 'title', 'response_model', 'comments_model'));

        } else {

            $error = $payment_id['msg'];

            $package = Input::get("package");

            return View::make('wtadmin/survey/paymentfailed', compact('error', 'package'));

        }

    }

    public function getMakepayment()
    {

        $paymentType = Input::get('type');

        /*
         * paypal payment here
         */

        if($paymentType == 'paypal') {

            $rules = array(
                'package' => 'required'
            );

            // Validate the inputs
            $validator = Validator::make(Input::all(), $rules);

            // Check if the form validates with success
            if ($validator->passes()) {

                $inputs = Input::except('csrf_token');

                $user_id = Auth::user()->id;

                $package_details = PackagesListModel::select()->where('pl_id', $inputs['package'])->get()->toArray();

                $payment = new PaypalPaymentController;

                $payment_url = $payment->paypal_payment($inputs, $package_details, $user_id);

                /*
                 * success payment url
                 */

                if (!empty($payment_url)) {

                    return Redirect::to($payment_url);

                }else{

                    $package = Input::get("package");
                    return Redirect::to('survey/payment/' . $package)->with('error', Lang::get('wtadmin/survey/messages.payment_error'));

                }

            }

        }

        /*
         * cc payment here
         */

        if($paymentType == 'cc'){

            $rules = array(
                'cc_name' => 'required',
                'cc_number' => 'required',
                'csvcode' => 'required',
                'package' => 'required'
            );

            // Validate the inputs
            $validator = Validator::make(Input::all(), $rules);

            // Check if the form validates with success
            if ($validator->passes()) {

                $inputs = Input::except('csrf_token');

                $user_id = Auth::user()->id;

                $package_details = PackagesListModel::select()->where('pl_id', $inputs['package'])->get()->toArray();

                $payment = new PaypalPaymentController;

                $payment_id = $payment->store($inputs, $package_details, $user_id);

                /*
                 * success payment
                 */

                if ($payment_id['error'] == false) {

                    $obj = new PaymentsModel();

                    $obj->payment_no = $payment_id['msg'];
                    $obj->payment_user_id = $user_id;
                    $obj->payment_charges = $package_details[0]['pl_charges'];

                    $obj->save();

                    $current_package = PackagesModel::select()->where('p_user_id', Auth::user()->id)->get()->toArray();

                    $payment = new PackagesModel;
                    $payment->p_user_id = $user_id;
                    $payment->p_available_emails = $current_package[0]['p_available_emails'] + $package_details[0]['pl_email_counts'];
                    $payment->p_package = $package_details[0]['pl_id'];
                    $payment->p_used_emails = $current_package[0]['p_used_emails'];

                    $payment->save();

                    return View::make('wtadmin/survey/paymentsuccess', compact('survey_details', 'title', 'response_model', 'comments_model'));

                } else {

                    $error = $payment_id['msg'];

                    $package = Input::get("package");

                    return View::make('wtadmin/survey/paymentfailed', compact('error', 'package'));

                }

            } else {

                $package = Input::get("package");
                return Redirect::to('survey/payment/' . $package)->with('error', Lang::get('wtadmin/survey/messages.payment_error'));

            }

        }



    }

    /**
     * Remove the specified user from storage.
     *
     * @param $survey
     * @internal param $id
     * @return Response
     */
    public function postDelete($survey)
    {
        // Was the Permission deleted?
        if ($survey->delete()) {
            // Redirect to the Permission management page
            return Redirect::to('wtadmin/survey')->with('success', Lang::get('wtadmin/survey/messages.delete.success'));
        }

        // There was a problem deleting the role
        return Redirect::to('wtadmin/survey')->with('error', Lang::get('wtadmin/survey/messages.delete.error'));
    }

    /**
     * Show a list of all the surveys formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {

        $survey = Category::select(array('survey.id as id', 'survey.cat_name as FullName', 'survey.url_key as entries', 'survey.created_at'))->where('survey.s_id', Auth::user()->s_id);
        return Datatables::of($survey)
            ->edit_column('entries', '{{{ DB::table(\'pages\')->where(\'cat_id\', \'=\', $id)->count()  }}}')
            ->add_column('content', '<a href="{{{ URL::to(\'wtadmin/pages/\' . $id . \'/\' ) }}}" class="btn default">{{{ Lang::get(\'button.list\') }}}</a>
                        ')
            ->add_column('actions', ' <div style="width:210px !important"> <a href="{{{ URL::to(\'wtadmin/survey/\' . $id . \'/edit\' ) }}}" class="btn default" data-toggle="lists/data"><i class="fa fa-edit"></i></a>
                                <a href="{{{ URL::to(\'wtadmin/survey/\' . $id . \'/delete\' ) }}}" class="iframe_del btn red" data-toggle=""><i class="fa fa-trash"></i></a></div>
                        ')
            ->edit_column('created_at', function ($result_obj) {
                //in a callback, the Eloquent object is returned so carbon may be used
                return $result_obj->created_at->format('d/m/Y h:i A');
            })
            ->remove_column('id')
            ->make();
    }

    public function getRecords()
    {

        $survey = SurveysModel::select(array('created_at', 's_name', 's_id'))->orderBy('created_at', 'desc')->where('surveys.s_user_id', Auth::user()->id);
        return Datatables::of($survey)
            ->edit_column('created_at', function ($result_obj) {
                //in a callback, the Eloquent object is returned so carbon may be used
                return $result_obj->created_at->format('d/m/Y h:i A');
            })
            ->edit_column('s_name', function ($result_obj) {
                //in a callback, the Eloquent object is returned so carbon may be used
                return '<a href="' . URL::to('survey/detail') . '/' . $result_obj->s_id . '">' . $result_obj->s_name . '</a>';
            })
            ->edit_column('s_id', function ($result_obj) {

                return '<a class="btn btn-info" href="' . URL::to('survey/detail') . '/' . $result_obj->s_id . '">Details</a>';

            })
            ->make();

    }

    public function getFullRecords()
    {

        $survey = SurveysModel::select(array('s_id','s_name','created_at'))
            ->orderBy('created_at', 'desc')
            ->where('surveys.s_user_id', Auth::user()->id);

        return Datatables::of($survey)
            ->edit_column('created_at', function ($result_obj) {
                //in a callback, the Eloquent object is returned so carbon may be used
                return $result_obj->created_at->format('d/m/Y h:i A');
            })
            ->edit_column('detail', function ($result_obj) {
                return '<a class="btn btn-info" href="' . URL::to('survey/detail') . '/' . $result_obj->s_id . '">Details</a>';
            })
            ->make();

    }


    public function getResponse()
    {

        $responses = ResponseModel::select(array('responses.created_at', 'surveys.s_name','questions.s_question', 'responses.r_response', 'surveys.s_name'))
            ->join('surveys', 'surveys.s_id', '=', 'responses.r_s_id')
            ->join('questions', 'questions.q_id', '=', 'responses.s_question')
            ->orderBy('responses.created_at', 'desc')
            ->where('surveys.s_user_id', Auth::user()->id);

        return Datatables::of($responses)
            ->edit_column('created_at', function ($result_obj) {
                //in a callback, the Eloquent object is returned so carbon may be used
                return $result_obj->created_at->format('d/m/Y h:i A');
            })
            ->edit_column('detail', function ($result_obj) {
                return '<a class="btn btn-info" href="' . URL::to('survey/detail') . '/' . $result_obj->s_id . '">Details</a>';
            })
            ->make();

    }

    public function getComments()
    {

        $responses = ResponseModel::select(array('responses.created_at', 'surveys.s_name', 'comments.c_text'))
            ->join('surveys', 'surveys.s_id', '=', 'responses.r_s_id')
            ->join('comments', 'comments.c_s_id', '=', 'responses.r_s_id')
            ->orderBy('comments.created_at', 'desc')
            ->where('surveys.s_user_id', Auth::user()->id);

        return Datatables::of($responses)
            ->edit_column('surveys.s_name', function ($result_obj) {
                //in a callback, the Eloquent object is returned so carbon may be used
                return '<a href="' . URL::to('survey/detail') . '/' . $result_obj->s_id . '">' . $result_obj->s_name . '</a>';
            })
            ->edit_column('comments.created_at', function ($result_obj) {
                //in a callback, the Eloquent object is returned so carbon may be used
                return $result_obj->created_at->format('d/m/Y h:i A');
            })
            ->make();

    }


    public function getPackage()
    {

        $responses = PackagesListModel::select(array('packages_lists.pl_id', 'packages_lists.pl_name', 'packages_lists.pl_email_counts', 'packages_lists.pl_charges', 'packages_lists.created_at'));

        return Datatables::of($responses)
            ->edit_column('created_at', function ($result_obj) {
                //in a callback, the Eloquent object is returned so carbon may be used
                return $result_obj->created_at->format('d/m/Y h:i A');
            })
            ->edit_column('sub_action', function ($result_obj) {
                //in a callback, the Eloquent object is returned so carbon may be used

                $id = Input::segment(3);

                if ($id >= $result_obj->pl_id) {
                    return '<a href="">Already Subscribed</a>';
                } else {
                    return '<a href="' . URL::to('survey/payment') . '/' . $result_obj->pl_id . '">Subscribe</a>';
                }

            })
            ->make();

    }

    public function getSubscription()
    {

        $responses = PackagesModel::select(array('packages_lists.pl_id', 'packages_lists.pl_name', 'packages.p_used_emails', 'packages_lists.pl_email_counts', 'packages.created_at'))
            ->join('packages_lists', 'packages_lists.pl_id', '=', 'packages.p_package')
            ->orderBy('packages.created_at', 'desc')
            ->where('packages.p_user_id', Auth::user()->id);

        return Datatables::of($responses)
            ->edit_column('created_at', function ($result_obj) {
                //in a callback, the Eloquent object is returned so carbon may be used
                return $result_obj->created_at->format('d/m/Y h:i A');
            })
            ->make();

    }

    public function getResponseDetail()
    {

        $s_id = Input::segment(3);

        $responses = ResponseModel::select(array('responses.s_question', 'responses.r_response','responses.r_comments','responses.r_email','responses.created_at'))
            ->join('surveys', 'surveys.s_id', '=', 'responses.r_s_id')
            ->orderBy('responses.created_at', 'desc')
            ->where('surveys.s_id', $s_id);

        return Datatables::of($responses)
            ->edit_column('responses.created_at', function ($result_obj) {
                //in a callback, the Eloquent object is returned so carbon may be used
                return $result_obj->created_at->format('d/m/Y h:i A');
            })
            ->make();

    }

    public function newSurveyhtml()
    {

        $id = Input::get('count');

        if (empty($id)) {
            return 'false';
        }

        return View::make('wtadmin/survey/newquestion', compact('id'));

    }

}
