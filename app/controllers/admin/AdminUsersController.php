<?php

class AdminUsersController extends AdminController {

    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Role Model
     * @var Role
     */
    protected $role;

    /**
     * Permission Model
     * @var Permission
     */
    protected $permission;


    /**
     * Inject the models.
     * @param User $user
     * @param Role $role
     * @param Permission $permission
     */
    public function __construct(User $user, Role $role, Permission $permission) {
        parent::__construct();
        $this->user = $user;
        $this->role = $role;
        $this->permission = $permission;
       
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex() {
        // Title
        $title = Lang::get('wtadmin/users/title.user_management');

        // Grab all the users
        $users = $this->user;

        // Show the page
        return View::make('wtadmin/users/index', compact('users', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate() {
        // All roles
//        $roles = $this->role->all();
        $roles = $this->role->select()->where('s_id', Auth::user()->s_id)->get();
        $roles[] = $this->role->find(3);

        // Get all the available permissions
//     $permissions = $this->permission->all();
        $permissions = $this->permission->select()->where('s_id', Auth::user()->s_id)->get();

        // Selected groups
        $selectedRoles = Input::old('roles', array());

        // Selected permissions
        $selectedPermissions = Input::old('permissions', array());

        // Title
        $title = Lang::get('wtadmin/users/title.create_a_new_user');

        // Mode
        $mode = 'create';

        // Show the page
        return View::make('wtadmin/users/create_edit', compact('roles', 'permissions', 'selectedRoles', 'selectedPermissions', 'title', 'mode'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate() {

        $this->user->username = Input::get('username');
        $this->user->s_id = Auth::user()->s_id;
        $this->user->email = Input::get('email');
        $this->user->password = Input::get('password');

        // The password confirmation will be removed from model
        // before saving. This field will be used in Ardent's
        // auto validation.
        $this->user->password_confirmation = Input::get('password_confirmation');
        $this->user->confirmed = Input::get('confirm');

        // Permissions are currently tied to roles. Can't do this yet.
        //$user->permissions = $user->roles()->preparePermissionsForSave(Input::get( 'permissions' ));
        // Save if valid. Password field will be hashed before save
        $this->user->save();

        if ($this->user->id) {
            // Save roles. Handles updating.
            $this->user->saveRoles(Input::get('roles'));
            // Redirect to the new user page
            return Redirect::to('survey/users/' . $this->user->id . '/edit')->with('success', Lang::get('wtadmin/users/messages.create.success'));
        } else {
            // Get validation errors (see Ardent package)
            $error = $this->user->errors()->all();

            return Redirect::to('survey/users/create')
                            ->withInput(Input::except('password'))
                            ->with('error', $error);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getShow($user) {
        // redirect to the frontend
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getEdit($user) {
        if ($user->id) {
//            $roles = $this->role->all();
//            $permissions = $this->permission->all();
//
//            $grades = $this->grade->all();

            $roles = $this->role->select()->where('s_id', Auth::user()->s_id)->get();
            $roles[] = $this->role->find(3);

            // Get all the available permissions
            //     $permissions = $this->permission->all();
            $permissions = $this->permission->select()->where('s_id', Auth::user()->s_id)->get();

            // Title
            $title = Lang::get('wtadmin/users/title.user_update');
            // mode
            $mode = 'edit';

            return View::make('wtadmin/users/create_edit', compact('user', 'roles', 'permissions', 'title', 'mode'));
        } else {
            return Redirect::to('survey/users')->with('error', Lang::get('wtadmin/users/messages.does_not_exist'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $user
     * @return Response
     */
    public function postEdit($user) {
        // Validate the inputs
        $validator = Validator::make(Input::all(), $user->getUpdateRules());


        if ($validator->passes()) {
            $oldUser = clone $user;
            $user->username = Input::get('username');
            $user->email = Input::get('email');
            $user->confirmed = Input::get('confirm');

            $password = Input::get('password');
            $passwordConfirmation = Input::get('password_confirmation');

            if (!empty($password)) {
                if ($password === $passwordConfirmation) {
                    $user->password = $password;
                    // The password confirmation will be removed from model
                    // before saving. This field will be used in Ardent's
                    // auto validation.
                    $user->password_confirmation = $passwordConfirmation;
                } else {
                    // Redirect to the new user page
                    return Redirect::to('survey/users/' . $user->id . '/edit')->with('error', Lang::get('wtadmin/users/messages.password_does_not_match'));
                }
            } else {
                unset($user->password);
                unset($user->password_confirmation);
            }

            if ($user->confirmed == null) {
                $user->confirmed = $oldUser->confirmed;
            }

            $user->prepareRules($oldUser, $user);

            // Save if valid. Password field will be hashed before save
            $user->amend();

            // Save roles. Handles updating.
            $user->saveRoles(Input::get('roles'));

        } else {
            return Redirect::to('survey/users/' . $user->id . '/edit')->with('error', Lang::get('wtadmin/users/messages.edit.error'));
        }

        // Get validation errors (see Ardent package)
        $error = $user->errors()->all();

        if (empty($error)) {
            // Redirect to the new user page
            return Redirect::to('survey/users/' . $user->id . '/edit')->with('success', Lang::get('wtadmin/users/messages.edit.success'));
        } else {
            return Redirect::to('survey/users/' . $user->id . '/edit')->with('error', Lang::get('wtadmin/users/messages.edit.error'));
        }
    }

    /**
     * Remove user page.
     *
     * @param $user
     * @return Response
     */
    public function getDelete($user) {
        // Title
        $title = Lang::get('wtadmin/users/title.user_delete');

        // Show the page
        return View::make('wtadmin/users/delete', compact('user', 'title'));
    }

    /**
     * Remove the specified user from storage.
     *
     * @param $user
     * @return Response
     */
    public function postDelete($user) {
        // Check if we are not trying to delete ourselves
        if ($user->id === Confide::user()->id) {
            // Redirect to the user management page
            return Redirect::to('survey/users')->with('error', Lang::get('wtadmin/users/messages.delete.impossible'));
        }

        AssignedRoles::where('user_id', $user->id)->delete();

        $id = $user->id;
        $user->delete();
        
        // Was the comment post deleted?
        $user = User::find($id);
        if (empty($user)) {
            // TODO needs to delete all of that user's content
            return Redirect::to('survey/users')->with('success', Lang::get('wtadmin/users/messages.delete.success'));
        } else {
            // There was a problem deleting the user
            return Redirect::to('survey/users')->with('error', Lang::get('wtadmin/users/messages.delete.error'));
        }
    }


     /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getImportUser() {
        $title = "Import Users";
        // Show the page
        return View::make('wtadmin/users/importuser', compact('title'));
    }

    /**
     * Import Users From CSV.
     *
     * @param $user
     * @return Response
     */
    public function importUser() {
        // Check if we are not trying to delete ourselves
//        print_r($_REQUEST);
//        exit;
        $file_name = Input::get("file_name");
        $user_id = Input::get("user_id");
        $s_id = Input::get("s_id");
        $file_imported_id = Input::get("i_id");
        $hash = Input::get("hash");

        $empty_error = false;

        Excel::load($file_name, function($reader) {
            $result = $reader->toArray();
            if (!empty($result)) {
                foreach ($result as $row) {
                    $username = $row['username'];
                    $email = $row['email'];
                    $password = $row['password'];
                    $is_active = $row['is_active'];
                    $grade = $row['group'];
                    $roles_name = $row['roles'];
                    $row['password_confirmation'] = $password;
                    $row['confirm'] = $is_active;
                    $row['s_id'] = Auth::user()->s_id;


                    $grade_id = '';
                    $grades = $this->grade->select()->where('grades.s_id', Auth::user()->s_id)->where('grades.gr_name', 'LIKE', '%' . $grade . '%')->get()->toArray();
                    if (isset($grades[0]) && !empty($grades[0])) {
                        $grade_id = $grades[0]['id'];
                    }
                    $row['grades'] = $grade_id;

                    $role_id = array();
                    $role = new Role;
                    $roles = $role->select()->where('roles.s_id', Auth::user()->s_id)->where('roles.name', 'LIKE', '%' . $roles_name . '%')->get()->toArray();
                    if (isset($roles[0]) && !empty($roles[0])) {
                        $role_id[] = $roles[0]['id'];
                    }
                    $row['roles'] = $role_id;
                    Input::merge($row);
                    $user = new User;


                    $user->username = Input::get('username');
                    $user->s_id = Auth::user()->s_id;
                    $user->email = Input::get('email');
                    $user->password = Input::get('password');

                    $user->password_confirmation = Input::get('password_confirmation');
                    $user->confirmed = Input::get('is_active');


                    $user->save();
                    if ($user->id) {
                        echo $user->id;
                        // Save roles. Handles updating.
                        $user->saveRoles(Input::get('roles'));

                        $userGrade = UserGrades::find($user->id);
                        if (empty($userGrade)) {
                            $userGrade = new UserGrades;
                        }

                        $userGrade->id = $user->id;
                        $userGrade->user_id = $user->id;
                        $userGrade->s_id = Auth::user()->s_id;
                        $userGrade->grade_id = Input::get('grades');
                        $userGrade->save();
                        // Redirect to the new user page
                    } else {
                        // Get validation errors (see Ardent package)
//                        $error = $this->user->errors()->all();
//                        return Redirect::to('survey/users')->withInput(Input::except('password'))->with('error', $error);
                    }
                }
//                return Redirect::to('survey/users/')->with('success', Lang::get('wtadmin/users/messages.import.success'));
            } else {
                $empty_error = true;
//                return Redirect::to('survey/users/')->with('error', Lang::get('wtadmin/users/messages.import.empty_array'));
            }
        });
        $error = $this->user->errors()->all();
        if (!empty($error))
            return Redirect::to('survey/users/import')->withInput(Input::except('password'))->with('error', $error);
        elseif ($empty_error)
            return Redirect::to('survey/users/import')->with('error', Lang::get('wtadmin/users/messages.import.empty_array'));
        else
            return Redirect::to('survey/users/import')->with('success', Lang::get('wtadmin/users/messages.import.success'));
    }

    /**
     * Show a list of all the users formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData() {
       
        $users = User::select(array('users.id as user_id', 'users.username', 'users.email', 'users.confirmed', 'users.created_at'))
                ->where('users.is_admin', '=', '2');

        return Datatables::of($users)
                        // ->edit_column('created_at','{{{ Carbon::now()->diffForHumans(Carbon::createFromFormat(\'Y-m-d H\', $test)) }}}')
                        
                        ->edit_column('confirmed', '@if($confirmed)
                            Yes
                        @else
                            No
                        @endif')
                        ->add_column('actions', '<a href="{{{ URL::to(\'survey/users/\' . $user_id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-default">{{{ Lang::get(\'button.edit\') }}}</a>
                                @if($username == \'survey\')
                                @else
                                    <a href="{{{ URL::to(\'survey/users/\' . $user_id . \'/delete\' ) }}}" class="iframe btn btn-xs btn-danger">{{{ Lang::get(\'button.delete\') }}}</a>
                                @endif
            ')
                        ->remove_column('user_id')
//                        ->remove_column('email')
                        ->make();
    }

}
