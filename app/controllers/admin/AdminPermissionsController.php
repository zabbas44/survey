<?php

class AdminPermissionsController extends AdminController {

    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Role Model
     * @var Role
     */
    protected $role;

    /**
     * Permission Model
     * @var Permission
     */
    protected $permission;

    /**
     * Inject the models.
     * @param User $user
     * @param Role $role
     * @param Permission $permission
     */
    public function __construct(User $user, Role $role, Permission $permission) {
        parent::__construct();
        $this->user = $user;
        $this->role = $role;
        $this->permission = $permission;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex() {
        //    $manageUsers = new Permission;
//    $manageUsers->name = 'manage_users';
//    $manageUsers->display_name = 'Manage Users';
//    $manageUsers->save();
        // Title
        $title = Lang::get('wtadmin/permissions/title.permission_management');

        // Grab all the Permission Lists
        $permissions = $this->permission;

        // Show the page
        return View::make('wtadmin/permissions/index', compact('permissions', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate() {
        // Get all the available permissions
        $permissions = $this->permission->all();

        // Selected permissions
        $selectedPermissions = Input::old('permissions', array());

        // Title
        $title = Lang::get('wtadmin/permissions/title.create_a_new_permission');

        // Show the page
        return View::make('wtadmin/permissions/create', compact('permissions', 'selectedPermissions', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate() {

        // Declare the rules for the form validation
        $rules = array(
            'name' => 'required'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);
        // Check if the form validates with success
        if ($validator->passes()) {
            // Get the inputs, with some exceptions
            $inputs = Input::except('csrf_token');

            $this->permission->name = strtolower(str_replace(" ", "_", $inputs['name']));
            $this->permission->display_name = ucwords($inputs['name']);
            $this->permission->s_id = Auth::user()->s_id;
            $this->permission->is_scope_all = 1;
            $this->permission->save();

            // Was the permission created?
            if ($this->permission->id) {
                $role = Role::find(3);
                $role->perms()->sync(array($this->permission->id));
                // Redirect to the new role page
                return Redirect::to('survey/permissions/' . $this->permission->id . '/edit')->with('success', Lang::get('wtadmin/permissions/messages.create.success'));
            }

            // Redirect to the new role page
            return Redirect::to('survey/permissions/create')->with('error', Lang::get('wtadmin/permissions/messages.create.error'));

            // Redirect to the role create page
            return Redirect::to('survey/permissions/create')->withInput()->with('error', Lang::get('wtadmin/permissions/messages.' . $error));
        }

        // Form validation failed
        return Redirect::to('survey/permissions/create')->withInput()->withErrors($validator);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return Response
     */
    public function getShow($id) {
        // redirect to the frontend
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $role
     * @return Response
     */
    public function getEdit($permission) {
        // Title
        $title = Lang::get('wtadmin/permissions/title.permission_update');

        // Show the page
        return View::make('wtadmin/permissions/edit', compact('permission', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $Permission
     * @return Response
     */
    public function postEdit($permission) {
        // Declare the rules for the form validation
        $rules = array(
            'name' => 'required'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes()) {
            // Update the Permission data
            $permission->name = strtolower(str_replace(" ", "_", Input::get('name')));
            $permission->display_name = ucwords(Input::get('name'));
            // Was the role updated?
            if ($permission->save()) {
                // Redirect to the Permission page
                return Redirect::to('survey/permissions/' . $permission->id . '/edit')->with('success', Lang::get('wtadmin/permissions/messages.update.success'));
            } else {
                // Redirect to the role page
                return Redirect::to('survey/permissions/' . $permission->id . '/edit')->with('error', Lang::get('wtadmin/permissions/messages.update.error'));
            }
        }

        // Form validation failed
        return Redirect::to('survey/permissions/' . $permission->id . '/edit')->withInput()->withErrors($validator);
    }

    /**
     * Remove user page.
     *
     * @param $role
     * @return Response
     */
    public function getDelete($permission) {
        // Title
        $title = Lang::get('wtadmin/permissions/title.permission_delete');

        // Show the page
        return View::make('wtadmin/permissions/delete', compact('permission', 'title'));
    }

    /**
     * Remove the specified user from storage.
     *
     * @param $permission
     * @internal param $id
     * @return Response
     */
    public function postDelete($permission) {
        // Was the Permission deleted?
        if ($permission->delete()) {
            // Redirect to the Permission management page
            return Redirect::to('survey/permissions')->with('success', Lang::get('wtadmin/permissions/messages.delete.success'));
        }

        // There was a problem deleting the role
        return Redirect::to('survey/permissions')->with('error', Lang::get('wtadmin/permissions/messages.delete.error'));
    }

    /**
     * Show a list of all the permissions formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData() {
        $permissions = Permission::select(array('permissions.id', 'permissions.display_name as FullName', 'permissions.name', 'permissions.created_at'))
                ->where('permissions.s_id', Auth::user()->s_id)
                ->orwhere('is_scope_all', "2")
        ;

        return Datatables::of($permissions)
                        ->add_column('actions', '<a href="{{{ URL::to(\'wtadmin/permissions/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-default">{{{ Lang::get(\'button.edit\') }}}</a>
                                <a href="{{{ URL::to(\'wtadmin/permissions/\' . $id . \'/delete\' ) }}}" class="iframe btn btn-xs btn-danger">{{{ Lang::get(\'button.delete\') }}}</a>
                    ')
                        ->remove_column('id')
                        ->make();
    }

}
