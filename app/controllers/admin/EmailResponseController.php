<?php

    class EmailResponseController extends AdminController
    {
        protected $url = null, $is_debug = false, $parameters_structure = 'array';

        /**
         * Default options for curl
         *
         * @var array
         */

        public function get()
        {

            $obj =  new GetResponse();

            $api_key = Input::get("option");
            $api_url = "http://api2.getresponse.com";

            $client = $obj->initiate($api_url);

            $response = $obj->get_account_info(
                $api_key
            );

            $lists = "";

            if (!isset($response['email']) && empty($response['email'])) {
                $lists = 'Api key not valid!';
                $error = true;
            } else {
                $error = false;
            }

            return View::make('wtadmin/emails/clients/getresponse_data', compact("error", "lists"));

        }
    }