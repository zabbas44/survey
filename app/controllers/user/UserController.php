<?php

class UserController extends BaseController {

    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Inject the models.
     * @param User $user
     */
    public function __construct(User $user) {
        parent::__construct();
        $this->user = $user;
    }

    /**
     * Users settings page
     *
     * @return View
     */
    public function getIndex() {
        list($user, $redirect) = $this->user->checkAuthAndRedirect('user');
        if ($redirect) {
            return $redirect;
        }

        // Show the page
        return View::make('site/user/index', compact('user'));
    }

    /**
     * Stores new user
     *
     */
    public function postIndex() {

        $rules = array(
            'email' => 'required|email|unique:users,email',
            'first_name' => 'required|alpha_dash',
            'last_name' => 'required|alpha_dash',
            //'company_name' => 'required|alpha_dash',
            'password' => 'required|min:4|confirmed',
            'password_confirmation' => 'min:4',
        );
        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);
        // Check if the form validates with success
        if ($validator->passes()) {
            $this->user->first_name = Input::get('first_name');
            $this->user->last_name = Input::get('last_name');
            $this->user->company_name = Input::get('company_name');
            $this->user->email = Input::get('email');
            $this->user->username = str_replace(array(' ', '_'), '-', Input::get('first_name') . ' ' . Input::get('last_name'));

            $password = Input::get('password');
            $passwordConfirmation = Input::get('password_confirmation');

            if (!empty($password)) {
                if ($password === $passwordConfirmation) {
                    $this->user->password = $password;
                    // The password confirmation will be removed from model
                    // before saving. This field will be used in Ardent's
                    // auto validation.
                    $this->user->password_confirmation = $passwordConfirmation;
                } else {
                    // Redirect to the new user page
                    return Redirect::to('user/create')
                                    ->withInput(Input::except('password', 'password_confirmation'))
                                    ->with('error', Lang::get('wtadmin/users/messages.password_does_not_match'));
                }
            } else {
                unset($this->user->password);
                unset($this->user->password_confirmation);
            }

            // Save if valid. Password field will be hashed before save
            $this->user->save();

            $userPackage = new PackagesModel;

            $userPackage->p_user_id          = $this->user->id;
            $userPackage->p_used_emails      = 0;
            $userPackage->p_available_emails = 500;
            $userPackage->p_package          = 1;

            $userPackage->save();

            if ($this->user->id) {
                // Redirect with success message, You may replace "Lang::get(..." for your custom message.
                return Redirect::to('user/login')
                                ->with('notice', Lang::get('user/user.user_account_created'));
            } else {
                // Get validation errors (see Ardent package)
                $error = $this->user->errors()->all();

                return Redirect::to('user/create')
                                ->withInput(Input::except('password'))
                                ->with('error', $error);
            }
        }
        // Form validation failed
        return Redirect::to('user/create')->withInput()->withErrors($validator);
    }

    /**
     * Edits a user
     *
     */
    public function postEdit($user) {
        // Validate the inputs
        $validator = Validator::make(Input::all(), $user->getUpdateRules());


        if ($validator->passes()) {
            $oldUser = clone $user;
            $user->username = Input::get('username');
            $user->email = Input::get('email');

            $password = Input::get('password');
            $passwordConfirmation = Input::get('password_confirmation');

            if (!empty($password)) {
                if ($password === $passwordConfirmation) {
                    $user->password = $password;
                    // The password confirmation will be removed from model
                    // before saving. This field will be used in Ardent's
                    // auto validation.
                    $user->password_confirmation = $passwordConfirmation;
                } else {
                    // Redirect to the new user page
                    return Redirect::to('users')->with('error', Lang::get('wtadmin/users/messages.password_does_not_match'));
                }
            } else {
                unset($user->password);
                unset($user->password_confirmation);
            }

            $user->prepareRules($oldUser, $user);

            // Save if valid. Password field will be hashed before save
            $user->amend();
        }

        // Get validation errors (see Ardent package)
        $error = $user->errors()->all();

        if (empty($error)) {
            return Redirect::to('user')
                            ->with('success', Lang::get('user/user.user_account_updated'));
        } else {
            return Redirect::to('user')
                            ->withInput(Input::except('password', 'password_confirmation'))
                            ->with('error', $error);
        }
    }

    /**
     * Displays the form for user creation
     *
     */
    public function getCreate() {
        return View::make('site/user/create');
    }

    /**
     * Displays the login form
     *
     */
    public function getLogin() {

        $user = Auth::user();
        if (!empty($user->id)) {
            return Redirect::to('/survey');
        }
        return View::make('site/user/login');
    }

    /**
     * Attempt to do login
     *
     */
    public function postLogin() {
        $rules = array(
            'email' => 'required|email',
            'password' => 'required',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);
        // Check if the form validates with success
        if ($validator->passes()) {
            $input = array(
                'email' => Input::get('email'), // May be the username too
                'is_active' => 1,
                'password' => Input::get('password'),
                'remember' => Input::get('remember'),
            );

            // If you wish to only allow login from confirmed users, call logAttempt
            // with the second parameter as true.
            // logAttempt will check if the 'email' perhaps is the username.
            // Check that the user is confirmed.
            if (Confide::logAttempt($input, true)) {

                if (Auth::check())
                    return Redirect::intended('/survey');
            }
            else {
//            $queries = DB::getQueryLog();
//            $last_query = end($queries);
//            print_r($last_query);
//            exit;
                $in_arr = $input;
                unset($in_arr['is_active']);
                unset($in_arr['password']);

                // Check if there was too many login attempts
                if (Confide::isThrottled($input)) {
                    $err_msg = Lang::get('confide::confide.alerts.too_many_attempts');
                } elseif ($this->user->checkUserExists($input) && !$this->user->isConfirmed($input)) {
                    $err_msg = Lang::get('confide::confide.alerts.not_confirmed');
                } elseif ($this->user->checkUserExists($input) && !$this->user->isActive_login($input)) {
                    $err_msg = Lang::get('confide::confide.alerts.not_active');
                } else {
                    $err_msg = Lang::get('confide::confide.alerts.wrong_credentials_email');
                }

                return Redirect::to('user/login')
                                ->withInput(Input::except('password'))
                                ->with('error', $err_msg);
            }
        } else {
            return Redirect::to('user/login')->withInput()->withErrors($validator);
        }
    }

    /**
     * Attempt to confirm account with code
     *
     * @param  string  $code
     */
    public function getConfirm($code) {
        if (Confide::confirm($code)) {
            return Redirect::to('user/login')
                            ->with('notice', Lang::get('confide::confide.alerts.confirmation'));
        } else {
            return Redirect::to('user/login')
                            ->with('error', Lang::get('confide::confide.alerts.wrong_confirmation'));
        }
    }

    /**
     * Displays the forgot password form
     *
     */
    public function getForgot() {
        return View::make('site/user/forgot', array());
    }

    /**
     * Attempt to reset password with given email
     *
     */
    public function postForgot() {
        if (Confide::forgotPassword(Input::get('email'))) {
            return Redirect::to('user/login')
                            ->with('notice', Lang::get('confide::confide.alerts.password_forgot'));
        } else {
            return Redirect::to('user/forgot')
                            ->withInput()
                            ->with('error', Lang::get('confide::confide.alerts.wrong_password_forgot'));
        }
    }

    /**
     * Shows the change password form with the given token
     *
     */
    public function getReset($token) {

        return View::make('site/user/reset')
                        ->with('token', $token);
    }

    /**
     * Attempt change password of the user
     *
     */
    public function postReset() {
        $input = array(
            'token' => Input::get('token'),
            'password' => Input::get('password'),
            'password_confirmation' => Input::get('password_confirmation'),
        );
        $rules = array(
            'token' => 'required',
            'password' => 'required|min:4|Confirmed',
            'password_confirmation' => 'required|min:4',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);
        // Check if the form validates with success
        if ($validator->passes()) {
            // By passing an array with the token, password and confirmation
            if (Confide::resetPassword($input)) {
                return Redirect::to('user/login')
                                ->with('notice', Lang::get('confide::confide.alerts.password_reset'));
            } else {
                return Redirect::to('user/reset/' . $input['token'])
                                ->withInput()
                                ->with('error', Lang::get('confide::confide.alerts.wrong_password_reset'));
            }
        } else {
            return Redirect::to('user/reset/' . $input['token'])->withInput()->withErrors($validator);
        }
    }

    /**
     * Log the user out of the application.
     *
     */
    public function getLogout() {
        Confide::logout();

        return Redirect::to('/');
    }

    /**
     * Get user's profile
     * @param $username
     * @return mixed
     */
    public function getProfile($username) {
        $userModel = new User;
        $user = $userModel->getUserByUsername($username);

        // Check if the user exists
        if (is_null($user)) {
            return App::abort(404);
        }

        return View::make('site/user/profile', compact('user'));
    }

    public function getSettings() {
        list($user, $redirect) = User::checkAuthAndRedirect('user/settings');
        if ($redirect) {
            return $redirect;
        }

        return View::make('site/user/profile', compact('user'));
    }

    /**
     * Process a dumb redirect.
     * @param $url1
     * @param $url2
     * @param $url3
     * @return string
     */
    public function processRedirect($url1, $url2, $url3) {
        $redirect = '';
        if (!empty($url1)) {
            $redirect = $url1;
            $redirect .= (empty($url2) ? '' : '/' . $url2);
            $redirect .= (empty($url3) ? '' : '/' . $url3);
        }
        return $redirect;
    }

    /**
     * Show success page after confirming user/driver
     * @function getSuccesPage($driverId)
     * @param int $driverId DriverId 
     */
    public function getSuccessPage($userId) {

        $user = User::findOrFail($userId);
        $title = 'Your Account has been Confirmed.';
        $hosting = 'http://new_patel.local';
        $data = array();
        $data['user_name'] = $user->username;
        $data['email'] = $user->email;
        $data['store_link'] = HOST_NAME_HTTP;
        $data['date'] = date("Y-m-d");
        // on behalf or the config file we should send and email or not
        $view = 'emails.account_approve';
        $data['title'] = "Thanks for confirming your account!";
        $subject = $data['title'];
        $data['msg_dr'] = "Your Account has been Confirmed. You Can Login and check your orders.";
        sendEmail($subject, $view, $data, array());
        // Show the page
        return View::make('wtadmin/user/succes', compact('user', 'title', 'hosting'));
    }

    /**
     * Show error page on wrong confirmation call
     * @function getErrorPage()
     * @param int $driverId DriverId 
     */
    public function getErrorPage() {

        $title = 'Wrong confirmation.';
        $hosting = HOST_NAME_HTTP;
        // Show the page
        return View::make('wtadmin/user/error', compact('title', 'hosting'));
    }

}
