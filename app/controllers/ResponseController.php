<?php

class ResponseController extends \BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function postComments(){

        // Declare the rules for the form validation
        $rules = array(
            'comments' => 'required',
            'question' => 'required'
        );

        $inputs = Input::except('csrf_token');

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        $id     = $inputs['id'];
        $answer = $inputs['answer'];

        if ($validator->passes()) {

            $objComments = new CommentsModel();

            $objComments->c_s_id  = $id;
            $objComments->c_s_answer = $answer;
            $objComments->c_text  = $inputs['comments'];
            $objComments->c_question_id  = $inputs['question'];

            $objComments->save();

            return View::make('wtadmin/responses/thanks', compact(''));


        }else{
            return View::make('wtadmin/responses/error', compact(''));
        }

    }

    public function getLogEntry(){

        $id       = base64_decode(Request::segment(3));
        $question = base64_decode(Request::segment(4));
        $answer   = base64_decode(Request::segment(5));
        $email    = base64_decode(Request::segment(6));
        $ip       = Request::getClientIp(true);

        if(!empty($id) && !empty($answer) && !empty($email)){

            $obj = new ResponseModel();

            //$lists  = ResponseModel::select()->where('r_email', $email)->where('r_s_id',$id)->where('s_question',$question)->get()->toArray();

            $lists  = ResponseModel::select()->where('r_ip', $ip)->where('r_s_id',$id)->where('s_question',$question)->get()->toArray();

            $info   = SurveysModel::select()->where('s_id', $id)->get()->toArray();

            $question_data   = QuestionModel::select()->where('s_question_order', $question)->where('q_survey_id',$id)->get()->toArray();

            $userinfo = User::select()->where('id', $info[0]['s_user_id'])->get()->toArray();

            if(is_array($info) && !empty($info)){

                $string = 's_'.$answer.'_enable_comments';

                if(count($lists) == 0){

                    $obj->r_email    = $email;
                    $obj->r_s_id     = $id;
                    $obj->r_response = $answer;
                    $obj->s_question = $question;
                    $obj->r_ip = $ip;

                    $obj->save();

                    /*
                     *  notify the creater about this
                     */

                    if(isset($info[0]['notify']) && $info[0]['notify']=='on'){

                        // To send HTML mail, the Content-type header must be set
                        $headers  = 'MIME-Version: 1.0' . "\r\n";
                        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                        $emailId    = $userinfo[0]['email'];

                        $subject  = "Response Regarding Survey : ".$info[0]['s_name'];

                        $email_body = View::make('emails/email_survey_notify',compact('info','userinfo','lists','email'));

                        $response   = mail($emailId,$subject,$email_body,$headers);

                    }

                    if(isset($question_data[0][$string]) && $question_data[0][$string] == 'on'){

                        return View::make('wtadmin/responses/responses', compact('info','id','answer','question_data'));

                    }else{

                        return View::make('wtadmin/responses/thanks', compact(''));

                    }


                }else{

                    return View::make('wtadmin/responses/error', compact(''));

                }

            }else{

                return View::make('wtadmin/responses/error', compact(''));
                die;

            }


        }else{

            return View::make('wtadmin/responses/error', compact(''));

        }


    }




}