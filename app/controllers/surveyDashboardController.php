<?php

class SurveyDashboardController extends \BaseController {


    /**
     * Class constructor.
     * 
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * @function index()
     * Site Home Page.
     * GET /
     * @return Response Home page view
     */
    public function index() {

        return View::make('site/dashboard/index');
            
    }

    /**
     * @function surveyInfo()
     * Site Survey Info Page.
     * GET /
     * @return Response Info page view
     */
    public function surveyInfo() {
        return View::make('site/dashboard/info');
    }
    /**
     * @function yourStory()
     * User Stories/testimonial page.
     * GET /
     * @return Response history page
     */
    public function yourStory() {
        return View::make('site/dashboard/story');
    }
    
    /**
     * @function package()
     * Survey Billing Package Page.
     * GET /
     * @return Response billing Package page view
     */
    public function package() {

        return View::make('site/dashboard/package');
    }

    /**
     * Show the form for creating a new resource.
     * GET /surveydashboard/create
     *
     * @return Response
     */
    public function postCreate() {
        
        echo 'zain here';

        die;
        
    }

    /**
     * Survey a newly created resource in storage.
     * POST /surveydashboard
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     * GET /surveydashboard/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
    }


    /**
     * Update the specified resource in storage.
     * PUT /surveydashboard/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /surveydashboard/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

}
