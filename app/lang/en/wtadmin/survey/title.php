<?php

return array(

	'survey_management' => 'Survey Management',
    'survey_update' => 'Update a Survey',
    'survey_delete' => 'Delete a Survey',
    'create_a_new_survey' => 'Create a New Survey',
    'survey_detail' => 'Survey detail'
);