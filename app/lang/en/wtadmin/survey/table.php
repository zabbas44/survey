<?php

return array(
    'term' => 'Term',
    'name' => 'Url Key',
    'FullName' => 'Name',
    'entries' => 'Entries',
    'valid_date' => 'Valid Date',
    'content' => 'Content',
    'pass' => 'Pass',
    'users' => '# of Users',
    'created_at' => 'Created at',
    'd_created_at' => "Created on",
    'd_survey_name' => "Survey Name",
    'd_survey_question' => 'Question',
    'd_survey_option1' => 'Option 1',
    'd_survey_option2' => 'Option 2',
    'd_survey_option3' => 'Option 3',
    'd_survey_option4' => 'Option 4',
    'd_survey_anwer' => "Survey Answer",
    "d_s_name" => "Survey Name" ,
    "d_s_comment" => "Comment",
    'd_survey_response' => 'Response Option',
    'd_survey_text' => 'Response Text'
);
