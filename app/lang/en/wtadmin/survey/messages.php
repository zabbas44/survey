<?php

return array(
    'already_exists' => 'Survey already exists!',
    'does_not_exist' => 'Survey does not exist.',
    'name_required' => 'The name field is required',
    'already' => 'Survey name already exist.',
    'create' => array(
        'error' => 'Survey was not created, please try again.',
        'success' => 'Survey created successfully.'
    ),
    'update' => array(
        'error' => 'Survey was not updated, please try again',
        'success' => 'Survey updated successfully.'
    ),
    'delete' => array(
        'error' => 'There was an issue deleting the survey. Please try again.',
        'success' => 'The survey was deleted successfully.'
    )
    ,'packageexpired' => "Available Package Consumed / Upgrade Package",
    'payment_error' => 'Please Fill all information and try again!',
    'create' => "Survey created successfully!"
);
