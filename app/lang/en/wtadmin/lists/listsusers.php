<?php

return array(

	    'list_name'    => 'List name required',
        'list_file'    => 'Csv file is required',
        'list_file_type' => 'Invalid file type! please upload .csv',
        'success_import' => 'File import Results. %s records success | %s records failed',
        'success_import_normal' => 'File import Results.',
        'success_email_add' => 'Email Address Added.',
        'email_invalid'  => 'Email address invalid! please recheck and upload',
        'file_open_error' => 'File cannot be opened, or some internal error please try again!',
        'generic_error'  => "please provide all information and try again!",
        "duplicate_email" => "email already exists for this list, try another",
        "invalid_email" => "email is invalid, try again!"
);
