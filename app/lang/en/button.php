<?php

return array(

	'list' => 'Sub Categories',
    'edit' => 'Edit',
    'driver_detail' => 'Driver Detail',
    'assign' => 'Assign',
    'view_assign' => 'View Assignments',
    'uhome' => 'User Home',
    'delete' => 'Delete',
    'duplicate' => 'Duplicate',
);
