<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersEmailsClients extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_emails_clients', function(Blueprint $table)
		{
			$table->increments('id');
			$table->engine = 'InnoDB';
			$table->string('u_e_c_key');
			$table->string('u_e_c_type');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("users_emails_clients");
	}
}
